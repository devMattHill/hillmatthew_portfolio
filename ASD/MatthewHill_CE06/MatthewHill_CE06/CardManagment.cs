﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MattsTools;
using System.Collections;

namespace MatthewHill_CE06
{
    class Card
    {
        public string Name { get; private set; }
        public string Description { get; set; }
        public decimal Value { get; set; }

        public Card() { }
        public Card(string name, string description, decimal value)
        {
            while (true)
            {
                if (value < 0)
                {
                    Console.WriteLine("Value can not be less than zero.");
                    Validator validator = new Validator();
                    value = validator.GetDecimal();
                }
                else
                {
                    break;
                }
            }

            Name = name;
            Description = description;
            Value = value;
        }


        public override string ToString()
        {
            string cardDisplay = "------------------------------\r\n|Name: " + Name + "\r\n|Description: " + Description + "\r\n|Value:" + Value + "\r\n------------------------------";
            return cardDisplay;

        }

    }




    class CollectionManager
    {
        public List<Card> AvailableCards { get; set; }
        public Dictionary<string,List<Card>> CardCollections { get; set; }


        // boolean values for stopping errors
        public bool CollectionsExist { get; set; }
        public bool CardsAreAvailable { get; set; }

        public CollectionManager()
        {
            CollectionsExist = false;
            CardsAreAvailable = false;
            AvailableCards = new List<Card>();
            CardCollections = new Dictionary<string, List<Card>>();
        }


        //Displays a collection
        public void DisplayCollection (string collectionName)
        {
            Console.WriteLine("Collection: {0}", collectionName);
            Console.WriteLine("*************** Inventory ***************\r\n");

            foreach (var card in CardCollections[collectionName])
            {
                Console.WriteLine(card);
                Console.WriteLine("\r\n");
            }
            Console.WriteLine("*****************************************\r\n");
        }

        public void DisplayAllCollections( )
        {


            foreach (var collection in CardCollections)
            {
                DisplayCollection(collection.Key);
            }

            if (CardsAreAvailable)
            {
                Console.WriteLine("Available Cards");
                Console.WriteLine("*************** Inventory ***************\r\n");

                foreach (var card in AvailableCards)
                {
                    Console.WriteLine(card);
                    Console.WriteLine("\r\n");
                }
                Console.WriteLine("*****************************************\r\n");
            }

        }


        public void MakeCardAvailable(Card card)
        {
            Card tmpCard = card;
            AvailableCards.Add(tmpCard);
            int numOfAvailableCards = AvailableCards.Count();
            CardsAreAvailable = true;
            Console.WriteLine("{0} has been added to the list of available cards.",tmpCard.Name);
            Console.WriteLine("Number of available cards: {0}", numOfAvailableCards);
        }

        public void MakeCardUnavailable(Card card)
        {
            AvailableCards.Remove(card);
            Console.WriteLine("{0} has been removed from the list of available cards.",card.Name);
            if (AvailableCards.Count() == 0)
            {
                Console.WriteLine("There are no more available cards.");
                CardsAreAvailable = false;
            }
            else
            {
                Console.WriteLine("Number of cards available: "+AvailableCards.Count() );
            }
        }

        public void AddCardToCollection(string collectionName, Card card)
        {
            if (CardCollections.ContainsKey(collectionName))
            {

            }
            else
            {
                CardCollections[collectionName] = new List<Card>();
                CollectionsExist = true;
            }

            CardCollections[collectionName].Add(card);
            Console.Clear();
            Console.WriteLine("{0} has been placed into the {1} collection.", card.Name, collectionName);

        }

        public void RemoveCardFromCollection(string collectionName, Card card)
        {
    
            CardCollections[collectionName].Remove(card);
            Console.Clear();
            Console.WriteLine("{0} has been moved from the collection named {1}.",card.Name,collectionName);
            CardsAreAvailable = true;
            if (CardCollections[collectionName].Count == 0)
            {
                CardCollections.Remove(collectionName);
                Console.WriteLine("The {0} collection has been removed from the collection manager as it no longer has any cards.",collectionName);
                if (CardCollections.Count == 0)
                {
                    Console.WriteLine("There are currently no active collections.");
                    CollectionsExist = false;
                }
            }

        }


    }
}





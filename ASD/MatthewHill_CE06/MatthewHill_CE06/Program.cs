﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MattsTools;

namespace MatthewHill_CE06
{
    class Program
    {
        static void Main(string[] args)
        {
           

            string mainMenuHeader = "Welcome to The Card Collection Manager.\r\nPlease select an action below.\r\nPick a number.";
            List<string> mainMenuOptions = new List<string>();
            mainMenuOptions.Add("Create collection manager");
            mainMenuOptions.Add("Create a card");
            mainMenuOptions.Add("Add a card to a collection");
            mainMenuOptions.Add("Remove a card from a collection");
            mainMenuOptions.Add("Display a collection");
            mainMenuOptions.Add("Display all collections");
            mainMenuOptions.Add("Exit");

            //Collection manager variable
            CollectionManager tradingTable = null;

            //Existence variables for preventing errors and throwing exceptions
            bool collectionManagerExists = false;
            //menu option instatiation
            int mainMenuOptionChosen = 0;

            //Beginning of main body loop a value of 7 breaks the loop
            while (mainMenuOptionChosen != 7) 
            {
                NumberMenu mainMenu = new NumberMenu(mainMenuHeader, mainMenuOptions);
                mainMenuOptionChosen = mainMenu.Display();

                switch (mainMenuOptionChosen)
                {

                    case 1: //Creat Collection Manager
                        if (collectionManagerExists)//warn about destroying old collection manager then proceed
                        {
                            string cmHeader = "A collection manager already exists.\r\nIf you create a new one all previous information will be lost.";
                            List<string> cmDecisions = new List<string>();
                            cmDecisions.Add("Create new collection manager.");
                            cmDecisions.Add("Return to main menu.");
                            NumberMenu cmMenu = new NumberMenu(cmHeader, cmDecisions);
                            int cmChoice = cmMenu.Display();
                            if (cmChoice == 1)
                            {
                                tradingTable = new CollectionManager();
                                collectionManagerExists = true;
                                Console.Clear();
                                Console.WriteLine("New collection manager created.\r\n");
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Returned to main menu.\r\n");
                            }
                        }
                        else
                        {
                            tradingTable = new CollectionManager();
                            collectionManagerExists = true;
                            Console.Clear();
                            Console.WriteLine("New collection manager created.\r\n");
                        }
                        break;

                    case 2: //Create a card
                        if (collectionManagerExists)//make a card
                        {
                            Card newCard = CreateCard();
                            Console.Clear();
                            tradingTable.MakeCardAvailable(newCard);
                            Console.WriteLine("\r\n");
                           
                        }
                        else//say there is no manager
                        {
                            Console.Clear();
                            Console.WriteLine("There is currently no collection manager.\r\nPlease create a collection manager.\r\n");
                        }
                        break;

                    case 3: //Add a card to a collection
                        if (collectionManagerExists)//check if there are any available cards
                        {
                            
                            if (tradingTable.CardsAreAvailable)//run card adding methods
                            {
                                tradingTable = AddCardToCollection(tradingTable);
                            }
                            else //say there are no cards available
                            {
                                Console.Clear();
                                Console.WriteLine("There are currently no available cards.\r\nPlease create a card.\r\n");
                            }
                        }
                        else //say there is no card manager
                        {
                            Console.Clear();
                            Console.WriteLine("There is currently no collection manager.\r\nPlease create a collection manager.\r\n");
                        }
                        break;

                    case 4: //Remove a card from a collection
                        if (collectionManagerExists)//check if there are any collections
                        {
                            if (tradingTable.CollectionsExist)//Run card removal methods
                            {
                                tradingTable = RemoveCardFromCollection(tradingTable);
                            }
                            else //Say no collections exist
                            {
                                Console.Clear();
                                Console.WriteLine("There are currently no collections with cards.\r\nPlease add a card to a collection.\r\n");
                            }
                        }
                        else //Say there is no card manager
                        {
                            Console.Clear();
                            Console.WriteLine("There is currently no collection manager.\r\nPlease create a collection manager.\r\n");
                        }
                        break;

                    case 5: //Display a collection
                        if (collectionManagerExists)//check if there are any collections
                        {
                            if (tradingTable.CollectionsExist)//Display collection names and display based on the return
                            {
                                Display(tradingTable);
                            }
                            else //Say no collections exist
                            {
                                Console.Clear();
                                Console.WriteLine("There are currently no collections with cards.\r\nPlease add a card to a collection.\r\n");
                            }
                        }
                        else //Say there is no card manager
                        {
                            Console.Clear();
                            Console.WriteLine("There is currently no collection manager.\r\nPlease create a collection manager.\r\n");
                        }
                        break;

                    case 6: //Display all collections
                        if (collectionManagerExists)//check if there are any collections
                        {
                            if (tradingTable.CollectionsExist)//Display the collections
                            {
                                Console.Clear();
                                tradingTable.DisplayAllCollections();
                                Console.WriteLine("Press any key to continue.");
                                Console.ReadKey();
                            }
                            else //Say no collections exist
                            {
                                Console.Clear();
                                Console.WriteLine("There are currently no collections with cards.\r\nPlease add a card to a collection.\r\n");
                            }
                        }
                        else //Say there is no card manager
                        {
                            Console.Clear();
                            Console.WriteLine("There is currently no collection manager.\r\nPlease create a collection manager.\r\n");
                        }
                        break;
                        
                    case 7: //Exit, don't do anything the break is right down there, the 7 should break the while loop for the main program

                        break;



                    default:
                        break;
                }




            }//End of the main body loop a value of 7 breaks the loop

        }//end of main method

        public static Card CreateCard()
        {
            Validator validator = new Validator();
            Console.WriteLine("Enter the name of the card:");
            string name = validator.GetString();
            Console.WriteLine("Enter a description of the card:");
            string description = validator.GetString();
            Console.WriteLine("Enter a value for the card:");
            decimal value = validator.GetDecimal();
            Card tmpCard = new Card(name, description, value);
            return tmpCard;

        }// end of create card

        public static CollectionManager AddCardToCollection(CollectionManager cm)
        {
            CollectionManager tmpCm = cm;
            Validator validator = new Validator();
            Console.WriteLine("To which collection would you like to add a card?");
            string collectionName = validator.GetString();
            
            //Builds the menu for picking which card you want
            string availableHeader = "\r\nWhich card would you like to move into the collection named "+collectionName+"?";
            List<string> availableOptions = new List<string>();
            foreach (var card in tmpCm.AvailableCards)
            {
                availableOptions.Add(card.Name);
            }
            NumberMenu availableMenu = new NumberMenu(availableHeader, availableOptions);
            int optionChosen = availableMenu.Display();
            Card cardChosen = tmpCm.AvailableCards[optionChosen-1];

            tmpCm.AddCardToCollection(collectionName, cardChosen);
            tmpCm.MakeCardUnavailable(cardChosen);
            Console.WriteLine("\r\n");
           
            return tmpCm;
        }

        public static CollectionManager RemoveCardFromCollection(CollectionManager cm)
        {
            Validator validator = new Validator();
            Console.WriteLine("From which collection would you like to remove a card?");
            string collectionName = validator.GetString();
            if (cm.CardCollections.ContainsKey(collectionName))
            {
                //builds menu for picking card
                string collectionHeader = "\r\nWhich card would you like to remove from the collection named " + collectionName + "?";
                List<string> collectionOptions = new List<string>();
                foreach (var card in cm.CardCollections[collectionName])
                {
                    collectionOptions.Add(card.Name);
                }
                NumberMenu collectionMenu = new NumberMenu(collectionHeader, collectionOptions);
                Card cardChosen = cm.CardCollections[collectionName][collectionMenu.Display()-1];

                cm.RemoveCardFromCollection(collectionName, cardChosen);
                cm.MakeCardAvailable(cardChosen);

                Console.WriteLine("\r\n");

            }
            else
            {
                Console.Clear();
                Console.WriteLine("The collection named {0} does not exist. \r\nTo see a list of available collections choose \"Display all collections\" from the main menu.\r\n",collectionName);
            }

            return cm;
        }

        public static void Display(CollectionManager cm)
        {
            Validator validator = new Validator();
            Console.WriteLine("Which collection do you want to display?");
            string collectionName = validator.GetString();
            if (cm.CardCollections.ContainsKey(collectionName))
            {
                Console.Clear();
                cm.DisplayCollection(collectionName);
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("The collection named {0} does not exist. \r\nTo see a list of available collections choose \"Display all collections\" from the main menu.\r\n", collectionName);
            }
        }
    }
}

﻿using MattsTools;
using System;
using System.Collections.Generic;

namespace MatthewHill_CE04
{
    class Program
    {
        //Logger info for the program class
        private static Logger logSheet = null;

        public static Logger GetLog()
        {
            return logSheet;
        }
        //Main Method
        static void Main(string[] args)
        {
            //Information for the main menu
            Dictionary<string, string> options = new Dictionary<string, string>();
            options.Add("DisableLogging", "Disables log functionality");
            options.Add("EnableLogging", "Enables log functionality");
            options.Add("CreateCar", "Creates a new car");
            options.Add("DriveCar", "Puts miles on the car");
            options.Add("DestroyCar", "Deletes the current car");
            options.Add("Exit", "Closes the program");
            string garageHeader;
            string loggingHeader;

            //Setup an instance of car
            Car currentCar = null;
            //Give the logsheet logger a value to prevent crashing
            logSheet = new DoNotLog();

            //Existence variables for preventing errors and throwing exceptions
            bool garageIsEmpty = true;
            bool exitProgram = false;
            bool loggingIsOn = false;
            //Main body loop starts here
            while (true)
            {   //Displays the main menu

                if (garageIsEmpty)
                {
                    garageHeader = "Your garage is currently empty.";
                }
                else
                {
                    garageHeader = "Your garage is currently full.";
                }

                if (loggingIsOn)
                {
                    loggingHeader = "Logging is on.";
                }
                else
                {
                    loggingHeader = "Logging is off.";
                }

                string header = "Welcome to your garage.\r\n"+garageHeader+"\r\n"+loggingHeader+"\r\nWhat would you like to do?\r\n";
                FancyMenu mainMenu = new FancyMenu(header, options);
                string menuOptionChosen = mainMenu.Display();
                switch (menuOptionChosen)
                {
                    case "disablelogging":
                        if (loggingIsOn)
                        {
                            
                            logSheet = LoggingOff();
                            loggingIsOn = false;
                        }
                        else
                        {
                            Console.Clear();
                            logSheet.Log("User attempted to disable logging. Logging was already disabled.");
                            Console.WriteLine("Logging is already off.\r\n");
                        }
                        break;

                    case "enablelogging":
                        if (loggingIsOn)
                        {
                            Console.Clear();
                            logSheet.Log("User attempted to enable logging. Logging was already enabled.");
                            Console.WriteLine("Logging is already on.\r\n");
                        }
                        else
                        {

                           
                            logSheet = LoggingOn();
                            logSheet.Log("User turned on logging.");
                            loggingIsOn = true;
                        }

                        break;

                    case "createcar":
                        if (garageIsEmpty)
                        {
                            currentCar = BuildCar();
                            garageIsEmpty = false;
                            Console.Clear();
                            logSheet.LogD(currentCar.ToString());
                            Console.WriteLine("Your car has been created.");
                        }
                        else
                        {
                            Console.Clear();
                            logSheet.Log("User attempted to creat a car with a car already in the garage.");
                            Console.WriteLine("There is already a car in your garage.");
                        }
                        
                        break;

                    case "drivecar":
                        if (garageIsEmpty)
                        {
                            Console.Clear();
                            logSheet.Log("User tried to drive car with an empty garage.");
                            Console.WriteLine("There is no car in your garage.\r\n");
                        }
                        else
                        {

                            float milesDriven;
                            while (true)
                            {
                                Console.WriteLine("How many miles do you wish to drive the car?");
                                Validator validator = new Validator();
                                milesDriven = validator.GetFloat();
                                if (milesDriven <= 0)
                                {
                                    Console.WriteLine("Please enter a number greater than zero.");
                                }
                                else
                                {
                                    break;
                                }

                            }
                            
                            currentCar.Drive(milesDriven);
                            string newMileage = currentCar.Mileage.ToString();
                            Console.Clear();
                            logSheet.LogW("New Mileage: " + newMileage);
                            Console.WriteLine("You drove {0} miles. The current mileag on the car is {1}.\r\n",milesDriven,newMileage);
                            
                        }
                        break;

                    case "destroycar":
                        if (garageIsEmpty)
                        {
                            Console.Clear();
                            logSheet.Log("User attempted to destroy car with an empty garage.");
                            Console.WriteLine("There is no car in your garage.\r\n");
                        }
                        else
                        {
                            currentCar = null;
                            garageIsEmpty = true;
                            Console.Clear();
                            logSheet.Log("User destroyed their car.");
                            Console.WriteLine("Current vehicle removed. Your garage is now empty.");
                        }

                        break;

                    case "exit":
                        exitProgram = true;
                        logSheet.Log("User exited program.");
                        
                        break;
                    default:
                        break;
                }//End of the main switch
                //This breaks the main body while loop
                if (exitProgram)
                {
                    break;
                }
                
            }//Main body loop ends here

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }//Main Method Ends Here

        public static Car BuildCar()
        {
            Validator validator = new Validator();
            Console.WriteLine("Please enter the make of the car.");
            string make = validator.GetString();

            Console.WriteLine("Please enter the model of the car.");
            string model = validator.GetString();

            int year;
            while (true)
            {
                Console.WriteLine("Please enter the year of the car.");
                year = validator.GetInt();
                if (year <= 0 )
                {
                    Console.WriteLine("Please enter a positive integer.");
                }
                else
                {
                    break;
                }
            }


            Console.WriteLine("Please enter the color of the car.");
            string color = validator.GetString();

            float mileage;
            while (true)
            {
                Console.WriteLine("Please enter the mileage on the car.");
                mileage = validator.GetFloat();
                if (mileage < 0)
                {
                    Console.WriteLine("Please enter zero or a positive number.");
                }
                else
                {
                    break;
                }
            }
           

            Car tmpCar = new Car(make, model, year, color, mileage);
            return tmpCar;
            
        }

        public static LogToConsole LoggingOn()
        {
            Console.Clear();
            Console.WriteLine("Logging is now on.\r\n");
            return new LogToConsole();
        }

        public static  DoNotLog LoggingOff()
        {
            Console.Clear();
            Console.WriteLine("Logging is now off.\r\n");
            return new DoNotLog();
        }

 
    }
}

﻿using System;

namespace MatthewHill_CE04
{
    interface ILog
    {
        void Log(string logInfo);
        void LogD(string logInfo);
        void LogW(string logInfo);
    }

    abstract class Logger : ILog
    {

        protected static int LineNumber { get; set; }

        abstract public void Log(string logInfo);
        abstract public void LogD(string logInfo);
        abstract public void LogW(string logInfo);

    }

    class DoNotLog : Logger
    {
        public override void Log(string logInfo) { }
        public override void LogD(string logInfo) { }
        public override void LogW(string logInfo) { }
    }

    class LogToConsole : Logger
    {
        public LogToConsole()
        {
            LineNumber = 0;
        }

        public override void Log(string logInfo)
        {
            LineNumber++;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(LineNumber +": "+ logInfo);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public override void LogD(string logInfo)
        {
            LineNumber++;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(LineNumber + ": DEBUG - " + logInfo);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public override void LogW(string logInfo)
        {
            LineNumber++;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(LineNumber + ": WARNING - " + logInfo);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }


}

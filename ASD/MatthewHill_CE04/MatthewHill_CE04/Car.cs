﻿namespace MatthewHill_CE04
{
    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        public float Mileage { get; set; }
        

        public Car() { }
        public Car(string make, string model, int year, string color, float mileage)
        {
            if (year < 0)
            {
                System.Console.WriteLine("Please enter a positive whole number for the year.");
            }
            else if (mileage < 0)
            {
                System.Console.WriteLine("Please enter zero or a positive number for the mileage.");
            }else { 
            Make = make;
            Model = model;
            Year = year;
            Color = color;
            Mileage = mileage;
            }

        }

        public void Drive (float milesDriven)
        {
            Mileage += milesDriven;
        }

        public override string ToString()
        {
            return "Make: "+ Make + ", Model: " + Model + ", Year: " + Year + ", Color: " + Color + ", Mileage: " + Mileage;
            
        }

    }
}

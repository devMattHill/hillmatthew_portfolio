﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE10
{
 


    class Item
    {
        public string Name { get; private set; }
        public decimal Value { get; set; }

        public Item() { }
        public Item(string name, decimal value)
        {

            Name = name;
            Value = value;
        }


        public override string ToString()
        {
            //string itemDisplay = $"Name: {Name} Price: {Value} Dubloons";
            return $"Name: {Name} Price: {Value}";

        }

    }


    class Customer
    {
        public string Name { get; set; }
        public List<Item> Cart { get; set; }
        public bool CartIsEmpty { get; set; }

        public Customer()
        {
            Cart = new List<Item>();
            CartIsEmpty = true;
        }
        public Customer(string name)
        {
            Name = name;
            Cart = new List<Item>();
            CartIsEmpty = true;
        }

        public decimal CartTotal()
        {
            decimal amt = 0;
            if (CartIsEmpty)
            {

            }
            else
            {

                foreach (Item item in Cart)
                {
                    amt += item.Value;
                }
            }
            return amt;
        }

        public int ItemCount()
        {
            int itemCount = Cart.Count;
            return itemCount;
        }
        

    }


    class Store
    {

        public string Name { get; set; }
        public List<Item> Inventory { get; set; }
        public Dictionary<string, Customer> Customers { get; set; }
        public string ActiveCustomer { get; set; }
        public bool StoreIsEmpty { get; set; }
        public Logger Log {get; set;}
        public string ItemInventory { get; set; }

        public Store() { }

        public Store(string name, string logFileName)
        {
            Name = name;
            Inventory = new List<Item>();
            Customers = new Dictionary<string, Customer>();
            ActiveCustomer = null;
            StoreIsEmpty = true;
            Log = new Logger(logFileName);
        }
	

        public void AddCustomer()
    {

            Console.WriteLine();
            Console.WriteLine("What is the name of the customer you would like to add?");

            Validator validator = new Validator();
            string name = validator.GetString();

            if (Customers.ContainsKey(name))
            {
                Console.Clear();
                Console.WriteLine($"{name} is already in the store.");
                Console.WriteLine();
                //Log here
                Log.Error($"User attempted to add an already existing customer named {name}.");
                
            }
            else
            {

                Customer tmp = new Customer(name);
                Customers.Add(tmp.Name, tmp);
                Console.Clear();
                Console.WriteLine($"{name} entered the store.");
                Console.WriteLine();
                ActiveCustomer = tmp.Name;
                //Log here
                Log.NewCustomer(tmp.Name);
            }
            StoreIsEmpty = Customers.Count < 1;
       }


       public void CashOutCustomer()
       {
            if (ActiveCustomer == null)
            {
                Console.Clear();
                Console.WriteLine("You currently have no customer selected.");
                Console.WriteLine();
                //log here
                Log.Error("User attempted to cashout with no selected customer.");
            }
            else
            {
                Customer tmp = Customers[ActiveCustomer];
                if (tmp.CartTotal() == 0)
                {
                    Console.WriteLine($"{ActiveCustomer} left the store without making a purchase.");
                    //Log here
                    Log.CashOut(ActiveCustomer, 0);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine($"{ActiveCustomer} purchased goods totalling {tmp.CartTotal()} and left the store.");
                    //Log here
                    Log.CashOut(ActiveCustomer, tmp.CartTotal());
                }
                Customers.Remove(ActiveCustomer);
                ActiveCustomer = null;
                StoreIsEmpty = (Customers.Count < 1);
            }


        }

        public void AddToCart()
        {
            Console.Clear();
            if (ActiveCustomer == null)
            {
                Console.Clear();
                Console.WriteLine("You currently have no customer selected.");
                Console.WriteLine();
                //log here
                Log.Error("User attempted to add to cart with no selected customer.");
            }
            else
            {
                //#problem with it allowing a new customer to access an empty menu
                if (StoreIsEmpty)
                {
                    Console.Clear();
                    Console.WriteLine($"The store inventory is empty.");
                    Console.WriteLine();
                    //Log Here
                    Log.Error($"User attempted to add an item to {ActiveCustomer}'s cart but the store is empty.");
                }

                else
                {
                    List<string> options = GetInventory();
                    int choice = ChooseItem(options);
                    Item tmp = Inventory[choice-1];
                    Customers[ActiveCustomer].Cart.Add(tmp);
                    Inventory.Remove(tmp);
                    Console.Clear();
                    Console.WriteLine($"{tmp.Name} was placed in {ActiveCustomer}'s cart.");
                    Console.WriteLine();
                    Customers[ActiveCustomer].CartIsEmpty = false;
                    //log here
                    Log.AddToCart(ActiveCustomer, tmp);
                    StoreIsEmpty = Inventory.Count < 1;
                }
            }


        }

        public void RemoveFromCart()
        {
            Console.Clear();
            if (ActiveCustomer == null)
            {

                Console.WriteLine("You currently have no customer selected.");
                Console.WriteLine();
                //log here
                Log.Error("User attempted to remove from cart with no selected customer.");
            }
            else
            {
                if (Customers[ActiveCustomer].CartIsEmpty)
                {
                    Console.WriteLine($"{ActiveCustomer}'s cart is empty.");
                    Console.WriteLine();
                    //Log Here
                    Log.Error($"User attempted to remove an item from {ActiveCustomer}'s empty cart.");
                }
                else
                {
                    List<string> options = GetCart();
                    int choice = ChooseItem(options);
                    Item tmp = Customers[ActiveCustomer].Cart[choice-1];
                    Customers[ActiveCustomer].Cart.Remove(tmp);
                    Inventory.Add(tmp);
                    Console.Clear();
                    Console.WriteLine($"{tmp.Name} was removed from {ActiveCustomer}'s cart.");
                    Console.WriteLine();
                    Customers[ActiveCustomer].CartIsEmpty = Customers[ActiveCustomer].Cart.Count < 1;
                    //log here
                    Log.RemoveFromCart(ActiveCustomer, tmp);
                }
            }

 


        }

        public int ChooseItem(List<string> optionItems)
        {
            string header = "Select an item below.";
            NumberMenu menu = new NumberMenu(header, optionItems);
            int choice = menu.Display();
            return choice;
        }

        public void BuildInventory(string storeInventoryFile)
        {

            using (StreamReader input = new StreamReader(storeInventoryFile))
            {
                while (input.Peek() > -1)
                {
                    string line = input.ReadLine();
                    string[] lineItem = line.Split(',');
                    string itemName = lineItem[0];
                    decimal itemPrice;
                    decimal.TryParse(lineItem[1], out itemPrice);
                    Item tmp = new Item(itemName, itemPrice);
                    Inventory.Add(tmp);
                    //Log
                    Log.CreateItem(tmp);

                }
            }

            StoreIsEmpty = Inventory.Count < 1;
        }

        public void SelectCustomer()
        {

            Console.Clear();
            string header = "Please select a number to choose your current customer.";
            List<string> currentCustomers = new List<string>();

            foreach (var customer in Customers)
            {
                currentCustomers.Add(customer.Key);
            }

            if (currentCustomers.Count < 1)
            {
                header = "There are currently no customers. \r\nPlease create a customer.";
            }
            currentCustomers.Add("Create New Customer");
            currentCustomers.Add("Go Back");

            NumberMenu customerMenu = new NumberMenu(header, currentCustomers);

            int chosenCustomer = customerMenu.Display();

            if (chosenCustomer == currentCustomers.Count())
            {
                Console.Clear();
                Log.Miscellaneous("User returned to main menu without selcting a customer.");
            }
            else if (chosenCustomer == currentCustomers.Count()-1)
            {
                Log.Miscellaneous("User chose to create a new customer.");
                AddCustomer();
            }
            else
            {
                Console.Clear();
                Console.WriteLine($"{currentCustomers[chosenCustomer - 1]} is now the selected customer.");
                Console.WriteLine();
                ActiveCustomer = currentCustomers[chosenCustomer-1];

                //Log
                Log.SelectCustomer(currentCustomers[chosenCustomer-1]);
            }
        }
        
        public List<string> GetInventory()
        {

            List<string> res = new List<string>();
            if (Inventory.Count > 0)
            {

                Console.Clear();
                Console.WriteLine("Current store inventory.");
                Console.WriteLine();
                foreach (var item in Inventory)
                {
                    
                    Console.WriteLine(item.Name + "\t" + String.Format("{0:C2}", item.Value));
                    res.Add(item.Name + "\t" + String.Format("{0:C2}", item.Value));
                    //Log here
                    Log.Miscellaneous("User displayed store inventory.");
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("The store has no more inventory.");
                Console.WriteLine();
                //Log Here
                Log.Miscellaneous("User attempted to display store inventory. Inventory was empty.");
            }


            

            return res;
        }

        public List<string> GetCart()
        {

            List<string> res = new List<string>();
            if (ActiveCustomer == null)
            {
                
                Console.Clear();
                Console.WriteLine("There is no currently selected customer.\r\nPlease select a customer.");
                Console.WriteLine();
                //Log here
                Log.Error("User attempted to view cart with no selected customer.");
                return null;
            }
            else
            {
                List<Item> cart = Customers[ActiveCustomer].Cart;
                if (cart.Count > 0)
                {
                    decimal total = 0;
                    Console.Clear();
                    Console.WriteLine($"{ActiveCustomer}'s cart");
                    Console.WriteLine();
                    foreach (var item in cart)
                    {
                        Console.WriteLine(item.Name + "\t" + String.Format("{0:C2}", item.Value));
                        res.Add(item.Name);
                        total += item.Value;
                        //Log here
                    }
                    Console.WriteLine("Cart total value is "+String.Format("{0:C2}",total)+".");
                    
                    Log.Miscellaneous("User displayed "+ActiveCustomer+"'s cart. with a value of "+total+".");
                    return res;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine($"{ActiveCustomer}'s cart is empty.");
                    Console.WriteLine("Try adding some items.");
                    Console.WriteLine();
                    //Log Here
                    Log.Miscellaneous($"User attempted to display {ActiveCustomer}'s cart inventory. Cart was empty.");
                    return null;
                }
            }

        }

        
        
        
    }//End of class store
    


    
}//End of namespace

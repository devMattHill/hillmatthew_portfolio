﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE10
{

    

    class Program
    {



        static void Main(string[] args)
        {
            Store store = new Store("Aramis' Armor Emporium","StoreLog.txt");
            store.BuildInventory("Inventory.txt");
            string mainMenuHeader;
            string mainMenuChosen = "";
            Dictionary<string, string> mainMenuOptions = null;

            while (mainMenuChosen != "exit program")
            {
                mainMenuOptions = new Dictionary<string, string>();
                if (store.Customers.Count < 1)
                {
                    mainMenuHeader = "Welcome to " + store.Name + ".\r\nThe store is currently empty.\r\nPlease select an option below.";
                    mainMenuOptions.Add("Create new Shopper", "");
                    mainMenuOptions.Add("View Store Inventory", "");
                    mainMenuOptions.Add("View Cart", "Unavailable");
                    mainMenuOptions.Add("Add Item to Cart", "Unavailable");
                    mainMenuOptions.Add("Remove Item from Cart", "Unavailable");
                    mainMenuOptions.Add("Complete Purchase or Leave Store", "Unavailable");
                    mainMenuOptions.Add("Exit Program", "");
                }
                else if (store.ActiveCustomer == null)
                {
                    mainMenuHeader = "Welcome to " + store.Name + ".\r\nThe is currently no shopper selected.\r\nPlease select an option below.";
                    mainMenuOptions.Add("Select Current Shopper", "<-------");
                    mainMenuOptions.Add("View Store Inventory", "");
                    mainMenuOptions.Add("View Cart", "Unavailable");
                    mainMenuOptions.Add("Add Item to Cart", "Unavailable");
                    mainMenuOptions.Add("Remove Item from Cart", "Unavailable");
                    mainMenuOptions.Add("Complete Purchase or Leave Store", "Unavailable");
                    mainMenuOptions.Add("Exit Program", "");


                }
                else
                {
                    string activeCustomer = store.ActiveCustomer;
                    mainMenuHeader = "Welcome to " + store.Name + ".\r\n"+ activeCustomer + " is the currently selected shopper.\r\nPlease select an option below.";
                    mainMenuOptions.Add("Select Current Shopper", "");
                    mainMenuOptions.Add("View Store Inventory", "");
                    mainMenuOptions.Add("View Cart", $"{activeCustomer}'s cart contains {store.Customers[activeCustomer].ItemCount()} items.");
                    mainMenuOptions.Add("Add Item to Cart", "");
                    mainMenuOptions.Add("Remove Item from Cart", "");
                    if (store.Customers[activeCustomer].ItemCount() > 0)
                    {
                        mainMenuOptions.Add("Complete Purchase", $"Cart total {store.Customers[activeCustomer].CartTotal()}.");
                    }
                    else
                    {
                        mainMenuOptions.Add("Leave Store", "");
                    }
                    
                    mainMenuOptions.Add("Exit Program", "");
                }
                /*
                 Notes for next portfolio class. Come into the menu and add more if then statements so that if an option is not valid, it just won't appear in the menu. MattsTools.FancyMenu should already support this. 
                 */
                FancyMenu mainMenu = new FancyMenu(mainMenuHeader, mainMenuOptions);
                mainMenuChosen = mainMenu.Display();




                switch (mainMenuChosen)
                {

                    case "select current shopper":
                    case "create new shopper":
                        {
                            store.SelectCustomer();
                        }
                        break;


                    case "view store inventory":
                        {
                            List<string> inventory = store.GetInventory();
                            if (inventory == null)
                            {

                            }
                            else
                            {
                                Console.WriteLine("\r\nPress any key to continue.");
                                Console.ReadKey();
                                Console.Clear();
                            }

                        }
                        break;


                    case "view cart":
                        {
                            List<string> cart = store.GetCart();
                            if (cart == null)
                            {

                            }
                            else
                            {
                                Console.WriteLine("\r\nPress any key to continue.");
                                Console.ReadKey();
                                Console.Clear();
                            }

                        }
                        break;


                    case "add item to cart":
                        {
                            store.AddToCart();
                        }
                        break;



                    case "remove item from cart":
                        {
                            store.RemoveFromCart();
                        }
                        break;



                    case "complete purchase or leave store":
                    case "complete purchase":
                    case "leave store":
                        {
                            store.CashOutCustomer();
                        }
                        break;



                    case "exit program":
                        {

                        }
                        break;




                    default:
                        break;
                }
            }


            
            


            

        }






    }// end of class Program
}

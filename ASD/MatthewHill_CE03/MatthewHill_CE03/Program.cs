﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace MatthewHill_CE03
{
    class Program
    {
        static void Main(string[] args)
        {
            int menuOption;
            
            Course course1 = new Course();
            while (true)
            {   //This finds the
                menuOption = PickMenuOption();
                //This ends the main body loop if 5 is returned from the PickFishColor method
                switch (menuOption)
                {
                    case 1: //This checks if course1 has a name before allowing customer creation.
                        if (course1.Title == null)
                        {
                            course1 = CreateCourse();
                        }
                        else
                        {
                            Console.WriteLine("You already have a course named " + course1.Title + ".\r\n\r\n");
                        }
                        break;

                    case 2: //This checks if course1 has a name before allowing teacher creation.
                        if (course1.Title == null)
                        {
                            Console.WriteLine("You have not created a course.\r\n\r\n");
                        }
                        else if (course1.Instructor != null)
                        {
                            Console.WriteLine("You have already created an instructor named {0}.\r\n\r\n",course1.Instructor.Name);
                        }
                        else
                        {
                            course1.Instructor = CreateInstructor();
                        }
                        break;

                    case 3:
                        if (course1.Title == null)
                        {
                            Console.WriteLine("You have not created a course.\r\n\r\n");
                        }
                        else if (course1.Students != null)
                        {
                            Console.WriteLine("You have already created the roster for this course.\r\n\r\n");
                        }
                        else
                        {
                            course1.Students = CreateRoster(course1);
                        }
                        break;

                    case 4:
                        if (course1.Title == null)
                        {
                            Console.WriteLine("You have not created a course.\r\n\r\n");
                        }
                        else
                        {
                            DisplayClassInfo(course1);
                        }

                        break;

                    case 5:
                        break;
                    default:
                        break;
                }
                //This ends the main loop if menu option is 5
                if (menuOption == 5)
                {
                    break;
                }//End of the main program while loop


            }

            Console.WriteLine("\r\nThank you for using Course Tracker!\r\nPress any key to exit.\r\n");
            Console.ReadKey();

        }//End of Main method

        public static int PickMenuOption()
        {

            int option;

            while (true)
            {
                Console.WriteLine("Welcome to Course Tracker.");
                Console.WriteLine("Please select an option.");
                Console.WriteLine("***************************");
                Console.WriteLine("* 1. Create Course");
                Console.WriteLine("* 2. Create Teacher");
                Console.WriteLine("* 3. Add Students");
                Console.WriteLine("* 4. Display Class Information");
                Console.WriteLine("* 5. Exit");
                Console.WriteLine("**************************");
                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out option))
                {
                    //This makes sure the integer is number 1-5
                    if (option == 1 || option == 2 || option == 3 || option == 4 || option == 5)
                    {
                        return option;
                    }
                    //Error Code
                    else
                    {
                        Console.WriteLine(option + " is not a valid option. Please select a number between 1 and 5");
                    }
                    //Error Code
                }
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please select a number between 1 and 5.");
                }
            }


        }

        public static Course CreateCourse()
        {
            
            //input for course title
            string title = "";
            while (string.IsNullOrWhiteSpace(title))
            {
                Console.WriteLine("Please enter the course title.");
                title = Console.ReadLine();
            }
            //input for course description
            string description = "";
            while (string.IsNullOrWhiteSpace(description))
            {
                Console.WriteLine("Please enter the course description.");
                description = Console.ReadLine();
            }
            //input for course size
            int size;
            while (true)
            { //gets the roster size
                Console.WriteLine("Please enter number of students on the class roster.");

                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out size))
                {
                    break;
                }//Error Code
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please give an integer value for the rosterSize.");
                }
            }

            Course tempCourse = new Course(title, description, size);
            return tempCourse;
        }

        public static Teacher CreateInstructor()
        {
            //input for instructor name
            string name = "";
            while (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Please enter the teacher's name.");
                name = Console.ReadLine();
            }
            //input for instructor description
            string description = "";
            while (string.IsNullOrWhiteSpace(description))
            {
                Console.WriteLine("Please enter a description for the teacher.");
                description = Console.ReadLine();
            }
            int age;
            while (true)
            { //gets the instructors age
                Console.WriteLine("Please input teacher's age.");

                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out age))
                {
                    if (age < 1)
                    {   //Error code of young instructors.
                        Console.WriteLine("That is against schoool policy.\r\nPlease have your teacher doublecheck their date of birth.");
                    }
                    else
                    {
                        break;
                    }

                }//Error Code
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please give an integer for the teacher's age.");
                }
            }


            //input for instructor knowledge
            string knowledgeAreas = "";
            while (string.IsNullOrWhiteSpace(knowledgeAreas))
            {
                Console.WriteLine("Please enter a list of of the instructors knowledge areas separated by commas.");
                knowledgeAreas = Console.ReadLine();
            }
            string[] knowledge = knowledgeAreas.Split(',');

            Teacher tempTeacher = new Teacher(name, description, age, knowledge);
            return tempTeacher;
        }

        public static Student CreateStudent(int num)
        {
            //input for student name
            string name = "";
            while (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Please enter the student name for student number {0}.",num);
                name = Console.ReadLine();
            }
            //input for student description
            string description = "";
            while (string.IsNullOrWhiteSpace(description))
            {
                Console.WriteLine("Please enter a description for {0}.",name);
                description = Console.ReadLine();
            }
            int age;
            while (true)
            { //gets the instructors age
                Console.WriteLine("Please input {0}'s age.",name);

                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out age))
                {
                    if (age < 0)
                    {   //Error code of young instructors.
                        Console.WriteLine("That is against schoool policy.\r\nPlease have {0} double check their date of birth.",name);
                    }
                    else
                    {
                        break;
                    }

                }//Error Code
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please give an integer value for {0}'s age.",name);
                }
            }

            int grade;
            while (true)
            { //gets the instructors age
                Console.WriteLine("Please input {0}'s current grade.", name);

                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out grade))
                {
                    if (grade < 0  || grade >100)
                    {   //Error code of young instructors.
                        Console.WriteLine("Please enter a whole number value between 0 and 100.");
                    }
                    else
                    {
                        break;
                    }

                }//Error Code
                else
                {
                    Console.WriteLine(input + "Please enter a whole number value between 0 and 100.");
                }
            }

            
            Student tempStudent = new Student(name, description, age, grade);
            return tempStudent;
        }

        public static ArrayList CreateRoster(Course course)
        {
            ArrayList tempRoster = new ArrayList();

            for (int i = 0; i < course.Size; i++)
            {
                tempRoster.Insert(0, CreateStudent(i+1));
            }
            return tempRoster;
        }

        public static void DisplayClassInfo(Course course)
        {
            Console.WriteLine(course);
            if (course.Instructor != null)
            {
                Console.WriteLine(course.Instructor);
            }
            if (course.Students != null)
            {
                foreach (var student in course.Students)
                {
                    Console.WriteLine(student);
                }
            }

        }
    }
}

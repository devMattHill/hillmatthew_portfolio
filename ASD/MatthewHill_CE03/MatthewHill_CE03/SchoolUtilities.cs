﻿using System.Collections;

namespace MatthewHill_CE03
{
    class Person
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Age { get; set; }

        public Person() { }
        public Person(string name, string description, int age)
        {
            Name = name;
            Description = description;
            Age = age;
        }

    }

    class Student:Person
    {
        public int Grade { get; set; }
        public Student()
        {
        }
        public Student(string name, string description, int age, int grade) :
            base(name, description, age)
        {
            Grade = grade;
        }

        //Needs a ToString written
        public override string ToString()
        {
         
            return "\r\nName: " + Name + "\r\nDescription: " + Description + "\r\nAge: " + Age + "\r\nGrade: " + Grade+ "\r\n";
        }
    }

    class Teacher:Person
    {
        public string[] Knowledge { get; set; }
        public Teacher()
        {
        }
        public Teacher(string name, string description, int age, string[] knowledge) :
            base(name, description, age)
        {
            Knowledge = knowledge;
        }

        //Needs a ToString Written
        public override string ToString()
        {
            string credentials="";
            foreach (var item in Knowledge)
            {
                credentials += item + "\r\n          ";
            }
            return "Teacher\r\nName: " + Name + "\r\nDescription: " + Description + "\r\nAge: " +Age+ "\r\nKnowledge: "+credentials;
        }

    }

    class Course
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Teacher Instructor { get; set; }
        public int Size { get; set; }
        public ArrayList Students { get; set; }

        public Course()
        {
        }
        public Course(string title, string description, int size)
        {
            Title = title;
            Description = description;
            Size = size;
        }
            //This needs a really fancy ToString Written
        public override string ToString()
        {
            return "Course\r\nTitle: " + Title + "\r\nDescription: " + Description + "\r\nSize: "+Size+"\r\n";
        }
    
    }
}

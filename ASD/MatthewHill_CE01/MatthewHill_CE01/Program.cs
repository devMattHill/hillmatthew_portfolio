﻿using System;
using System.Collections;
using System.Linq;

namespace MatthewHill_CE01
{
    class Program
    {




        static void Main(string[] args)
        {







            int colorOption;
            int lengthOption;

            //Fish Color options
            string[] fishColorOptions = { "", "Teal", "Cyan", "Silver", "Gold"};
            string[] fishLengthOptions = { "", "longest", "shortest" };

            //The fish tank
            string[] fishes = { "Teal", "Cyan", "Silver", "Gold", "Teal", "Cyan", "Silver", "Gold", "Teal", "Cyan", "Silver", "Gold" };
            float[] lengths = { 1.25f, 3.75f, 5.25f, 7.75f, 9.25f, 11.75f, 12f, 10.5f, 8f, 6.5f, 4f, 2.5f };

            //This is the while loop to keep the main program running
            while (true)
            {   //This finds the
                colorOption = PickFishColor(fishColorOptions);
                //This ends the main body loop if 5 is returned from the PickFishColor method
                if (colorOption == 5)
                {
                    break;
                }
                lengthOption = PickFishLength(fishLengthOptions);
                if (lengthOption == 3)
                {
                    break;
                }

                //This calls the FindLength method to do exactly that.
                float lengthFound = FindLength(fishLengthOptions[lengthOption], fishColorOptions[colorOption], fishes, lengths);
                //This is the output and answer.
                Console.WriteLine("\r\nThe {0} {1} fish is {2}cm long.\r\n", fishLengthOptions[lengthOption], fishColorOptions[colorOption], lengthFound);

            } //End of the main program while loop

            Console.WriteLine("\r\nThank you for using fish finder!\r\nPress any key to exit.\r\n");
            Console.ReadKey();
           
        }//This is the end of the main method


        public static int PickFishColor(string[] options)
        {
            int option;

            while (true)
            {
                Console.WriteLine("Welcome to Fish Finder.");
                Console.WriteLine("Please select the color of the fish you would like to find.");
                Console.WriteLine("* ><> ><> ><> ><> ><> ><> *");
                Console.WriteLine("* 1. {0}", options[1]);
                Console.WriteLine("* 2. {0}", options[2]);
                Console.WriteLine("* 3. {0}", options[3]);
                Console.WriteLine("* 4. {0}", options[4]);
                Console.WriteLine("* 5. End Program");
                Console.WriteLine("* <>< <>< <>< <>< <>< <>< *");
                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out option))
                {
                    //This makes sure the integer is number 1-5
                    if (option == 1 || option == 2 || option == 3 || option == 4 || option == 5)
                    {
                        return option;
                    }
                    //Error Code
                    else
                    {
                        Console.WriteLine(option + " is not a valid option. Please select a number between 1 and 5");
                    }
                //Error Code
                }
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please select a number between 1 and 5.");
                }
            }
        }  //End of PickFishColor

        public static int PickFishLength(string[] options)
        {
            int option;

            while (true)
            {
                Console.WriteLine("Please select the length of the fish you would like to find.");
                Console.WriteLine("* ><> ><> ><> ><> ><> ><> *");
                Console.WriteLine("* 1. {0}", options[1]);
                Console.WriteLine("* 2. {0}", options[2]);
                Console.WriteLine("* 3. End Program");
                Console.WriteLine("* <>< <>< <>< <>< <>< <>< *");
                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out option))
                {
                    //This makes sure the integer is number 1-5
                    if (option == 1 || option == 2 || option == 3)
                    {
                        return option;
                    }
                    //Error Code
                    else
                    {
                        Console.WriteLine(option + " is not a valid option. Please select a number between 1 and 3");
                    }
                    //Error Code
                }
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please select a number between 1 and 3.");
                }
            }
        }  //End of PickFishLength


        public static float FindLength(string longOrShort, string color, string[] colors, float[] lengths) {
            
            int count = 0;
            ArrayList possibleLengths = new ArrayList();
            foreach (var item in colors)
            {
                if (item == color)
                {
                    possibleLengths.Add(lengths[count]);
                }
                count++;
            }

            float[] possibleFishLengths = possibleLengths.ToArray(typeof(float)) as float[];

            if (longOrShort == "longest" )
            {
                return possibleFishLengths.Max();
            }
            else
            {
                return possibleFishLengths.Min();
            }
            
        }


    }
}

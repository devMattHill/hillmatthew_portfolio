﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE07
{
     class Employee : IComparable<Employee>
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public Employee () {}

        public Employee(string name, string address)
        {
            Name = name;
            Address = address;
        }

        public virtual decimal CalculatePay()
        {
            return 0;
        }
        

        public virtual void Create() { }

        public int CompareTo(Employee next)
        {
            return this.Name.CompareTo(next.Name);
        }
    }

     class Hourly:Employee
    {
        public decimal PayPerHour { get; set; }
        public decimal HoursPerWeek { get; set; }

        public Hourly()
        {
        }
        public Hourly(string name, string address, decimal payPerHour, decimal hoursPerWeek):
            base(name, address)
        {
            
            PayPerHour = payPerHour;
            HoursPerWeek = hoursPerWeek;
        }

        public override decimal CalculatePay()
        {
            return PayPerHour * HoursPerWeek * 52;
        }



    }

    class FullTime : Hourly
    {
        FullTime() { }

        public FullTime(string name, string address, decimal payPerHour) :
            base(name, address, payPerHour, 40)
        { }

        

    }

    class PartTime : Hourly
    {
        public PartTime() { }

        public PartTime(string name, string address, decimal payPerHour, decimal hoursPerWeek) :
            base(name, address, payPerHour, hoursPerWeek)
        { }
    }

    class Contractor : Hourly
    {
        public decimal NoBenefitsBonus { get; set; }

        public Contractor() { }
        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonus) :
            base(name, address, payPerHour, hoursPerWeek)
        {
            NoBenefitsBonus = 1+(noBenefitsBonus/100);
        }

        public override decimal CalculatePay()
        {
            return PayPerHour * HoursPerWeek * NoBenefitsBonus * 52;
        }

    }

    class Salaried : Employee
    {
        public decimal Salary { get; set; }

        public Salaried() { }
        public Salaried(string name, string address, decimal salary) :
            base(name, address)
        {
            Salary = salary;
        }

        public override decimal CalculatePay()
        {
            return Salary;
        }
    }

    class Manager : Salaried
    {
        decimal Bonus { get; set; }

        public Manager() { }
        public Manager(string name, string address, decimal salary, decimal bonus) :
            base(name, address,salary)
        {
            Bonus = bonus;
        }

        public override decimal CalculatePay()
        {
            return Salary + Bonus;
        }

    }

    



}

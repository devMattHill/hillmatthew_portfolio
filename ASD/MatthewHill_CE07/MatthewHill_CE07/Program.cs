﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MattsTools;

namespace MatthewHill_CE07
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> roster = new List<Employee>();
            bool rosterIsEmpty = (roster.Count <= 0);

            string menuHeader = "Welcome to Payroll Manager.\r\n Please select an option below.";
            Dictionary<string, string> menuOptions = new Dictionary<string, string>();
            menuOptions.Add("Add Employee", "");
            menuOptions.Add("Remove Employee", "");
            menuOptions.Add("Display Payroll", "");
            menuOptions.Add("Exit", "");
            FancyMenu mainMenu = new FancyMenu(menuHeader, menuOptions);

            string optionChosen = "";

            while (optionChosen != "exit")
            {
                optionChosen = mainMenu.Display();
                switch (optionChosen)
                {

                    case "add employee":
                        Employee employeeAdded = AddEmployee();
                        if (employeeAdded == null)
                        {
                            Console.Clear();
                        }
                        else
                        {
                            roster.Add(employeeAdded);
                            rosterIsEmpty = (roster.Count <= 0);
                            Console.Clear();
                            Console.WriteLine($"{employeeAdded.Name} was added to the company roster.");
                            roster.Sort();
                        }

                        break;
                    case "remove employee":
                        if (rosterIsEmpty)
                        {
                            Console.Clear();
                            Console.WriteLine("There are no employees to remove.\r\nPlease add an employee.\r\n");
                        }
                        else
                        {
                            Employee removedEmployee = RemoveEmployee(roster);
                            roster.Remove(removedEmployee);
                            rosterIsEmpty = (roster.Count <= 0);
                            Console.Clear();
                            Console.WriteLine("{0} has been removed from the company roster.\r\n",removedEmployee.Name);
                        }

                        break;
                    case "display payroll":
                        if (rosterIsEmpty)
                        {
                            Console.Clear();
                            Console.WriteLine("There are no employees who need payroll calculated.\r\nPlease add an employee.\r\n");
                        }
                        else
                        {
                            Console.Clear();
                            DisplayPayroll(roster);
                            Console.WriteLine("Press any button to continue.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case "exit":

                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine("Thank you for using Payroll Manager.\r\nPress any key to exit.");

        }

        public static Employee AddEmployee()
        {
            Console.Clear();
            string employeeMenuHeader = "Which type of employee would you like to add?";
            Dictionary<string, string> employeeOptions = new Dictionary<string, string>();
            employeeOptions.Add("Full Time", "");
            employeeOptions.Add("Part Time", "");
            employeeOptions.Add("Contractor", "");
            employeeOptions.Add("Salaried", "");
            employeeOptions.Add("Manager", "");
            employeeOptions.Add("Exit", "");
            FancyMenu employeeMenu = new FancyMenu(employeeMenuHeader, employeeOptions);
            string option = "";
            while (option != "exit")
            {
                option = employeeMenu.Display();

                switch (option)
                {
                    case "full time":
                        FullTime tmpFT = BuildFulltime();
                        return tmpFT;

                    case "part time":
                        PartTime tmpPT = BuildPartTime();
                        return tmpPT;
                        

                    case "contractor":
                        Contractor tmpCtr = BuildContractor();
                        return tmpCtr;
                        

                    case "salaried":
                        Salaried tmpSal = BuildSalaried();
                        return tmpSal;
                        

                    case "manager":
                        Manager tmpMan = BuildManager();
                        return tmpMan;

                    case "exit":
                        break;
                        
                    default:
                        break;
                }
            }
            return null;

        }

        public static Employee BuildEmployee(string emType)
        {
            Console.Clear();
            Validator validator = new Validator();
            Console.WriteLine("Enter the following information. About the {0} employee.",emType);
            Console.WriteLine("Name:");
            string name = validator.GetString();
            Console.WriteLine("Address:");
            string address = validator.GetString();
            Employee employee = new Employee(name, address);
            return employee;
        }

        public static FullTime BuildFulltime()
        {
            Validator validator = new Validator();
            Employee employee = BuildEmployee("full time");
            Console.WriteLine("Pay per hour:");
            decimal payPerHour = validator.GetDecimal();
            FullTime tmpEm = new FullTime(employee.Name, employee.Address, payPerHour);
            return tmpEm;
            
        }

        public static PartTime BuildPartTime()
        {
            Validator validator = new Validator();
            Employee employee = BuildEmployee("part time");
            Console.WriteLine("Pay per hour:");
            decimal payPerHour = validator.GetDecimal();
            Console.WriteLine("Hours worked per week:");
            decimal hoursPerWeek = validator.GetDecimal();
            PartTime tmpEm = new PartTime(employee.Name, employee.Address, payPerHour,hoursPerWeek);
            return tmpEm;
        }

        public static Contractor BuildContractor()
        {
            Validator validator = new Validator();
            Employee employee = BuildEmployee("contractor");
            Console.WriteLine("Pay per hour:");
            decimal payPerHour = validator.GetDecimal();
            Console.WriteLine("Hours worked per week:");
            decimal hoursPerWeek = validator.GetDecimal();
            Console.WriteLine("Contractor bonus. Type in percent value such as \"50\" for 50%.");
            decimal noBenefitsBonus = validator.GetDecimal();
            Contractor tmpEm = new Contractor(employee.Name, employee.Address, payPerHour, hoursPerWeek,noBenefitsBonus);
            return tmpEm;
        }

        public static Salaried BuildSalaried()
        {
            Validator validator = new Validator();
            Employee employee = BuildEmployee("salaried");
            Console.WriteLine("Salary:");
            decimal salary = validator.GetDecimal();
            Salaried tmpEm = new Salaried(employee.Name, employee.Address, salary);
            return tmpEm;

        }

        public static Manager BuildManager()
        {
            Validator validator = new Validator();
            Employee employee = BuildEmployee("manager");
            Console.WriteLine("Salary:");
            decimal salary = validator.GetDecimal();
            Console.WriteLine("Annual Bonus:");
            decimal bonus = validator.GetDecimal();
            Manager tmpEm = new Manager(employee.Name, employee.Address, salary, bonus);
            return tmpEm;
        }
        


        public static Employee RemoveEmployee(List<Employee> roster)
        {
            string removeHeader = "Select the number of the employee you wish to remove.";
            List<string> removeOptions = new List<string>();
            foreach (Employee employee in roster)
            {
                removeOptions.Add(employee.Name);
            }
            NumberMenu removeMenu = new NumberMenu(removeHeader, removeOptions);

            int chosen = removeMenu.Display();

            return roster[chosen-1];


        }

        public static void DisplayPayroll(List<Employee> roster)
        {
            Console.Clear();
           
            foreach (var em in roster)
            {

                Console.WriteLine($"Name: {em.Name}\tAddress: {em.Address}\tYearly Pay: {em.CalculatePay()}");
                Console.WriteLine();
            }
            
        }
    }


}

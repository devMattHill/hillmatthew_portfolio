﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MattsTools;

namespace MatthewHill_CE08
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 0;
            while (choice != 4)
            {

            
                string dataLocation = @"..\..\Datafiles\";
                string dataFields = @"..\..\Datafiles\DataFieldsLayout.txt";
                string mainJsonObject = "";

                string fileMenuHeader = "Choose a file to clean up into a JSON format.";
                List<string> fileOptions = new List<string>();
                fileOptions.Add("DataFile1");
                fileOptions.Add("DataFile2");
                fileOptions.Add("DataFile3");
                fileOptions.Add("Exit");

                NumberMenu fileMenu = new NumberMenu(fileMenuHeader, fileOptions);
                choice = fileMenu.Display();
                string dataFile = "";
                string outputFile = "";

                switch (choice)
                {
                    case 1:
                        dataFile = dataLocation+"DataFile1.txt";
                        outputFile = dataLocation + "OutputFile1.json";
                        mainJsonObject = BuildMainJsonObject(dataFile, dataFields);
                        OutputMainJsonObject(outputFile, mainJsonObject);
                        Console.Clear();
                        Console.WriteLine("OutputFile1.json has been created.");
                        Console.WriteLine();
                        break;

                    case 2:
                        dataFile = dataLocation + "DataFile2.txt";
                        outputFile = dataLocation + "OutputFile2.json";
                        mainJsonObject = BuildMainJsonObject(dataFile, dataFields);
                        OutputMainJsonObject(outputFile, mainJsonObject);
                        Console.Clear();
                        Console.WriteLine("OutputFile1.json has been created.");
                        Console.WriteLine();
                        break;

                    case 3:
                        dataFile = dataLocation + "DataFile3.txt";
                        outputFile = dataLocation + "OutputFile3.json";
                        mainJsonObject = BuildMainJsonObject(dataFile, dataFields);
                        OutputMainJsonObject(outputFile, mainJsonObject);
                        Console.Clear();
                        Console.WriteLine("OutputFile1.json has been created.");
                        Console.WriteLine();
                        break;

                    case 4:

                        break;

                    default:
                        break;
                }
                
            }
            

        }



        private static string BuildMainJsonObject(string dataFile, string dataFields )
        {
            string retJsonObject = "";

            string[] keys = GetAllLinesFromFile(dataFields);
            string[] allValues = GetAllLinesFromFile(dataFile);

            retJsonObject += "[ ";
            int length = allValues.Length;
            int i = 1;
            foreach (var line in allValues)
            {
                string[] values = line.Split('|');

                string tmpJsonObject = BuildJsonObject(keys, values);
                retJsonObject += tmpJsonObject;
                if (i < length)
                {
                    retJsonObject += ",";
                }
                i++;
            }
            retJsonObject += " ]";

            return retJsonObject;
        }

        private static void OutputMainJsonObject(string outputFile, string jsonObject)
        {
            using (StreamWriter output = new StreamWriter(outputFile))
            {
                output.WriteLine(jsonObject);
            }
        }



        private static string[] GetAllLinesFromFile(string fileRequested)
        {
            //using ArrayList prevents me from having to find a seperator character to add in and eventually to a .split('safecharacterhere')
            ArrayList allLines = new ArrayList();
            using (StreamReader input = new StreamReader(fileRequested))
            {
                while (input.Peek() > -1)
                {
                    string line = input.ReadLine();
                    if (line.Contains("BOF PUBLIC") || line.Contains("EOF PUBLIC"))
                    {

                    }
                    else
                    {
                        allLines.Add(line);
                    }
                }
            }
                return (string[]) allLines.ToArray(typeof (string));
        }

        private static string BuildJsonObject(string[] keys, string[] values)
        {
            string tmpJson = "";
            tmpJson += "{";
            int i = 0;
            foreach (var key in keys)
            {
                string tmpString = $"\"{key}\":\"{values[i]}\"";
                tmpJson += tmpString;
                if (values[i] != "!end")
                {
                    tmpJson += ",";
                }
                i++;
            }
            tmpJson += "}";
            return tmpJson;
        }
    }// end of class program


}// end of namespace

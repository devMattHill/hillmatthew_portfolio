﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MattsTools;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MatthewHill_CE09
{
    class Program
    {
        MySqlConnection Con = null;


        static void Main(string[] args)
        {
            Program instance = new Program();

            instance.Con = new MySqlConnection();
            instance.Connect();

            DataTable data = instance.QueryDB("SELECT DVD_Title, Price, publicRating FROM dvd where DVD_Title LIKE '%dragon%' limit 20");

            DataRowCollection rows = data.Rows;

            List<Dvd> inventory = new List<Dvd>();
            List<Dvd> shoppingCart = new List<Dvd>();

            foreach (DataRow row in rows)
            {
                string title = row["DVD_Title"].ToString();
                decimal price;
                decimal.TryParse(row["Price"].ToString(), out price);
                float rating;
                float.TryParse(row["publicRating"].ToString(), out rating);
                
                Dvd tmp = new Dvd(title, price, rating);
                inventory.Add(tmp);
            }
            inventory.Sort();
            
            //boolean values for throwing exceptions
            bool inventoryExists = (inventory.Count > 0);
            bool shoppingCartHasDvd = (shoppingCart.Count > 0);

            //And finally the main part of the program

            string menuHeader = "\r\nWelcome to DVD Mart.\r\nPlease select an option below.\r\n";
            Dictionary<string, string> menuOption = new Dictionary<string, string>();
            menuOption.Add("View Inventory", "");
            menuOption.Add("View Shopping Cart", "");
            menuOption.Add("Add DVD to Cart", "");
            menuOption.Add("Remove DVD from Cart", "");
            menuOption.Add("Exit", "");

            string chosenOption = "";

            FancyMenu mainMenu = new FancyMenu(menuHeader, menuOption);
            

            while (chosenOption != "exit")
            {
                chosenOption = mainMenu.Display();
                switch (chosenOption)
                {

                    case "view inventory":
                        {
                            if (inventoryExists)
                            {
                                Console.Clear();
                                Console.WriteLine("Store Inventory");
                                Console.WriteLine();
                                instance.ViewDvds(inventory);
                                Console.WriteLine();
                                Console.WriteLine("Press any key to continue.");
                                Console.ReadKey();
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("You put everything in your cart. Wow...");
                            }
                        
                            break;
                        }
                    case "view shopping cart":
                        {
                            if (shoppingCartHasDvd)
                            {
                                Console.Clear();
                                Console.WriteLine("Shopping Cart Contents:");
                                instance.ViewDvds(shoppingCart);
                                Console.WriteLine("Press any key to continue");
                                Console.ReadKey();
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("There are no DVDs in your cart.\r\nPlease add a DVD to your cart.");
                            }
                            break;
                        }
                    case "add dvd to cart":
                        {
                            if (inventoryExists)
                            {
                                Console.Clear();
                                string addHeader = "Which DVD would you like to place in your cart?";
                                Dvd tmpDvd = instance.PickDvd(addHeader, inventory);
                                inventory.Remove(tmpDvd);
                                shoppingCart.Add(tmpDvd);
                                inventoryExists = (inventory.Count > 0);
                                shoppingCartHasDvd = (shoppingCart.Count > 0);
                                shoppingCart.Sort();
                                Console.Clear();
                                Console.WriteLine($"{tmpDvd.Title} has been moved from store inventory to your shopping cart.");
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("You put everything in your cart. Wow...");
                            }
                            break;
                        }
                    case "remove dvd from cart":
                        {
                            if (shoppingCartHasDvd)
                            {
                                Console.Clear();
                                string removeHeader = "Which DVD would you like to to remove from your cart?";
                                Dvd tmpDvd = instance.PickDvd(removeHeader, shoppingCart);
                                inventory.Add(tmpDvd);
                                shoppingCart.Remove(tmpDvd);
                                inventoryExists = (inventory.Count > 0);
                                shoppingCartHasDvd = (shoppingCart.Count > 0);
                                inventory.Sort();
                                Console.Clear();
                                Console.WriteLine($"{tmpDvd.Title} has been moved from your shopping cart to store inventory.");
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("There are no DVDs in your cart.\r\nPlease add a DVD to your cart.");
                            }
                            break;
                        }
                    case "exit":
                        {

                            break;
                        }

                    default:
                        break;
                }



            }

            //End of the main program
            Console.WriteLine("Thank you for shopping. Press any key to continue.");
            Console.ReadKey();
        }

        void Connect()
        {
            BuildConString();

            try
            {
                Con.Open();
                Console.WriteLine("Connection successful!");
            }
            catch(MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + Con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username or password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }
                Console.WriteLine(msg);
                
            }

        }
        // Builds a connection string for accessing the database
        void BuildConString()
        {
            string ip = "";
            //using this to pull the IP for the connection string
            using(StreamReader sr = new StreamReader(@"C:\VFW\connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string conString = $"Server={ip};";
            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=exampleDatabase;";
            conString += "port=8889";

            Con.ConnectionString = conString;
            
        }
        
        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, Con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }

        void ViewDvds(List<Dvd> list)
        {
            foreach (var Dvd in list)
            {
                Console.WriteLine(Dvd);
            }
        }

        Dvd PickDvd(string header, List<Dvd> list)
        {
            List<string> options = new List<string>();
            foreach (var dvd in list)
            {
                options.Add(dvd.Title);
            }

            NumberMenu menu = new NumberMenu(header, options);

            int option = menu.Display();
            Dvd tmpDvd = list[option - 1];
            return tmpDvd;

        }

    }
}

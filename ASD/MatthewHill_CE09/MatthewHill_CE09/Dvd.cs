﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE09
{
    class Dvd : IComparable<Dvd>
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public float Rating { get; set; }

        public Dvd() { }
        public Dvd(string title, decimal price, float rating)
        {
            Title = title;
            Price = price;
            Rating = rating;
        }


        public override string ToString()
        {
            string str = $"Title: {Title} \tPrice: {Price}\tRating:{Rating}";
            return str;
        }

        public int CompareTo(Dvd next)
        {
            return this.Title.CompareTo(next.Title);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE02
{
    class Customer
    {
        //Constructors
        public Customer() { }
        public Customer(string customerName)
        {
            CustomerName = customerName;
        }


        private string customerName;
        private CheckingAccount customerAccount;
        //getters and setters
        public string CustomerName
        {
            get
            {
                return customerName;
            }

            set
            {
                customerName = value;
            }
        }
        //getters and setters
        public CheckingAccount CustomerAccount
        {
            get
            {
                return customerAccount;
            }

            set
            {
                customerAccount = value;
            }
        }
        public override string ToString()
        {
            return "Name: " + CustomerName;
        }
    }


    


    class CheckingAccount
    {
        int accountNumber;
        decimal accountBalance;

        public CheckingAccount() { }
        public CheckingAccount(int accountNumber)
        {
            AccountNumber = accountNumber;
            AccountBalance = 0;
        }

        public CheckingAccount(int accountNumber, decimal accountBalance)
        {
            AccountNumber = accountNumber;
            AccountBalance = accountBalance;
        }


        //getters and setters for AccountNumber.
        public int AccountNumber
        {
            get
            {
                return accountNumber;
            }

            set
            {
                accountNumber = value;
            }
        }
        //getters and setters for AccountBalance.
        public decimal AccountBalance
        {
            get
            {
                return accountBalance;
            }

            set
            {
                accountBalance = value;
            }
        }
        //ToString override so we can easily get all the bank data for the customer.
        public override string ToString()
        {
            return "Account Number: "+ AccountNumber + "\r\nAccount Balance: " + AccountBalance;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE02
{
    class Program
    {
        static void Main(string[] args)
        {
            int menuOption;
            Customer customer1 = new Customer();
            //This is the while loop to keep the main program running
            while (true)
            {   //This finds the
                menuOption = PickMenuOption();
                //This ends the main body loop if 5 is returned from the PickFishColor method
                switch (menuOption)
                {
                    case 1: //This checks if customer1 has a name before allowing customer creation.
                        if (customer1.CustomerName == null)
                        {
                            customer1.CustomerName = CreateCustomerName();
                        }
                        else
                        {
                            Console.WriteLine("You already have a customer named " +customer1.CustomerName + ".\r\n\r\n");
                        }
                        break;

                    case 2: //This checks if customer1 has a name before allowing account creation.
                        if (customer1.CustomerName == null )
                        {
                            Console.WriteLine("You have not created a customer.\r\n\r\n");
                        }
                        else if (customer1.CustomerAccount != null)
                        {
                            Console.WriteLine("You have already created an account.\r\n\r\n");
                        }
                        else
                        {
                            customer1.CustomerAccount = CreateCheckingAccount();
                        }
                       break;

                    case 3:
                        if (customer1.CustomerName == null)
                        {
                            Console.WriteLine("You have not created a customer.\r\n\r\n");
                        }
                        else if (customer1.CustomerAccount == null)
                        {
                            Console.WriteLine("You have not created an account.\r\n\r\n");
                        }
                        else
                        {
                            customer1.CustomerAccount.AccountBalance = SetAccountBalance();
                        }
                        break;

                    case 4:
                        if (customer1.CustomerName == null)
                        {
                            Console.WriteLine("You have not created a customer.\r\n\r\n");
                        }
                        else if (customer1.CustomerAccount == null)
                        {
                            Console.WriteLine("You have not created an account.\r\n\r\n");
                        }
                        else
                        {
                            Console.WriteLine(customer1.CustomerName);
                            Console.WriteLine(customer1.CustomerAccount);
                        }
                        
                        break;

                    case 5:
                        break;


                    default:
                        break;
                }
                //This ends the main loop if menu option is 5
                if (menuOption == 5)
                {
                    break;
                }


            } //End of the main program while loop

            Console.WriteLine("\r\nThank you for using ASD accounting!\r\nPress any key to exit.\r\n");
            Console.ReadKey();
            
        }

        public static int PickMenuOption()
        {

            int option;

            while (true)
            {
                Console.WriteLine("Welcome to ASD Accounting.");
                Console.WriteLine("Please select an option.");
                Console.WriteLine("***************************");
                Console.WriteLine("* 1. Create customer");
                Console.WriteLine("* 2. Create account");
                Console.WriteLine("* 3. Set account balance");
                Console.WriteLine("* 4. Display account balance");
                Console.WriteLine("* 5. End Program");
                Console.WriteLine("**************************");
                string input = Console.ReadLine();
                //This tries to parse the input as an integer
                if (int.TryParse(input, out option))
                {
                    //This makes sure the integer is number 1-5
                    if (option == 1 || option == 2 || option == 3 || option == 4 || option == 5)
                    {
                        return option;
                    }
                    //Error Code
                    else
                    {
                        Console.WriteLine(option + " is not a valid option. Please select a number between 1 and 5");
                    }
                    //Error Code
                }
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please select a number between 1 and 5.");
                }
            }


        }


        public static string CreateCustomerName()
        {

            string input = "";
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Please enter the customer's name.");
                input = Console.ReadLine();
            }

            return input;
        }

        public static CheckingAccount CreateCheckingAccount()
        {

            int newAccountNumber;
        
                while (true)
                { //gets the account number
                    Console.WriteLine("Please input the account number.");
                    
                    string input = Console.ReadLine();
                    //This tries to parse the input as an integer
                    if (int.TryParse(input, out newAccountNumber))
                    {
                    break;
                    }//Error Code
                    else
                    {
                        Console.WriteLine(input + " is not a valid option. Please give an integer value for the account number.");
                    }
                }

            decimal openingBalance;

            while (true)
            {  //gets the account starting amount
                Console.WriteLine("Please input the initial deposit amount.");

                string input = Console.ReadLine();
                //This tries to parse the input
                if (decimal.TryParse(input, out openingBalance))
                {
                    break;
                }//Error Code
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please give a decimal value for the account balance.");
                }
            }

            CheckingAccount tempAccount = new CheckingAccount(newAccountNumber, openingBalance);
            return tempAccount;

        }

        public static decimal SetAccountBalance()
        {
            decimal accountBalance;

            while (true)
            {  //gets the account amount
                Console.WriteLine("Please input the current account balance.");

                string input = Console.ReadLine();
                //This tries to parse the input
                if (decimal.TryParse(input, out accountBalance))
                {
                    return accountBalance;
                }//Error Code
                else
                {
                    Console.WriteLine(input + " is not a valid option. Please give a decimal value for the account balance.");
                }
            }

        }

    }
}

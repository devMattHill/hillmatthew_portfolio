from django.urls import path
from . import views

urlpatterns = [
    #ex: /polls/
    path('',views.index, name='index'),
    #this takes in the 'stuff/' ending then looks in the views.py 
    # file for the 'stuff' method and runs it i made this one for kicks/testing
    path('stuff/',views.stuff, name='stuff'),
    #the real stuff is down here
    #ex: /polls/5/
    path('<int:question_id>/', views.detail, name='detail'),
    #ex: /polls/5/results
    path('<int:question_id>/results', views.results, name='results'),
    #ex: /polls/5/vote
    path('<int:question_id>/vote', views.vote, name='vote'),
]
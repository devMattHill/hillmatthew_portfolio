﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array_Assignment_Starting_File
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Matthew Hill
             #02
             5 May 2017
             Arrays Assignment
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            //Declare and Define The Starting Number Arrays
            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //Find the total of each array and store it in a variable and output to console


            /*
              
             Section 1

            */
            Console.WriteLine();
            Console.WriteLine("--------------Section 1--------------");
            Console.WriteLine();
            //This declares the variable that is used for totaling the firstArray.
            double totalInt = 0;
            //This totals up the firstArray.
            totalInt += firstArray[0];
            totalInt += firstArray[1];
            totalInt += firstArray[2];
            totalInt += firstArray[3];



            //This declares the variable that is used for totaling the secondArray.
            double totalDouble = 0;
            //This totals up the secondArray.
            totalDouble += secondArray[0];
            totalDouble += secondArray[1];
            totalDouble += secondArray[2];
            totalDouble += secondArray[3];


            //The following will display the array totals.
            Console.WriteLine("The total of the firstArray is "+totalInt+".\r\nThe total for the secondArray is "+totalDouble+".");
            Console.WriteLine();
            Console.WriteLine("--------------------------------------");



            /*
             
             Section 2
             
             */

            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.

            //This declares and stores the average of the firstArray.
            double firstAverage = totalInt / firstArray.Length;
            //This declares and stores the average of the secondArray.
            double secondAverage = totalDouble / secondArray.Length;

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("--------------Section 2--------------");
            Console.WriteLine();

            //This displays the array averages.
            Console.WriteLine("The average of the firstArray is " + firstAverage + ".\r\nThe average for the secondArray is " + secondAverage + ".");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------");



            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */

            /*
             
             Section 3
             
             */

            //This builds the thirdArray.
            double[] thirdArray = new double[4] { firstArray[0]+secondArray[0], firstArray[1] + secondArray[1], firstArray[2] + secondArray[2], firstArray[3] + secondArray[3] };

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("--------------Section 3--------------");
            Console.WriteLine();
            //This displays the third array values.
            Console.WriteLine("The following are the values of the third array.");
            Console.WriteLine("{ "+ thirdArray[0] + ", "+thirdArray[1] + ", "+thirdArray[2] + ", "+thirdArray[3]+"}");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------");




            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            /*
             
            Section 4
             
             */


            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("--------------Section 4--------------");

            Console.WriteLine();
            //Declare and Define The String Array
            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };
            //Organize the array into the string
            string iEnglishGood = MixedUp[1] +" "+ MixedUp[3] + " " + MixedUp[4] + " " + MixedUp[0] + " " + MixedUp[2];
            //Display the answer.
            Console.WriteLine(iEnglishGood);
            Console.WriteLine();
            Console.WriteLine("--------------------------------------");



        }
    }
}

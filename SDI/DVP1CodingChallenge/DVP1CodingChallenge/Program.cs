﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DVP1CodingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {

//from here
            /*
             Main Menu
             */
            string menuOptionStr = "";
            int menuOption = 0;
            Console.WriteLine("\r\n\r\n\r\nWelcome to the DVP1 Coding Challenge");
            while (menuOption != 5)
            {
                menuOptionStr = "";
                menuOption = 0;

                while (menuOption != 1 && menuOption != 2 && menuOption != 3 && menuOption != 4 && menuOption != 5)
                {

                    while (!int.TryParse(menuOptionStr, out menuOption))
                    {
                        
                        Console.WriteLine("\r\n\r\nChoose the tool you would like to run by typing the number and pressing enter.");
                        Console.WriteLine("------------------------------");
                        Console.WriteLine("1. SwapName");
                        Console.WriteLine("2. Backwards");
                        Console.WriteLine("3. AgeConvert");
                        Console.WriteLine("4. TempConvert");
                        Console.WriteLine("5. End Program");
                        Console.WriteLine("------------------------------");
                        menuOptionStr = Console.ReadLine();
                    }
                }

                if (menuOption == 1)
                {
                    ArrayList name = new ArrayList();
                    string name1st = StringInput("\r\n\r\nPlease enter your first name.");
                    name.Add(name1st);
                    string nameLast = StringInput("Please enter your last name.");
                    name.Add(nameLast);
                    ArrayList swappedNames = SwapName(name);
                    Console.WriteLine("\r\nYour name is {0} {1}.",name[0],name[1]);
                    Console.WriteLine("\r\nFor indexing purposes your name will be filed as follows:\r\n{0}, {1}",swappedNames[0],swappedNames[1]);
                }//This is the end of menu option 1
                else if (menuOption == 2)
                {
                    string sentence = StringInput("\r\n\r\nPlease type a sentence and I will reverse the word order for you.\r\nAny punctuation next to a word will remain attached.\r\n\r\n");
                    Console.WriteLine("\r\n\r\n\r\nYou wrote the following:\r\n{0}",sentence);
                    string reverSentence = Backwards(sentence);
                    Console.WriteLine("\r\nYour reversed sentence is:\r\n{0}",reverSentence);
                    

                }//This is the end of menu option 2
                else if (menuOption == 3)
                {
                    string bDayName = StringInput("\r\n\r\nPlease enter your name in the format you wish to have it displayed.");

                    string bDayString = "";
                    DateTime bday;

                    //Validating the input as a DateTime
                    while (!DateTime.TryParse(bDayString, out bday))
                    {
                        bDayString = StringInput("\r\nWhen is your birthday?\r\nPlease use the following format:\r\nmm/dd/yyyy hh:mm:ss\r\nYou may skip the time if you do not know it.");
                    }
                    string ageOutPut = AgeConvert(bDayName, bday);
                    Console.WriteLine("\r\n\r\n"+ageOutPut);

                }//This is the end of menu option 3
                else if (menuOption == 4)
                {
                    int count = 0;
                    string tempType;
                    while (count <2)
                    {
                        if (count == 0) {
                            tempType = "Farenheit";
                        }else
                        {
                            tempType = "Celsius";
                        }
                    
                        string tempInput = "";
                        decimal tempNum;
                        string tempConverted = "";
                        string inputMessageTemp = "\r\n\r\nPlease input a temperature in degrees " + tempType +".";
                        while (!decimal.TryParse(tempInput, out tempNum))
                        {
                            tempInput = StringInput(inputMessageTemp);
                        }
                        tempConverted = TempConvert(tempNum, tempType);

                        Console.WriteLine("\r\n{0} {1} is {2}",tempInput,tempType, tempConverted);
                    
                        count++;
                   }
                    
                } //This is the end of menu option 4
                else if (menuOption == 5)
                {//This stops the program.
                    Console.WriteLine("Thank you, have a nice day!");
                    break;
                }
              
            }  //This is the ending bracket for the while loop that uses menu option 5 to close the program
            
        } //This is the end of the main method


        //First Required Method
        //This method can technically reverse the order of any ArrayList regardless of length. It then returns the reversed array.
        public static ArrayList SwapName(ArrayList unswappedName)
        {
            //Declare the array that will be returned
            ArrayList swap = new ArrayList();
            //Loop for each object in the arraylist
            foreach(string name in unswappedName)
            {
                //puts each looped item in the 0 index position of the new array
                swap.Insert(0, name);
            }
            return swap;
        }
        //Wanted a repeat use string input loop w/out writing it every time.
        public static string StringInput(string question)
        {
            string answer = "";
            while (string.IsNullOrWhiteSpace(answer))
            {
                Console.WriteLine(question);
                answer = Console.ReadLine();
            }
            return answer;
        }
        

        //Second required method
        public static string Backwards(string frontWardString)
        {
            char splitter = ' ';
            ArrayList sentenceArray = new ArrayList(frontWardString.Split(splitter));
            //found out there is a .Reverse function. Otherwise I would send the sentenceArray to the custom method SwapName.
            sentenceArray.Reverse();
            string backWardSentence = "";
            foreach (string word in sentenceArray)
            {
                backWardSentence += word+" ";
            }
            return backWardSentence;
            
        }
        //Third required method
        public static string AgeConvert(string ageName, DateTime bDay)
        {
            Age usersAge = new Age(bDay);
            string bDayOutput = "";
           
            bDayOutput = ageName + " you are " + usersAge.InDays + " days old.\r\n" + ageName + " you are " + usersAge.InHours + " hours old.\r\n" + ageName + " you are " + usersAge.InMinutes + " minutes old.\r\n" + ageName + " you are " + usersAge.InSeconds + " seconds old.\r\n";
            
            return bDayOutput;
        }
        //Fourth required method
        public static string TempConvert(decimal temp, string cOrF)
        {
            string resTemp;
            decimal tempFinal;
            if (cOrF == "Celsius")
            {
                tempFinal = Math.Round((temp * 1.8m) + 32m,2);
                resTemp = "Farenheit";
            }
            else
            {
                tempFinal = Math.Round((temp - 32) / 1.8m,2);
                resTemp = "Celsius";
            }
           
            return tempFinal + " " + resTemp;
        }

    }
}

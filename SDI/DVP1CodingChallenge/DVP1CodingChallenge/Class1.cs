﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DVP1CodingChallenge
{
    class Age
    {
        //Preparing to store the users birth date
        private DateTime mbDay;

        //Used to instantiage an age. 
        public Age(DateTime _bDay)
        {
            mbDay = _bDay;
        }

        //Age in Days
        public double InDays
        {
            get
            {
                TimeSpan timeDifference = DateTime.Now.Subtract(mbDay);
                return Math.Floor(timeDifference.TotalDays);
            }
        }

        //Age in Hours
        public double InHours
        {
            get
            {
                TimeSpan timeDifference = DateTime.Now.Subtract(mbDay);
                return Math.Floor(timeDifference.TotalHours);
            }
        }

        //Age in Minutes
        public double InMinutes
        {
            get
            {
                TimeSpan timeDifference = DateTime.Now.Subtract(mbDay);
                return Math.Floor(timeDifference.TotalMinutes);
            }
        }

        //Age in seconds.
        public double InSeconds
        {
            get
            {
                TimeSpan timeDifference = DateTime.Now.Subtract(mbDay);
                return Math.Floor(timeDifference.TotalSeconds);
            }
        }
        



    }
}

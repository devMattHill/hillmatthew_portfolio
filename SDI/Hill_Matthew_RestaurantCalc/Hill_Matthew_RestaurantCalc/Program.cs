﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {


            /*
             Matthew Hill
             #02
             6 May 2017
             RestaurantCalc Assignment
            */

            //Inform user what we are doing.
            Console.WriteLine("\r\n\r\n-------------------------------------------------------------------------------------");
            Console.WriteLine("Hello, I am Restaurant Calculator and I'm here to help!");
            Console.WriteLine("-------------------------------------------------------------------------------------\r\n\r\n");

            //Declare Price Array
            double[] mealPrices = new double[3];


            //Ask for price 1.
            Console.WriteLine("\r\nInput the price of the first meal and press enter.");
            //Store price 1.
            mealPrices[0] = double.Parse(Console.ReadLine());

            //Ask for price 2.
            Console.WriteLine("\r\nInput the price of the second meal and press enter.");
            //Store price 2.
            mealPrices[1] = double.Parse(Console.ReadLine());

            //Ask for price 3.
            Console.WriteLine("\r\nInput the price of the third meal and press enter.");
            //Store price 3.
            mealPrices[2] = double.Parse(Console.ReadLine());

            //Determine the tip percentage.
            Console.WriteLine("\r\nBased on your experience today, what tip percentage do you want to add?");
            Console.WriteLine("Common practice is between 15% and 20%.");
            Console.WriteLine("Input it as a normal number and press enter.   Examples: \"17\" or \"18\"");
            double tip = double.Parse(Console.ReadLine());
            double tipPercent = tip / 100;

                    // string.Format("{0:0.00}",number)  <-- that bad boy pretties up numbers for display as common monetary values #money #rounding

            Console.WriteLine("\r\n\r\n-------------------------------------------------------------------------------------");
            //Display Meal Totals
            Console.WriteLine("\r\nMeal 1: $"+ string.Format("{0:0.00}", mealPrices[0],2)+"\r\nMeal 2: $"+ string.Format("{0:0.00}", mealPrices[1])+"\r\nMeal 3: $"+ string.Format("{0:0.00}", mealPrices[2]));
            //Calculate and Display Subtotal
            double mealSubtotal = mealPrices[0] + mealPrices[1] + mealPrices[2];
            Console.WriteLine("Sub-Total With No Tip: $"+ string.Format("{0:0.00}", mealSubtotal));
            //Calculate tip.
            double tipTotal = mealSubtotal * tipPercent;
            //Display Tip Percentage and total.
            Console.WriteLine("Tip: "+tip+"%   Tip amount: $"+ string.Format("{0:0.00}", tipTotal));
            //Calculate and Display final total
            double mealTotal = mealSubtotal + tipTotal;
            Console.WriteLine("Final Total: $"+ string.Format("{0:0.00}", mealTotal));
            //Calculate and display cost per person if split evenly.
            double evenSplit = mealTotal / 3;
            Console.WriteLine("Cost per person: $" + string.Format("{0:0.00}", evenSplit));
            Console.WriteLine("-------------------------------------------------------------------------------------");
            Console.WriteLine("Thank you for using Restaurant Calculator!");
            Console.WriteLine("-------------------------------------------------------------------------------------\r\n\r\n");
            


            /*
             Below are the test values given.
                Test	#1
                • Check	#1	– 10.00
                • Check#2	– 15.00
                • Check#3	– 25.00
                • Tip	%	- 20
                • Sub-Total	Without	Tip	– 50.00
                • Total	Tip	– 10.00
                • Grand	total	With	Tip	– 60.00
                • Cost	per	person	– 20.00
                Test	#2
                • Check	#1	– 20.25
                • Check#2	– 17.75
                • Check#3	– 23.90
                • Tip	%	- 10
                • Sub-Total	Without	Tip	– 61.90
                • Total	Tip	– 6.19
                • Grand	total	With	Tip	– 68.09
                • Cost	per	person	– 22.69666	or	rounded	22.70 
                          
             */

        }
    }
}

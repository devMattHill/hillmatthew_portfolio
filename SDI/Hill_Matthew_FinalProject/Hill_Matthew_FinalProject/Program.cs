﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {



            /*
            Matthew Hill
            Scalable Data Infrastructures
            #02
            25 May 2017
            Final Project
             */

            //welcome and explanation
            Console.WriteLine("Welcome to the Calorie Tracker");
            Console.WriteLine("In a moment you will enter your daily calorie intake and your intake goal.");
            Console.WriteLine("You will then be given the results of your intake for the day");
            Console.WriteLine("Thank you for choosing Calorie Tracke and good luck!");




            //ask for number of meals this will be an argument sent to GetCalories  

            string inputStr = "";
            int inputNum = -1;
            //I want the user to be able to input 0 so i'm allowing all integers greater than 0
            while (inputNum < 0)
            {
                inputStr = "";
                //This attempts to turn the input into an integer and repeats itself if it can't
                while (!int.TryParse(inputStr, out inputNum))
                {

                    Console.WriteLine("How many meals have you eaten today?");
                    inputStr = Console.ReadLine();


                }

            }

            if (inputNum < 1) {
                Console.WriteLine("Oh wow that's horrible! Please stop what you are doing and eat something.");
            }else {

                //this is the array sent back from the GetCalories method 
                ArrayList mealsByCalories = GetCalories(inputNum);

                //This sends the meals array as an argument to the TotalCalories method and gets an int returned
                int caloriesConsumed = TotalCalories(mealsByCalories);
                if (caloriesConsumed < 1) {
                    Console.WriteLine("Oh wow that's horrible! Please stop what you are doing and eat something.");
                }else { 


                        //ask the user for the total number of calories they wanted to consume, fysa asking someone how many calories they wanted after the fact is bad form and in bird culture this is considered "rude"

                        string wantedStr = "";
                        int wantedNum = 0;
                        //Debated allowing the user to input 0 and tell them to see a doctor then decided against it, we just won't allow it as an option.
                        while (wantedNum < 1)
                        {
                            wantedStr = "";
                            //This attempts to turn the input into an integer and repeats itself if it can't
                            while (!int.TryParse(wantedStr, out wantedNum))
                            {

                                Console.WriteLine("\r\n\r\nPlease enter your daily calorie goal.");
                                wantedStr = Console.ReadLine();
                            
                            }
                        }




                    //Send the two int arguments of calorieTotal and caloriesWanted to the CaloriesLeft method and retrieve an int if this number is negative they have over eaten and we should tell them as such
                    int remaining = CaloriesLeft(caloriesConsumed,wantedNum);
                    //Final Display
                    // debated on a third output for 0 but chose -1 for the if statement so if the user has 0 calories remaining the out put will just say as such
                    if (remaining > -1) {
                        Console.WriteLine("\r\n\r\nYou still have {0} calories left today.",remaining);
                    }else{
                        Console.WriteLine("\r\n\r\nYou have over eaten by {0} calories today.",remaining*-1);


                    }












                }
            }

        }

        //GetCalories takes in the int parameter of number of meals eaten it then runs a loop that many times asking for all the calorie amounts and returns and arraylist with those numbers

        public static ArrayList GetCalories(int mealCount) {
            ArrayList munches = new ArrayList();

            // in this loops I started i at 1 so i can use it in the input request sentence. I also added 1 to the meal count number to balance it out.
            for(int i = 1 ;  i < mealCount+1; i++) { 

                string inputStr = "";
                int inputNum = -1;
                //I want the user to be able to input 0 so i'm allowing all integers greater than 0
                while (inputNum < 0)
                {
                    inputStr = "";
                    //This attempts to turn the input into an integer and repeats itself if it can't
                    while (!int.TryParse(inputStr, out inputNum))
                    {

                        Console.WriteLine("\r\n\r\nPlease enter the total number of calories you consumed during meal number {0}.", i);
                        inputStr = Console.ReadLine();


                    }

                }
                munches.Add(inputNum);
            }
            return munches;
        }



        //TotalCalories runs a forEach loop for the arraylist parameter and adds all of the values together then returns the int value to the main method

        public static int TotalCalories(ArrayList meals) {
            int consumption = 0;
            foreach (int meal in meals) {
                consumption += meal;
            }
            return consumption;
        }

        //CaloriesLeft takes in two int parameters of eaten and wanted. It compares them (subtraction) and determines if the user can eat more or should be notified that they are over

        public static int CaloriesLeft(int eaten, int goal) {

            int remaining = goal - eaten;
            return remaining;

        }


        /*
         10. Test,	Test,	Test	
            a. Test	your	code	at	the	very	least	3	times	and	put	your	Test	Results	in	a	multi-lined	
            comment	at	the	end	of	your	Main	Method.
            b. Here	is	one	data	set	that	you	should	test	with	to	make	sure	your	math	is	correct.
            i. #	of	meals =3
            ii. 1st Meal Calories=	800,		2nd Meal Calories =	1000,	3rd Meal Calories =	1200
            iii. Calories	they	wanted	to	consume	- 2500
            iv. Final	Output	should	be:
            1. “You	have	over	eaten	by	500 calories	today
         
            Test1
                8 meals at 200 calories
                Calorie goal 2000
                You still have 400 calories left today.
            Test2
                meal 1   300
                meal 2   300
                meal 3   300
                meal 4   1000
                Calorie goal 1800
                You have over eaten by 100 calories today.
            Test3
                meal 1   1500
                meal 2   1500
                Calorie goal 3000
                You still have 0 calories left today.
         
         
         */









    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Mathew_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {




            /*
            Matthew Hill
            #02
            20 May 2017
            String Objects Assignment
            */

            //Declare Variables for the menu.
            string menuOptionStr = "";
            int menuOption = 0;
            //This loop makes sure the user must pick a number 1 - 3
            while (menuOption != 1 && menuOption != 2)
            {
                //This loop parses the 1 - 3 as an integer into the variable 'menuOption'
                while (!int.TryParse(menuOptionStr, out menuOption))
                {
                    Console.WriteLine("Welcome to the StringObjects Tool Suite!");
                    Console.WriteLine("Choose the tool you would like to use. \r\nType the number next to the tool and press enter.");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("1. Email Address Checker");
                    Console.WriteLine("2. Separator Swap Out");
                    Console.WriteLine("------------------------------");
                    menuOptionStr = Console.ReadLine();
                }
            }

            /* 
           Start the if then set for running the different tools.
            */

            if (menuOption == 1)
            {
                //Begin

                string emailQuestStr = "";

                while (string.IsNullOrWhiteSpace(emailQuestStr))
                {
                    //Console.WriteLine("made it to the while loop");
                    Console.WriteLine("Please enter an email address for validation.");

                    emailQuestStr = Console.ReadLine();
                }
                //Send the input email as an argument for validation in a different method
                string validation = ValidateEmail(emailQuestStr);

                Console.WriteLine("The email address of {0} is {1} email address.", emailQuestStr, validation);

                /*
                • Data	Sets	To	Test	
                o Email	– test@fullsail.com
                § Results	- “The	email	address	of	test@fullsail.com	is	a	valid email	address.”
                o Email	– test@full@sail.com
                § Results	- “The	email	address	of	test@full@sail.com	is	not	a	valid email	
                address.”
                o Email	– test@full	sail.com
                § Results	- “The	email	address	of	test@full	sail.com	is	not	a	valid email	
                address.”
                o Now	try	an	email	of	your	own 
                Email - kittens@tower.tall
                The email address of kittens@tower.tall is a valid email address.
                I couldn't resist, we got two new kittens today and bought them a 6 foot tower to climb, it's the most adorable thing ever.
                
                */



                //End
            }
            else if (menuOption == 2)
            {
                //Begin
                

                char separator;
                string sepInput = "";
                //loops that asks for and verifies a separator
                while (!char.TryParse(sepInput, out separator))
                {
                    //Console.WriteLine("made it to the while loop");
                    Console.WriteLine("Please enter the separator you would like to use for your list.\r\nInput:");
                    sepInput = Console.ReadLine();
                }
            
                string inputStr = "notwhitespace";
                ArrayList listOfThings = new ArrayList();
                //loop that allows "infinite" input of items into an arraylist
                while (!string.IsNullOrEmpty(inputStr))
                {
                    //Console.WriteLine("made it to the while loop");
                    Console.WriteLine("Please enter an item to add your list.\r\nWhen you are finsihed press enter while the input is blank.\r\nInput:");
                    inputStr = Console.ReadLine();
                    if (!string.IsNullOrEmpty(inputStr)) { 
                        listOfThings.Add(inputStr);
                    }
                }

                //organizes the array into a string by sending the arguments listOfThings and the separator.
                string list1 = FrontArray(listOfThings, separator);
                
                //Console.WriteLine(list1);

                char newSeparator;
                string newSepInput = "";
                //loop that asks for and verifies the new separator
                while (!char.TryParse(newSepInput, out newSeparator))
                {
                    //Console.WriteLine("made it to the while loop");
                    Console.WriteLine("Please enter the second separator you would like to use for your list.\r\nInput:");
                    newSepInput = Console.ReadLine();
                }
                //sends the arguments original list, separator and new separator to get the separators swapped.
                string list2 = ReplaceSeparator(list1, separator, newSeparator);
                //Console.WriteLine(list2);

                Console.WriteLine("The original string of {0} with the new separator is {1}.",list1,list2);


                /*
                   • Data	Sets	To	Test	
                    o List	– “1,2,3,4,5”	orginalSeparator	– “,”	newSeparator	– “-”
                    § Results	– “The	original	string	of	1,2,3,4,5 with	the	new	separator	is	1-2-3-
                    4-5.”
                    o List	– “red:	blue:	green:	pink”	orginalSeparator	– “:”	newSeparator	– “,”
                    § Results	– “The	original	string	red:	blue:	green:	pink	with	the	new	
                    separator	is	red,	blue,	green,	pink.”
                    o Now	try	one	of	your	own.
                    
                    List 213141516  originalSeparator "1" newSeparator "5" 
                    The original string 213141516 with the new separator is 253545556.
                */


                //End
            }


        }

        public static string ValidateEmail(string email)
        {
            
            /*
                1. Only	one	@	symbol in	the	whole	string
                2. No	spaces
                3. At	least	one	dot	after	the	@	symbol
                4. There	is	no	need	to	check	for	.com,	.edu,	etc.	
                5. The	code	should	work	for	any	string	entered.	
             */

            //variables for each rule
            bool a; //Exactly one @ symbol in the whole string
            bool b; //No Spaces
            bool c; //At least one dot after the @ symbol
            bool d; //Does not start with the @ symbol
            bool e; //Does not end with the . 

            //This line uses .Replace to remove the @ symbol and confirm that only 1 was removed from the original string
            a = ((email.Length - email.Replace("@","").Length) == 1);
            //This line uses .Replace to remove any spaces and confirm that zero were removed from the original string
            b = ((email.Length - email.Replace(" ", "").Length) == 0);
            //This confirms that the first period is at least two spaces to the right of the @ sign.
            c = ((email.IndexOf(".") - email.IndexOf("@")) >= 2);
            //Checks that the index of @ is not the first character in the string
            d = (email.IndexOf("@") > 0);
            //Checks that the . is not the last character in the string
            e = (email.LastIndexOf(".") < email.Length - 1);

            //This line checks that ALL rules are true and stores that value to the variable 'valid' for return to the main method
            bool truthValue = (a && b && c && d && e);
            string validity;

            if (truthValue == true)
            {
                validity = "a valid";
            }
            else {
                validity = "not a valid";
            }

            return validity;
        }

        //Sets up the given array into a string for display
        public static string FrontArray(ArrayList frontways, char separator)
        {
            string frontStr = "";
            int i = 0;
            //this parses the array into a string and ...
            foreach (string thing in frontways)
            {
                frontStr += thing;
                //...adds original separators after all but the last item in the array
                if (i < frontways.Count - 1)
                {
                    frontStr += separator;
                }
                i++;
            }
            return frontStr;
        }
        //uses the string.Replace command to replace firstSeparator with secondSeparator
        public static string ReplaceSeparator(string list, char first, char second) {

            string replaced = list.Replace(first, second);

            return replaced;
        }
    }
}

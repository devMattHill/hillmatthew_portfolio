﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Matthew Hill
             #02
             6 May 2017
             RestaurantCalc Assignment
            */


            /*
            Female name heroine.

            Verb   gardenVerb

            Unit of measurment for time. time

            Number.   How long the romanceLasts.

            Number distanceTraveled

            Male name capturedPrince

            Unit of measurement for length. To be used as distanceMeasure.

            Number greater than 10 To be used as mythicalBeastsAmount.

            Male Name badGuy.

            Unit of measurement for length. buildingMeasure.

            Number greater than 10   buildingHeight

            Name.   mountName.

            A riding animal mountType

            Weapon weapon.

            Mythical Beast  mythicalBeast.

            Name of place country1

            Name of place country2

            Exclamation  gardenExclamation

            Number greater than 10 nightsAndDays

            Building type.    castle

            Adverb  laughAdverb

            Adjective mountDescription

            Furniture furniture

            */

            /*
             "Once upon a time in the royal land of "+ country1 +". Prince "+ capturedPrince +" was in the royal garden "+ gardenVerb +"ing the flowers. Suddenly, the Dark Lord + badGuy + appeared. On a flying "+ furniture +". "
             + gardenExclamation +"! exclaimed the startled Prince + capturedPrince + as the Dark Lord swept him away laughing "+ laughAdverb +". As soon as the Lady "+ heroine +" heard of this she grabbed her "+ weapon +"
             and went to the stables to find her trusty steed "+ mountName +" the "+ mountDescription +"est "+ mountType +" in "+ country1 +". After "+ nightsAndDays +" days and "+ nightsAndDays +" nights "+ heroine +" and "+ mountName +" managed to travel the "+ distanceTraveled +" "+ distanceMeasured +"s to the Dark Lord's home land
             of "+ country2 +". They stared in disbelief at the "+ mythicalBeastsAmount +" "+ mythicalBeast +"s guarding "+ badGuy +"'s "+ buildingHeight +" "+ buildingMeasure +" "+ castle +". "+ heroine +" made quick work of the "+ mythicalBeast +" while "+ mountName +" bashed in the door.
             "+ heroine +" found "+ capturedPrince +" chained inside with no sign of "+ badGuy +" anywhere. \"You're free\" she said, lets go \"And, don't follow me around like the last time.\" But, he did exactly that. For nearly "+ romanceLasts +" "+ time +"s the prince was smitten with "+ heroine +" before giving up and returning to his castle."
             
             
             
             
             */


            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("-----------------------------------------------------------Welcome to MattLibs---------------------------------------------------------------------");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------\r\n\r\n");
            Console.WriteLine("                                               Press Enter to begin building your story!");
            string str = Console.ReadLine();



            //Female name heroine.
            Console.WriteLine("\r\nPlease enter a female name and press enter.");
            string heroine = Console.ReadLine();

            //Verb   gardenVerb
            Console.WriteLine("\r\nPlease enter a verb that ends in \"ing\"and press enter.");
            string gardenVerb = Console.ReadLine();

            //Unit of measurment for time. time
            Console.WriteLine("\r\nPlease enter a plural unit of time such as \"seconds\" or \"decades\" and press enter.");
            string time = Console.ReadLine();

            //Number.   How long the romanceLasts.
            Console.WriteLine("\r\nPlease enter a number greater than 10 and press enter.");
            double romanceLasts = double.Parse(Console.ReadLine());

            //Number distanceTraveled
            Console.WriteLine("\r\nPlease enter a number greater than 10 and press enter.");
            double distanceTraveled = double.Parse(Console.ReadLine());

            //Male name capturedPrince
            Console.WriteLine("\r\nPlease enter a male name and press enter.");
            string capturedPrince = Console.ReadLine();

            //Unit of measurement for length. To be used as distanceMeasure.
            Console.WriteLine("\r\nPlease enter a plural unit of length such as \"inches\" or \"miles\"and press enter.");
            string distanceMeasure = Console.ReadLine();

            //Number greater than 10 To be used as mythicalBeastsAmount.
            Console.WriteLine("\r\nPlease enter a number greater than 10 and press enter.");
            double mythicalBeastsAmount = double.Parse(Console.ReadLine());

            //Male Name badGuy.
            Console.WriteLine("\r\nPlease enter a male name and press enter.");
            string badGuy = Console.ReadLine();

            //Unit of measurement for length. buildingMeasure.
            Console.WriteLine("\r\nPlease enter a unit of length such as \"inch\" or \"mile\"and press enter.");
            string buildingMeasure = Console.ReadLine();

            //Number greater than 10   buildingHeight
            Console.WriteLine("\r\nPlease enter a number greater than 10 and press enter.");
            double buildingHeight = double.Parse(Console.ReadLine());

            //Name.   mountName.
            Console.WriteLine("\r\nPlease enter a name and press enter.");
            string mountName = Console.ReadLine();

            //A riding animal mountType
            Console.WriteLine("\r\nPlease enter an animal you can ride on and press enter.");
            string mountType = Console.ReadLine();

            //Weapon weapon.
            Console.WriteLine("\r\nPlease enter a type of weapon and press enter.");
            string weapon = Console.ReadLine();

            //Mythical Beast  mythicalBeast.
            Console.WriteLine("\r\nPlease enter a plural word for mythical beasts such as \"unicorns\" and press enter.");
            string mythicalBeast = Console.ReadLine();

            //Name of place country1
            Console.WriteLine("\r\nPlease enter the name of a place and press enter.");
            string country1 = Console.ReadLine();

            //Name of place country2
            Console.WriteLine("\r\nPlease enter the name of a place and press enter.");
            string country2 = Console.ReadLine();

            //Exclamation  gardenExclamation
            Console.WriteLine("\r\nPlease enter an exclamation such as \"Oh wow\" and press enter. Capitalize the first letter.");
            string gardenExclamation = Console.ReadLine();

            //Number greater than 10 nightsAndDays
            Console.WriteLine("\r\nPlease enter a number greater than 10 and press enter.");
            double nightsAndDays = double.Parse(Console.ReadLine());

            //Building type.    castle
            Console.WriteLine("\r\nPlease enter a type of building and press enter.");
            string castle = Console.ReadLine();

            //Adverb  laughAdverb
            Console.WriteLine("\r\nPlease enter an adverb and press enter. Use one that ends in \"ly\" such as \"hastily\".");
            string laughAdverb = Console.ReadLine();

            //Adjective mountDescription
            Console.WriteLine("\r\nPlease enter a superlative adjective such as \"squishiest\" and press enter.");
            string mountDescription = Console.ReadLine();

            //Furniture furniture
            Console.WriteLine("\r\nPlease enter a piece of furniture and press enter.");
            string furniture = Console.ReadLine();
            Console.WriteLine("\r\n\r\n\r\n\r\n");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------\r\n\r\n");

            Console.WriteLine("Once upon a time in the royal land of " + country1 + ". Prince " + capturedPrince + " was in the royal garden " + gardenVerb + " the flowers.");
            Console.WriteLine("Suddenly, the Dark Lord " + badGuy +" appeared. On a flying " + furniture + ".");
            Console.WriteLine(gardenExclamation + "! exclaimed the startled Prince "+ capturedPrince +" as the Dark Lord swept him away laughing " + laughAdverb+".");
            Console.WriteLine("As soon as the Lady " + heroine + " heard of this she grabbed her " + weapon + " and went to the stables to find her trusty steed");
            Console.WriteLine(mountName + ", the " + mountDescription + " " + mountType + " in " + country1 + ". After " + nightsAndDays + " days and " + nightsAndDays + " nights ");
            Console.WriteLine(heroine +" and "+ mountName +" managed to travel the "+ distanceTraveled +" "+ distanceMeasure +" to the Dark Lord's home land of "+ country2 +".");
            Console.WriteLine("They stared in disbelief at the "+ mythicalBeastsAmount +" "+ mythicalBeast +" guarding "+ badGuy +"'s "+ buildingHeight +" "+ buildingMeasure + " high " + castle +".");
            Console.WriteLine(heroine + " made quick work of the " + mythicalBeast + " while " + mountName + " bashed in the door.");
            Console.WriteLine(heroine +" found "+ capturedPrince +" chained inside with no sign of "+ badGuy +" anywhere. \"You're free\" she said, \"lets go.\"");
            Console.WriteLine("\"And, don't follow me around like the last time.\" But, he did exactly that.");
            Console.WriteLine("For nearly " + romanceLasts + " " + time + " the prince was smitten with " + heroine + " before giving up and returning to his castle.");

            Console.WriteLine("\r\n\r\n------------------------------------------------------------------THE END--------------------------------------------------------------------------\r\n\r\n");


        }
    }
}

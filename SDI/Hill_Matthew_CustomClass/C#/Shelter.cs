﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass {

    class Shelter {

        string mName;
        int mMin = 0;
        int mMax;
        int mCurrent;
        //Allows instantiation with a name for the shelter a max number and a starting number of dogs.
        public Shelter(string _name, int _max, int _current){
            mName = _name;
            mMax = _max;
            mCurrent = _current;
            
        }

        public string Adopt(int _adopted) {
            if (mMin > mCurrent - _adopted)
            {
                string adoptMessage = "The {0} shelter {0} dogs available and you chose {1} dogs to be adopted.\r\nPlease refer your customer to another shelter.",mName, mCurrent, _adopted;
            }
            else {
                this.mCurrent -= _adopted;
                string adoptMessage = "The {0} shelter now has {1} dogs.", mName, mCurrent;
            }

            return adoptMessage;


        }

        public string DropOff(int _dropOff) {
            if (mMax < mCurrent - _dropOff)
            {
                int excess = mCurrent + _dropOff - mMax;
                int allowable = mMax - mCurrent;
                string dropOffMessage = "This will put {0} shelter {1} dogs over capacity. Your remaining capacity is {2} dogs.\r\nPlease refer your customer to another shelter.", mName, excess, allowable;
            }
            else
            {
                this.mCurrent += _dropOff;
                string dropOffMessage = "The {0} shelter now has {1} dogs.", mName, mCurrent;
            }

            return adoptMessage;
        }






    }
}
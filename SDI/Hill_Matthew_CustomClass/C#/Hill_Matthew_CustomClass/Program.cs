﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Hill_Matthew_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {




            /*
                Matthew Hill
                #02
                25 May 2017
                Custom Class Assignment
            */



            Console.WriteLine("\r\n\r\nThank you for installing the Dog Shelter Population Tracker!");

            string shelterName = "";

            Shelter shelter1 = new Shelter(shelterName, 45, 13);

            while (string.IsNullOrWhiteSpace(shelterName)) {
                Console.WriteLine("\r\n\r\nPlease type the name of your shelter.");
                shelterName = Console.ReadLine();
            }

            shelter1.Name = shelterName;

            Console.WriteLine("\r\n\r\nWelcome to {0}.",shelter1.Name);
            Console.WriteLine("Your maximum capacity is {0} dogs.", shelter1.Capacity);
            Console.WriteLine("You are currently caring for {0} dogs and have room for {1} more.\r\n\r\n", shelter1.Population, shelter1.Space);
            //This is the main loop for program. It will run until all customers have been served.
            for (int i = 5; i > 0; i--) {
                string transactionType = "";

                string adOrDropStr = "";
                int adOrDropNum = 0;
                //This is the input loop for deciding if the customers are dropping off or adopting dogs it will try and parse in input and confirm it is only one of the two options available.

                //
                while (adOrDropNum != 1 && adOrDropNum != 2)
                {
                    adOrDropStr = "";
                    while (!int.TryParse(adOrDropStr, out adOrDropNum)) {
                        Console.WriteLine("\r\n\r\nYou have {0} customers in line.", i);
                        Console.WriteLine("You are currently caring for {0} dogs and have room for {1} more.", shelter1.Population, shelter1.Space);
                        Console.WriteLine("Is the current customer adopting or dropping off?");
                        Console.WriteLine("Press 1 and then Enter for adopting.\r\nPress 2 and then Enter for dropping off.");
                        adOrDropStr = Console.ReadLine();


                    }

                }


                //this will help with verbiage and decision making in the following loop.
                if (adOrDropNum == 1)
                {
                    transactionType = "adopt";
                }
                else {
                    transactionType = "drop off";
                }

                
                string amntStr = "";
                int amntNum = 0;
                //This is the input loop for acquiring the number of dogs in the "transaction" for lack of a better word.  
                while (amntNum < 1)
                {
                    amntStr = "";
                    //This will prevent a "transaction" from having 0 or a negative number of dogs.
                    while (!int.TryParse(amntStr, out amntNum))
                    {
                        Console.WriteLine("How many dogs does the customer wish to {0}?",transactionType);
                        amntStr = Console.ReadLine();
                    }
                }


                if (transactionType == "adopt")
                {
                    shelter1.Population = shelter1.Population - amntNum;
                } else {
                    shelter1.Population = shelter1.Population + amntNum;
                }
                Console.WriteLine("Thank you.");
                Console.WriteLine("Press Enter to continue.");
                Console.ReadLine();              


            }


        }
    }
}

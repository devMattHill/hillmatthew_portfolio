﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_CustomClass
{
    class Shelter
    {

        private string mName;
        private int mCapacity;
        private int mPopulation;
        //Allows instantiation with a name for the shelter a max population and a starting number of dogs.
        public Shelter(string _name, int _capacity, int _population)
        {
            mName = _name;
            mCapacity = _capacity;
            mPopulation = _population;

        }
        



// Altering the getters and setters for Capacity, Population, and Name. 
//Getting rid of the mMin because I'll make the Population check to see if it will be below zero before allowing changes


        public string Name {
            get { return this.mName; }
            set { this.mName = value; }

        }


        public int Capacity {
            get { return this.mCapacity; }
            set {
                if (value < 0)
                {
                    Console.WriteLine("Shelter capacity can not be a negative number.");
                }else
                {
                    this.mCapacity = value;
                }
            }
        }


        public int Population {
            
            get { return this.mPopulation; }
            set
            {
                if (value < 0)
                {
                    int _tooMany = this.mPopulation - value;
                    Console.WriteLine("\r\n\r\n" + mName + " only has " + mPopulation + " dog(s) available and you chose " + _tooMany + " dog(s) to be adopted.\r\nPlease refer your customer to another shelter.\r\n\r\n");
                }else if(value > mCapacity ){
                    int _excess = value - this.mCapacity;
                    int _allowable = this.mCapacity - this.mPopulation;
                    Console.WriteLine("\r\n\r\nThis will put " + mName + " " + _excess + " dog(s) over capacity. You only have room for " + _allowable + " more.\r\nPlease refer your customer to another shelter.\r\n\r\n");

                }else{
                    this.mPopulation = value;
                }
            }
        }

        
        public int Space {
            get { return this.mCapacity - this.mPopulation; }
        }
        



    }
}

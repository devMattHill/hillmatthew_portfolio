﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
              Matthew Hill
              #02
              21 May 2017
              ArrayLists
              */
            Lineage();



        }

        public static void Lineage() {

            //Declare and define arrayLists
            ArrayList fathers = new ArrayList() { "Amos Diggory", "Vernon Dursley", "Marvolo Gaunt", "Frank Longbottom", "Xenophilius Lovegood", "Lucius Malfoy", "James Potter", "Ted Tonks", "Arthur Weasley"};
            ArrayList children = new ArrayList() { "Cedric Diggory", "Dudley Dursley", "Merope Gaunt", "Neville Longbottom", "Luna Lovegood", "Draco Malfoy", "Harry Potter", "Nymphadora Tonks", "Ron Weasley"};

            string newFather = "Ron Weasley";
            string newChild = "Rose Weasley";


            Console.WriteLine("\r\nWelcome Death Eater press enter to retrieve a list of the families in your current location.\r\n");
            Console.ReadLine();
            //This uses the Fathering method to put the arrays together into descriptive sentences.
            for (int iteration=0; iteration < fathers.Count; iteration++) {
                Console.WriteLine(Fathering(fathers[iteration], children[iteration]));

            }
            Console.WriteLine("\r\nPress enter to check the list for Muggles and Mud Bloods.");
            Console.ReadLine();
            Console.WriteLine("\r\n\r\n");
            //This uses a repeatable method to find fathers of specific surnames and logs their index numbers
            int mdbldCount = 1;
            
            while (mdbldCount > 0)
            {



                mdbldCount = 0;
                int targetNumber = 0;
                ArrayList targetNumbers = new ArrayList();
                foreach (string surname in fathers) {
                    //Console.WriteLine(surname);
                    //Uses a method to determine if the object in the array needs removed
                    bool target = FindTheMugglesAndMudBloods(surname);
                    //Console.WriteLine("The function returned "+target);
                    if (target == true) {
                        mdbldCount++;
                        Console.WriteLine("Target number {0}, {1} has been found.\r\n", targetNumber, surname);
                        targetNumbers.Add(targetNumber);
                        target = false;
                    }
                    targetNumber++;


                }
                Console.Write("Type the target number you wish to eliminate and press enter.\r\nTarget Number:");

                
                int chosenTargetNum = -1;
                string chosenTargetStr = "";
                //Confirms proper input of target numbers
                while (!int.TryParse(chosenTargetStr, out chosenTargetNum))
                    {
                        //Console.WriteLine("\r\nMade it to the int.tryparse target Check.");
                     
                        chosenTargetStr = Console.ReadLine();
                    Console.WriteLine("\r\n\r\n");
                    }
                    foreach (int num in targetNumbers)
                    {
                        //Console.WriteLine(num + "  " + chosenTargetNum);
                       // Console.WriteLine(num == chosenTargetNum);
                        if (num == chosenTargetNum)
                        {
                            //Console.WriteLine("Inside the if statment.");
                            //This uses the same method to remove people of specific surnames from each array
                            fathers = RemoveMugglesAndMudBloods(fathers, fathers[num].ToString());
                            children = RemoveMugglesAndMudBloods(children, children[num].ToString());
                        mdbldCount--;
                            //resets the menu options to prevent a neverending loop
                            //chosenTargetStr = "";
                    }
                    }


              



            }

            Console.WriteLine("\r\n\r\nYou have removed two Muggle or Mud Blood families.\r\nGood job Death Eater.\r\nPress Enter to Continue.");
            Console.ReadLine();
            //This uses the same method twice to add in a new father child set
            fathers = NewFamily(fathers, newFather);
            children = NewFamily(children, newChild);

            Console.WriteLine("\r\n\r\nA new target has arrived in your area. Press Enter to renew your list.\r\n");
            Console.ReadLine();
            //This uses the Fathering method to put the arrays together.
            for (int iteration = 0; iteration < fathers.Count; iteration++)
            {
                Console.WriteLine(Fathering(fathers[iteration], children[iteration]));

            }

            Console.WriteLine("\r\nGood luck Death Eater.\r\n\r\n");





        }
        //This method produces a sentence for each pairing in the arrays
        public static string Fathering(object f, object c) {
            string line = f+" is the father of "+c+".";
            return line;
        }
        //Checks the contents of the given parameters against a hard coded list of objects to remove.
        public static bool FindTheMugglesAndMudBloods(string name) {
            //Console.WriteLine("made it to FIND");
            bool mud = false;
          
                //Console.WriteLine("Checking Target "+target);
                if (name.Contains("Tonks") || name.Contains("Dursley"))
                {
                    mud = true;
                }
                else {
                    mud = false;
                }
           
            return mud;
        }


        //This Method will remove any items from the given arraylist if they match the given string
        public static ArrayList RemoveMugglesAndMudBloods(ArrayList ledger, string name) {
            ledger.Remove(name);
            return ledger;
        }

        //This Method will add in the next generation of Weasleys aka will add given strings to given ArrayLists
        public static ArrayList NewFamily(ArrayList ledger, string addition)
        {
            ledger.Insert(0, addition);
            return ledger;

        }


    }
}

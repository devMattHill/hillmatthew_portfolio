﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_Methods
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Matthew Hill
            #02
            19 May 2017
            Methods Assignment
            */

            //Declare Variables for the menu.
            string menuOptionStr = "";
            int menuOption = 0;
            //This loop makes sure the user must pick a number 1 - 3
            while (menuOption != 1 && menuOption != 2 && menuOption != 3)
            {
                //This loop parses the 1 - 3 as an integer into the variable 'menuOption'
                while (!int.TryParse(menuOptionStr, out menuOption))
                {
                    Console.WriteLine("Welcome to the Methods Tool Suite!");
                    Console.WriteLine("Choose the tool you would like to use. \r\nType the number next to the tool and press enter.");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("1. Painting A Wall");
                    Console.WriteLine("2. Stung!");
                    Console.WriteLine("3. Reverse It");
                    Console.WriteLine("------------------------------");
                    menuOptionStr = Console.ReadLine();
                }
            }
            /* 
            Start the if then set for running the different programs.
             */

            if (menuOption == 1)
            {
                //Painting a wall begin
                //I like to use an array for questions if the input type is the same, this prevents reusing similar loops
                string[] questions = new string[4] { "the Width of the wall (in feet)", "the Height of the wall (in feet)", "the Number of coats of paint you wish to apply to the wall", "surface area one gallon of paint will cover (in square feet)" };
                //This will store the answers to the questions
                decimal[] inputNums = new decimal[4];

                //for loop for asking all the questions
                for (int i = 0; i < 4; i++)
                {
                    //Console.WriteLine("made it to the for loop");
                    //variable for storing the input
                    string menuQuestStr = "";
                    //while loop that asks the questions and tests the input type
                    while (!decimal.TryParse(menuQuestStr, out inputNums[i]))
                    {
                        //Console.WriteLine("made it to the while loop");
                        Console.WriteLine("Please enter the {0}.", questions[i]);

                        menuQuestStr = Console.ReadLine();
                    }


                }
                //variable that stores the returned calculation after passing all of the arguments to the method, .ToString formats my returned value
                string need = CalcNeededPaint(inputNums[0], inputNums[1], inputNums[2], inputNums[3]).ToString("N");
                Console.WriteLine("For {0} coats on that wall, you will need {1} gallons of paint.",inputNums[2].ToString("N"), need);

                /*
                Data	Sets	To	Test	
                    o Width	– 8,	Height	– 10,	Coats	– 2,	Surface	Area	- 300	ft2
                    § Results	- “For	2	coats	on	that	wall,	you	will	need	.53	gallons	of	paint.”
                    o Width	– 30,	Height	– 12.5,	Coats	– 3,	Surface	Area	- 350	ft2
                    § Results	- “For	3	coats	on	that	wall,	you	will	need	3.21	gallons	of	paint.”
                    o Test	one	of	your	own.
                    W- 142560, H- 11.81 Coats- 3 SA 450
                    For 3.00 coats on that wall, you will need 11,224.22 gallons of paint.
                    ^^ Berlin wall border between East and West Berlin

                Painting a Wall end 
                */
            }
            else if (menuOption == 2)
            {
                //Stung! begin
                //declare needed variables
                string weightStr = "";
                int weightNum;
                //a while loop that asks for input and verfies it is the correct type
                while (!int.TryParse(weightStr, out weightNum))
                {
                    //Console.WriteLine("made it to the while loop");
                    Console.WriteLine("Please enter the weight of the animal in pounds in whole numbers.");

                    weightStr = Console.ReadLine();
                }
                //variable that stores the returned calculation after passing the argument to the method
                int stings = StingCount(weightNum);

                Console.WriteLine("It takes {0} bee stings to kill this animal.", stings);


                /*
                 Data	Sets	To	Test	
                    o Animal’s	Weight	– 10
                    § Results	- “It	takes 90 bee	stings	to	kill	this	animal.”
                    o Animal’s	Weight	– 160
                    § Results	- “It	takes 1440 bee	stings	to	kill	this	animal.”
                    o Animal’s	Weight	– Twenty
                    § Results	– Re-prompt	for	number value.	
                    o Test	one	of	your	own.
                    My weight - 236
                    Results - "It takes 2124 bee stings to kill this animal."
                */


                //Stung! end
            }
            else if (menuOption == 3) {

                //declare an editable array list
                ArrayList listOfThings = new ArrayList();
                //declare the variable for list item storage, i chose fruit
                string fruitStr = "";
                //setup a variable to test the continuation of the below loop
                int fruitNum = 1 ;
                //made a loop that can be broken by entering 0 in the item list or keep entering items as long as needed
                while (fruitNum != 0) {
                    //clearing out the value of fruitStr so it does not interfere with the following input loop
                    fruitStr = "";
                    //the input loop that will keep running so long as the user does not input 0 as it will store to fruitNum and then break the outer loop
                    while (!int.TryParse(fruitStr, out fruitNum))
                    {
                        //Console.WriteLine("made it to the while loop");
                        Console.WriteLine("Please enter an item to add your list.\r\n When you are finsihed type 0 then press enter.");
                        fruitStr = Console.ReadLine();
                        if (fruitStr != "0") {
                            listOfThings.Add(fruitStr);
                        }
                    }

                }
                //sends the built array as an argument to another method and returns a string value of a specific order
                string listOfThingsFront = FrontArray(listOfThings);
                //sends the built array as an argument to another method and returns a string value of a specific order
                string listOfThingsRev = ReverseArray(listOfThings);

                Console.WriteLine("Your original array was [{0}].",listOfThingsFront);
                Console.WriteLine("and now it is reversed as [{0}].", listOfThingsRev);

                /*
                o Initial array	– [“apple”,	“pear”,	“peach”,	“coconut”,	“kiwi”]
                § Results	- Your	original	array	was	[“apple”,	“pear”,	“peach”,	“coconut”,	
                “kiwi”] and	now	it	is	reversed as	[“kiwi”,	“coconut”,	“peach”,	“pear”,	
                “apple”]
                o Initial array	– [“red”,	“orange”,	“yellow”,	“green”, “blue”, ”indigo”,	“violet”]
                § Results	- Your	original	array	was	[“red”,	“orange”,	“yellow”,	“green”,
                “blue”, ”indigo”,	“violet”] and	now	it	is	reversed as	[“violet”,	“indigo”,	
                “blue”,	“green”,	“yellow”,	“orange”,	“red”]
                o Now	that	you	tested	these	2	arrays,	create	one	of	your	own	to	test	and	turn	in. 
                Your	original	array	was	[“1”, “2”, “3”, “4”, “5”] 
                and	now	it	is	reversed as	[“5”, “4”, “3”, “2”, “1”]
                */
            }

            /* 
            End the if then set for running the different programs.
             */


        }
        //Calculates the paint amount
        public static decimal CalcNeededPaint(decimal w, decimal h, decimal layers, decimal spread) {
            decimal surface = w * h * layers;
            decimal gallonsNeeded = surface / spread;
            return gallonsNeeded;
        }
        //Determines how many stings are needed.
        public static int StingCount(int s) {
            int maxStings = s * 9;
            return maxStings;
        }

        //Sets up the given array into a string for display
        public static string FrontArray(ArrayList frontways)
        {
            string frontStr = "";
            int i = 0;
            //this parses the array into a string and ...
            foreach (string thing in frontways)
            {
                frontStr += thing;
                //...adds commas between all but the last item in the array
                if (i < frontways.Count - 1)
                {
                    frontStr += ", ";
                }
                i++;
            }
            return frontStr;
        }

        //Reverses and sets up the given array into a string for display
        public static string ReverseArray(ArrayList frontways) {
            //declare a local array for organizing the given array
            ArrayList reverse = new ArrayList();
            string reverseStr = "";
            //This loop walks down the given array and places the current object at the beginning of the new array BOOM reversed
            foreach (string thing in frontways) {
                reverse.Insert(0, thing);
            }
            int i = 0;
            //this parses the array into a string and ...
            foreach (string thing in reverse) {
                reverseStr += thing;
                //...adds commas between all but the last item in the array
                if (i < reverse.Count - 1) {
                    reverseStr += ", ";
                }
                i++;
            }
            return reverseStr;
        }
    }
}

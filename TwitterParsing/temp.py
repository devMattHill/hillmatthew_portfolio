#this import is needed to use the 'pd' object
import pandas as pd

#this is an import to bring in common english stop words to be ignored

from nltk.corpus import stopwords
#variable to store stopwords
stop = stopwords.words('english')

train = pd.read_csv('originalTwitterRawData.csv')

train['word_count'] = train['tweet'].apply(lambda x: len(str(x).split(" ")))
#train[['tweet','word_count']].head()

train['char_count'] = train['tweet'].str.len() # this also includes spaces
#train[['tweet','char_count']].head()

def avg_word(sentence):
  words = sentence.split(" ")
  #this section put in to check math on this function
  #print (words)
  #print (len(words))
  #print ("---------------------------------------")
  return (sum(len(word) for word in words)/len(words))

train['avg_word'] = train['tweet'].apply(lambda x: avg_word(x))
#train[['tweet','avg_word']].head()


train['stopwords'] = train['tweet'].apply(lambda x: len([x for x in x.split() if x in stop]))
#train[['tweet', 'stopwords']].head()

train['hashtags'] = train['tweet'].apply(lambda x: len([x for x in x.split() if x.startswith('#')]))
#train[['tweet', 'hashtags']].head()

#this combines all of the .head() methods above

#check for numbers
train['numerics'] = train['tweet'].apply(lambda x: len([x for x in x.split() if x.isdigit()]))
#check for uppercase
train['upper'] = train['tweet'].apply(lambda x: len([x for x in x.split() if x.isupper()]))
#change everything to lowercase to simplify the data
train['tweet'] = train['tweet'].apply(lambda x: " ".join(x.lower() for x in x.split()))
#remove punctuation to simplify the data
train['tweet'] = train['tweet'].str.replace('[^\w\s]','')
#remove stopwords
train['tweet'] = train['tweet'].apply(lambda x: " ".join(x for x in x.split() if x not in stop))
#build a 





train[['tweet','word_count','char_count','avg_word', 'stopwords','hashtags','numerics','upper']].head()

print(train)
﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE06: TreeView and TabControl
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MatthewHill_CE06
{
    public partial class TravelPlanner : Form
    {
        //Instantiate a list to keep track of the legs of the trip
        List<Leg> trip = new List<Leg>();

        public TravelPlanner()
        {
            InitializeComponent();
            SetToDefaults();
            UpdateTripTotals();
        }

        //Sets the form to default values
        private void SetToDefaults() {
            legValues = new Leg();
        }
        
        //Property for modifying and extracting the form values
        public Leg legValues
        {
            //Getters and Setters
            get
            {
                Leg tmp = new Leg();
               
                tmp.Direction = comboBoxDirection.Text;
                tmp.Miles = numericUpDownMiles.Value;
                tmp.Hours = numericUpDownHours.Value;
                tmp.Mode = textBoxMode.Text;
                
                return tmp;
            }

            set
            {
                comboBoxDirection.Text = value.Direction;
                numericUpDownMiles.Value = value.Miles;
                numericUpDownHours.Value = value.Hours;
                textBoxMode.Text = value.Mode;
            }
        }

        //Triggers on "File>New or CTRL+N
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Clears the trip list
            trip = new List<Leg>();
            //Clears the form
            SetToDefaults();
            //Updates Totals tab
            UpdateTripTotals();
            //Clear the tree view
            treeViewPlanner.Nodes.Clear();
        }

        //Triggers on buttonAdd
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if(comboBoxDirection.SelectedIndex != 0)
            {   //Stores a leg with current form values into the trip list
                trip.Add(legValues);
                //Clears the form
                SetToDefaults();
                //Updates Totals tab
                UpdateTripTotals();
                //Update tree view
                UpdateTree();
            }
            else
            {
                //This way if the trip list is zero the treeview is cleared
                treeViewPlanner.Nodes.Clear();
            }

        }

        //Updates the trip totals for the Totals tab
        private void UpdateTripTotals()
        {
            if(trip.Count == 0)
            {   //Sets form fields to zero
                textBoxTotalsMiles.Text = "0";
                textBoxTotalsHours.Text = "0";
                
            }
            else
            {   //Sets form fields to their appropriate amounts
                textBoxTotalsMiles.Text = GetTotalMiles();
                textBoxTotalsHours.Text = GetTotalHours();
                
            }
            //sets the leg count
            textBoxTotalsLegs.Text = trip.Count.ToString();

        }

        //Returns total trip miles
        private string GetTotalMiles()
        {   //Instantiate a decimal variable for counting miles
            decimal tmp = 0;
            //Goes through each leg of the trip
            foreach(Leg leg in trip)
            {   //and adds up the mileage
                tmp += leg.Miles;
            }
            //Then return that number as a string
            return tmp.ToString();
        }

        //Returns total trip hours
        private string GetTotalHours()
        {
            //Instantiate a decimal variable for counting hours
            decimal tmp = 0;
            //Goes through each leg of the trip
            foreach (Leg leg in trip)
            {   //and adds up the hours
                tmp += leg.Hours;
            }
            //Then return that number as a string
            return tmp.ToString();
        }

        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Closes the application
            Application.Exit();
        }

        //Method for updating the treeview
        private void UpdateTree()
        {   //Clears the current treeview
            treeViewPlanner.Nodes.Clear();
            //cycle through the trip
            foreach(Leg leg in trip)
            {   //instantiate a tree node to work with
                TreeNode node = new TreeNode();
                //Give it a label
                node.Text = leg.ToString();
                //Give it an image
                node.ImageKey = leg.Direction;
                //Give it a selected image
                node.SelectedImageKey = "gearIcon";

                //Create a node for each property
                TreeNode nodeMiles = new TreeNode();
                nodeMiles.Text = leg.Miles.ToString();
                nodeMiles.ImageKey = "document";
                nodeMiles.SelectedImageKey = "gearIcon";

                //See above
                TreeNode nodeHours = new TreeNode();
                nodeHours.Text = leg.Hours.ToString();
                nodeHours.ImageKey = "plusSign";
                nodeHours.SelectedImageKey = "gearIcon";

                //See above
                TreeNode nodeMode = new TreeNode();
                nodeMode.Text = leg.Mode;
                nodeMode.ImageKey = "xIcon";
                nodeMode.SelectedImageKey = "gearIcon";

                //Add property nodes to the main leg node
                node.Nodes.Add(nodeMiles);
                node.Nodes.Add(nodeHours);
                node.Nodes.Add(nodeMode);

                //Add the top node to the tree view
                treeViewPlanner.Nodes.Add(node);


            }
        }

        
    }
}

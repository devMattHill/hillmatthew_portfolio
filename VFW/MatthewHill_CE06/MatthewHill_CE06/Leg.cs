﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE06: TreeView and TabControl
*/

namespace MatthewHill_CE06
{
    public class Leg
    {
        //Properties of a leg
        string direction;
        decimal miles;
        decimal hours;
        string mode;

        //Leg default constructor
        public Leg() {
            direction = "Nowhere";
            miles = 0;
            hours = 0;
            mode = "";
        }
        //Leg info constructor
        public Leg(string direction, decimal miles, decimal hours, string mode)
        {}
        //Public getters and setters
        public string Direction { get => direction; set => direction = value; }
        public decimal Miles { get => miles; set => miles = value; }
        public decimal Hours { get => hours; set => hours = value; }
        public string Mode { get => mode; set => mode = value; }

        //To String Override
        public override string ToString()
        {
            return direction;
        }
    }
}

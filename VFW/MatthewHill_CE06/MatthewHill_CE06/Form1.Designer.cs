﻿namespace MatthewHill_CE06
{
    partial class TravelPlanner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TravelPlanner));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControlTravel = new System.Windows.Forms.TabControl();
            this.tabPageLeg = new System.Windows.Forms.TabPage();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.groupBoxLeg = new System.Windows.Forms.GroupBox();
            this.textBoxMode = new System.Windows.Forms.TextBox();
            this.numericUpDownHours = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMiles = new System.Windows.Forms.NumericUpDown();
            this.labelMode = new System.Windows.Forms.Label();
            this.labeLeglHours = new System.Windows.Forms.Label();
            this.labelLegMiles = new System.Windows.Forms.Label();
            this.comboBoxDirection = new System.Windows.Forms.ComboBox();
            this.labelDirection = new System.Windows.Forms.Label();
            this.tabPageTotals = new System.Windows.Forms.TabPage();
            this.textBoxTotalsLegs = new System.Windows.Forms.TextBox();
            this.textBoxTotalsHours = new System.Windows.Forms.TextBox();
            this.textBoxTotalsMiles = new System.Windows.Forms.TextBox();
            this.labelLegs = new System.Windows.Forms.Label();
            this.labelTotalsHours = new System.Windows.Forms.Label();
            this.labelTotalsMiles = new System.Windows.Forms.Label();
            this.treeViewPlanner = new System.Windows.Forms.TreeView();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControlTravel.SuspendLayout();
            this.tabPageLeg.SuspendLayout();
            this.groupBoxLeg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMiles)).BeginInit();
            this.tabPageTotals.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1028, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = global::MatthewHill_CE06.Properties.Resources.document;
            this.newToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "document");
            this.imageList1.Images.SetKeyName(1, "East");
            this.imageList1.Images.SetKeyName(2, "West");
            this.imageList1.Images.SetKeyName(3, "North");
            this.imageList1.Images.SetKeyName(4, "South");
            this.imageList1.Images.SetKeyName(5, "plusSign");
            this.imageList1.Images.SetKeyName(6, "xIcon");
            this.imageList1.Images.SetKeyName(7, "gearIcon");
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.statusStrip1.Location = new System.Drawing.Point(0, 591);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1028, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.26168F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.73832F));
            this.tableLayoutPanel1.Controls.Add(this.tabControlTravel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.treeViewPlanner, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1028, 551);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tabControlTravel
            // 
            this.tabControlTravel.Controls.Add(this.tabPageLeg);
            this.tabControlTravel.Controls.Add(this.tabPageTotals);
            this.tabControlTravel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlTravel.Location = new System.Drawing.Point(3, 3);
            this.tabControlTravel.Name = "tabControlTravel";
            this.tabControlTravel.SelectedIndex = 0;
            this.tabControlTravel.Size = new System.Drawing.Size(469, 545);
            this.tabControlTravel.TabIndex = 0;
            // 
            // tabPageLeg
            // 
            this.tabPageLeg.Controls.Add(this.buttonAdd);
            this.tabPageLeg.Controls.Add(this.groupBoxLeg);
            this.tabPageLeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageLeg.Location = new System.Drawing.Point(8, 39);
            this.tabPageLeg.Name = "tabPageLeg";
            this.tabPageLeg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLeg.Size = new System.Drawing.Size(453, 498);
            this.tabPageLeg.TabIndex = 0;
            this.tabPageLeg.Text = "Leg";
            this.tabPageLeg.UseVisualStyleBackColor = true;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.Location = new System.Drawing.Point(24, 362);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(369, 96);
            this.buttonAdd.TabIndex = 1;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // groupBoxLeg
            // 
            this.groupBoxLeg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLeg.Controls.Add(this.textBoxMode);
            this.groupBoxLeg.Controls.Add(this.numericUpDownHours);
            this.groupBoxLeg.Controls.Add(this.numericUpDownMiles);
            this.groupBoxLeg.Controls.Add(this.labelMode);
            this.groupBoxLeg.Controls.Add(this.labeLeglHours);
            this.groupBoxLeg.Controls.Add(this.labelLegMiles);
            this.groupBoxLeg.Controls.Add(this.comboBoxDirection);
            this.groupBoxLeg.Controls.Add(this.labelDirection);
            this.groupBoxLeg.Location = new System.Drawing.Point(24, 32);
            this.groupBoxLeg.Name = "groupBoxLeg";
            this.groupBoxLeg.Size = new System.Drawing.Size(369, 312);
            this.groupBoxLeg.TabIndex = 0;
            this.groupBoxLeg.TabStop = false;
            this.groupBoxLeg.Text = "Leg";
            // 
            // textBoxMode
            // 
            this.textBoxMode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMode.Location = new System.Drawing.Point(156, 219);
            this.textBoxMode.Name = "textBoxMode";
            this.textBoxMode.Size = new System.Drawing.Size(197, 44);
            this.textBoxMode.TabIndex = 7;
            // 
            // numericUpDownHours
            // 
            this.numericUpDownHours.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownHours.DecimalPlaces = 2;
            this.numericUpDownHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownHours.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDownHours.Location = new System.Drawing.Point(156, 156);
            this.numericUpDownHours.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHours.Name = "numericUpDownHours";
            this.numericUpDownHours.Size = new System.Drawing.Size(197, 44);
            this.numericUpDownHours.TabIndex = 6;
            // 
            // numericUpDownMiles
            // 
            this.numericUpDownMiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownMiles.DecimalPlaces = 2;
            this.numericUpDownMiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMiles.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDownMiles.Location = new System.Drawing.Point(156, 104);
            this.numericUpDownMiles.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownMiles.Name = "numericUpDownMiles";
            this.numericUpDownMiles.Size = new System.Drawing.Size(197, 44);
            this.numericUpDownMiles.TabIndex = 5;
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.AutoSize = true;
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMode.Location = new System.Drawing.Point(7, 222);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(96, 37);
            this.labelMode.TabIndex = 4;
            this.labelMode.Text = "Mode";
            // 
            // labeLeglHours
            // 
            this.labeLeglHours.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeLeglHours.AutoSize = true;
            this.labeLeglHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeLeglHours.Location = new System.Drawing.Point(7, 158);
            this.labeLeglHours.Name = "labeLeglHours";
            this.labeLeglHours.Size = new System.Drawing.Size(103, 37);
            this.labeLeglHours.TabIndex = 3;
            this.labeLeglHours.Text = "Hours";
            // 
            // labelLegMiles
            // 
            this.labelLegMiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLegMiles.AutoSize = true;
            this.labelLegMiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLegMiles.Location = new System.Drawing.Point(7, 106);
            this.labelLegMiles.Name = "labelLegMiles";
            this.labelLegMiles.Size = new System.Drawing.Size(90, 37);
            this.labelLegMiles.TabIndex = 2;
            this.labelLegMiles.Text = "Miles";
            // 
            // comboBoxDirection
            // 
            this.comboBoxDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDirection.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.comboBoxDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDirection.ForeColor = System.Drawing.SystemColors.MenuText;
            this.comboBoxDirection.FormattingEnabled = true;
            this.comboBoxDirection.Items.AddRange(new object[] {
            "Nowhere",
            "East",
            "West",
            "North",
            "South"});
            this.comboBoxDirection.Location = new System.Drawing.Point(156, 39);
            this.comboBoxDirection.Name = "comboBoxDirection";
            this.comboBoxDirection.Size = new System.Drawing.Size(197, 45);
            this.comboBoxDirection.TabIndex = 1;
            this.comboBoxDirection.Tag = "";
            // 
            // labelDirection
            // 
            this.labelDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDirection.AutoSize = true;
            this.labelDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDirection.Location = new System.Drawing.Point(7, 42);
            this.labelDirection.Name = "labelDirection";
            this.labelDirection.Size = new System.Drawing.Size(143, 37);
            this.labelDirection.TabIndex = 0;
            this.labelDirection.Text = "Direction";
            // 
            // tabPageTotals
            // 
            this.tabPageTotals.Controls.Add(this.textBoxTotalsLegs);
            this.tabPageTotals.Controls.Add(this.textBoxTotalsHours);
            this.tabPageTotals.Controls.Add(this.textBoxTotalsMiles);
            this.tabPageTotals.Controls.Add(this.labelLegs);
            this.tabPageTotals.Controls.Add(this.labelTotalsHours);
            this.tabPageTotals.Controls.Add(this.labelTotalsMiles);
            this.tabPageTotals.Location = new System.Drawing.Point(8, 39);
            this.tabPageTotals.Name = "tabPageTotals";
            this.tabPageTotals.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTotals.Size = new System.Drawing.Size(453, 498);
            this.tabPageTotals.TabIndex = 1;
            this.tabPageTotals.Text = "Totals";
            this.tabPageTotals.UseVisualStyleBackColor = true;
            // 
            // textBoxTotalsLegs
            // 
            this.textBoxTotalsLegs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTotalsLegs.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBoxTotalsLegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalsLegs.Location = new System.Drawing.Point(184, 192);
            this.textBoxTotalsLegs.Name = "textBoxTotalsLegs";
            this.textBoxTotalsLegs.ReadOnly = true;
            this.textBoxTotalsLegs.Size = new System.Drawing.Size(186, 44);
            this.textBoxTotalsLegs.TabIndex = 5;
            // 
            // textBoxTotalsHours
            // 
            this.textBoxTotalsHours.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTotalsHours.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBoxTotalsHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalsHours.Location = new System.Drawing.Point(184, 124);
            this.textBoxTotalsHours.Name = "textBoxTotalsHours";
            this.textBoxTotalsHours.ReadOnly = true;
            this.textBoxTotalsHours.Size = new System.Drawing.Size(186, 44);
            this.textBoxTotalsHours.TabIndex = 4;
            // 
            // textBoxTotalsMiles
            // 
            this.textBoxTotalsMiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTotalsMiles.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBoxTotalsMiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTotalsMiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalsMiles.Location = new System.Drawing.Point(184, 60);
            this.textBoxTotalsMiles.Name = "textBoxTotalsMiles";
            this.textBoxTotalsMiles.ReadOnly = true;
            this.textBoxTotalsMiles.Size = new System.Drawing.Size(186, 44);
            this.textBoxTotalsMiles.TabIndex = 3;
            // 
            // labelLegs
            // 
            this.labelLegs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLegs.AutoSize = true;
            this.labelLegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLegs.Location = new System.Drawing.Point(64, 199);
            this.labelLegs.Name = "labelLegs";
            this.labelLegs.Size = new System.Drawing.Size(86, 37);
            this.labelLegs.TabIndex = 2;
            this.labelLegs.Text = "Legs";
            // 
            // labelTotalsHours
            // 
            this.labelTotalsHours.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTotalsHours.AutoSize = true;
            this.labelTotalsHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalsHours.Location = new System.Drawing.Point(64, 131);
            this.labelTotalsHours.Name = "labelTotalsHours";
            this.labelTotalsHours.Size = new System.Drawing.Size(103, 37);
            this.labelTotalsHours.TabIndex = 1;
            this.labelTotalsHours.Text = "Hours";
            // 
            // labelTotalsMiles
            // 
            this.labelTotalsMiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTotalsMiles.AutoSize = true;
            this.labelTotalsMiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalsMiles.Location = new System.Drawing.Point(64, 60);
            this.labelTotalsMiles.Name = "labelTotalsMiles";
            this.labelTotalsMiles.Size = new System.Drawing.Size(90, 37);
            this.labelTotalsMiles.TabIndex = 0;
            this.labelTotalsMiles.Text = "Miles";
            // 
            // treeViewPlanner
            // 
            this.treeViewPlanner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewPlanner.ImageIndex = 0;
            this.treeViewPlanner.ImageList = this.imageList1;
            this.treeViewPlanner.Location = new System.Drawing.Point(478, 3);
            this.treeViewPlanner.Name = "treeViewPlanner";
            this.treeViewPlanner.SelectedImageIndex = 0;
            this.treeViewPlanner.Size = new System.Drawing.Size(547, 545);
            this.treeViewPlanner.TabIndex = 1;
            // 
            // TravelPlanner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 613);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TravelPlanner";
            this.Text = "Travel Planner";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControlTravel.ResumeLayout(false);
            this.tabPageLeg.ResumeLayout(false);
            this.groupBoxLeg.ResumeLayout(false);
            this.groupBoxLeg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMiles)).EndInit();
            this.tabPageTotals.ResumeLayout(false);
            this.tabPageTotals.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControlTravel;
        private System.Windows.Forms.TabPage tabPageLeg;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.GroupBox groupBoxLeg;
        private System.Windows.Forms.TextBox textBoxMode;
        private System.Windows.Forms.NumericUpDown numericUpDownHours;
        private System.Windows.Forms.NumericUpDown numericUpDownMiles;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Label labeLeglHours;
        private System.Windows.Forms.Label labelLegMiles;
        private System.Windows.Forms.ComboBox comboBoxDirection;
        private System.Windows.Forms.Label labelDirection;
        private System.Windows.Forms.TabPage tabPageTotals;
        private System.Windows.Forms.TextBox textBoxTotalsLegs;
        private System.Windows.Forms.TextBox textBoxTotalsHours;
        private System.Windows.Forms.TextBox textBoxTotalsMiles;
        private System.Windows.Forms.Label labelLegs;
        private System.Windows.Forms.Label labelTotalsHours;
        private System.Windows.Forms.Label labelTotalsMiles;
        private System.Windows.Forms.TreeView treeViewPlanner;
    }
}


﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE07: JSON and Web Connectivity
*/
using System;
using System.Windows.Forms;

namespace MatthewHill_CE07
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormClassViewer());
        }
    }
}

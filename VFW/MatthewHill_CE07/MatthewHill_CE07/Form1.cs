﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE07: JSON and Web Connectivity
*/

//Namespaces for JSON stuff
using Newtonsoft.Json.Linq;
using System;
using System.IO;
//Namespaces for accessing the internet and stream readers and writers
using System.Net;
using System.Windows.Forms;

namespace MatthewHill_CE07
{
    public partial class FormClassViewer : Form
    {
        //Instance variables for the form
        WebClient apiConnection = new WebClient();

        //ASD string
        string apiASD = "http://mdv-vfw.com/asd.json";
        //VFW string
        string apiVFW = "http://mdv-vfw.com/vfw.json";

        //Path for saving files
        static string saveFolder = @"..\..\SavedCourses";

        //Initializes the primary form
        public FormClassViewer()
        {
            InitializeComponent();
            //Sets the form to default values.
            FormReset();
            radBtnASD.Checked = true;
        }

        //Property for modifying and extracting the form values
        public Course values
        {
            //Getters and Setters
            get
            {
                Course tmp = new Course();

                tmp.Name = txtBxClassName.Text;
                tmp.Code = txtBxCourseCode.Text;
                tmp.Month = numericMonth.Value;
                tmp.Credits = numericHours.Value;
                tmp.Description = txtBxCourseDescription.Text;

                return tmp;
            }

            
        set
            {
                txtBxClassName.Text = value.Name;
                txtBxCourseCode.Text = value.Code;
                numericMonth.Value = value.Month;
                numericHours.Value = value.Credits;
                txtBxCourseDescription.Text = value.Description;
            }
        }

        //Method for resetting the form
        private void FormReset()
        {
            values = new Course();
        }

        //Method for reading JSON data from an API
        private void ReadDataJSON()
        {   //Setup the correct api to access based on radio buttons
            string apiString = apiVFW;
            if (radBtnASD.Checked == true)
            {
                apiString = apiASD;
            }
            //Setup the variable to store the downloaded string in 
            string apiData = "";

            //Tries to complete the webClient connection
            try
            {
                apiData = apiConnection.DownloadString(apiString);
                //if successful it clears the form and
                FormReset();
                //pushes the apiData to a method that fills the form
                PutObjectIntoForm(apiData);

            }//Catches any errors
            catch(WebException error)
            {//Provides an error message
                MessageBox.Show("Error: \n" + error.Message);
            }
             
            



        }

        //method for taking the Download String and filling in the form
        private void PutObjectIntoForm(string apiData)
        {
            //see the string
            //MessageBox.Show("The string: \n" + apiData);

            //Convert string into a json object
            JObject o = JObject.Parse(apiData);

            //Find specific data in the jObject 
            string name = o["class"]["course_name_clean"].ToString();
            string code = o["class"]["course_code_long"].ToString();

            //parse as decimals
            string monthStr = o["class"]["sequence"].ToString();
            decimal month;
            decimal.TryParse(monthStr, out month);

            //parse as decimals
            string creditsStr = o["class"]["credit"].ToString();
            decimal credits;
            decimal.TryParse(creditsStr, out credits);

            string description = o["class"]["course_description"].ToString();

            //and fill the form values
            values = new Course(name, code, month, credits, description);
            
        }

        //Triggers on button "CheckOnlineInformation"
        private void btnOnlineData_Click(object sender, EventArgs e)
        {
           
            ReadDataJSON();

        }

        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Closes the application
            Application.Exit();
        }

        //Triggers on File>New
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Resets to defaults
            FormReset();
        }

        //Triggers on File>Save
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates the save directory if it does not already exist.
            Directory.CreateDirectory(saveFolder);

            
            //opens a save file dialog for storing the starship information
            if (saveFileDialogCourse.ShowDialog() == DialogResult.OK)
            {    //opens a streamwriter to save the ship file as a .txt
                using (StreamWriter outStream = new StreamWriter(saveFileDialogCourse.FileName))
                {
                    //cycles through each ship in each listBox and saves them with a WriteLine
                    //This should be neater but, the fleet names are static.
                    outStream.WriteLine(values.SaveString());
                }
            }
            
        }

        //Triggers on File>Load
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates the saved ships directory if it does not exist
            //I can't get the openDialogBox to default to the directory though :( #continue
            Directory.CreateDirectory(saveFolder);
            
            //Opens an openFialDialog for locating saved course files.
            if (openFileDialogCourse.ShowDialog() == DialogResult.OK)
            {
                //Clears out the current form
                FormReset();
                //opens a streamreader to read the selected file
                using (StreamReader inStream = new StreamReader(openFileDialogCourse.FileName))
                {
                    //Takes the information from the file and fills in the course properties
                    while (inStream.Peek() > -1)
                    {   //Read in the file information
                        string line = inStream.ReadLine();
                        //Split the file into usable fields
                        string[] readFile = line.Split('|');
                        //Check to see if this is a course file
                        if(readFile[0].ToString() == "CourseFile")
                        {
                            //If it is parse the values
                            //Find specific data in the array and
                            string name =readFile[1].ToString();
                            string code = readFile[2].ToString();

                            //parse as decimals
                            string monthStr = readFile[3].ToString();
                            decimal month;
                            decimal.TryParse(monthStr, out month);

                            //parse as decimals
                            string creditsStr = readFile[4].ToString();
                            decimal credits;
                            decimal.TryParse(creditsStr, out credits);

                            string description = readFile[5].ToString();

                            //and fill the form fields
                            values = new Course(name, code, month, credits, description);
                        }
                        else
                        {

                            MessageBox.Show("Error: Invalid File Type.");
                        }
                    }
                }
            }
            else
            {
                //Loads a "failed to load" name if the user cancels the openFileDialog

                MessageBox.Show("Error: Failed To Load File.");
            }


        }
    }
}

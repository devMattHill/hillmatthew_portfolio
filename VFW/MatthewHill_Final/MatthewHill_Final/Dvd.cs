﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE_Final
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_Final
{
    class Dvd
    {
        public int IdNumber { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }

        public Dvd() { }
        public Dvd(int idNumber, string title, string genre, int year, decimal price)
        {
            IdNumber = idNumber;
            Title = title;
            Genre = genre;
            Year = year;
            Price = price;
        }


        public override string ToString()
        {
            if(Title.Length > 20)
            {
                string truncatedTitle = Title.Substring(0, 20);
                return truncatedTitle;
            }
            else
            {
                return Title;
            }

            
        }
        

    }
}

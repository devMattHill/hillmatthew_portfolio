﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE_Final
*/

//name space for using mysql
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
//name space for saving files
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace MatthewHill_Final
{
    public partial class Form1 : Form
    {
        //connection string variable
        MySqlConnection conn = new MySqlConnection();

        //Instantiate the DataTable object
        DataTable lore = new DataTable();

       
        //Initializes the form and then runs the three follow on required methods to get 
        //Data from the MySql Database
        public Form1()
        {
            InitializeComponent();

            string connectionString = BuildConnectionString();

            Connect(connectionString);

            RetrieveData();
        }

        //Method for querying the database after the connection is made
        private void RetrieveData()
        {
            //Hard coded sql query
            string sql = "SELECT dvdid, dvd_title, genre, year, price FROM dvd LIMIT 25";

            //Create the DataAdapter
            MySqlDataAdapter adapter = new MySqlDataAdapter(sql, conn);

            //Set the type for the SELECT command to text
            adapter.SelectCommand.CommandType = CommandType.Text;

            //use the msqladapter to fill the datatable
            adapter.Fill(lore);

            //Use the datatable to fill a dataRowCollection
            DataRowCollection rows = lore.Rows;

            //storing all Dvd opjects into a list
            foreach (DataRow row in rows)
            {
                int idNumber;
                int.TryParse(row["dvdid"].ToString(), out idNumber);
                string title = row["DVD_Title"].ToString();
                string genre = row["Genre"].ToString();
                int year;
                int.TryParse(row["Year"].ToString(), out year);
                decimal price;
                decimal.TryParse(row["Price"].ToString(), out price);
                //instantiate a DVD object with the above values
                Dvd dvd = new Dvd(idNumber, title, genre, year, price);
                //Instantiate a list view item
                ListViewItem lvi = new ListViewItem();
                //Store dvd information to that list view item
                lvi.Text = dvd.ToString();
                lvi.Tag = dvd;
                lvi.ImageIndex = 0;
                //add that list view item to the dvd listview
                listViewDVD.Items.Add(lvi);

            }
            
        }

        //Method for connecting to the MySql Database
        private void Connect(string myConnectionString)
        {
            try
            {//Tries to open a connection
                conn.ConnectionString = myConnectionString;
                conn.Open();
                //MessageBox.Show("Connected!");
            }//Catches errors if it fails
            catch (MySqlException e)
            {
                // message string variable
                string msg = "";
                //check the exception
                switch (e.Number)
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve host address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username or password.";
                        break;
                    default:
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }//Shows any caught exceptions
                MessageBox.Show(msg);
            }
        }


        //Method for building the connection string for the MySql database
        private string BuildConnectionString()
        {
            string serverIP = "";
            //Tries to read the predetermined file for saving IP address
            try
            {
                //open C:\VFW\connect.txt file with a stream reader
                using (StreamReader reader = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    // read server IP
                    serverIP = reader.ReadToEnd();
                }
            }//Catches any errors in finding the file
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            //returns the connection string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";

        }


        //Method for updating the form
        private void UpdateForm(Dvd item)
        {
            //Put current record data into the form
            statusDvdID.Text = "DVD ID: " + item.IdNumber.ToString();
            txtDvdTitle.Text = item.Title;
            txtGenre.Text = item.Genre;
            numYear.Value = item.Year;
            numPrice.Value = item.Price;
            
        }
        
        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {//exits the application
            Application.Exit();
        }

        //Triggers on list view item selection
        private void listViewDVD_SelectedIndexChanged(object sender, EventArgs e)
        { //checks to make sure an item is selected
            if(listViewDVD.SelectedItems.Count > 0)
            { 
                //Updates the form fields
                UpdateForm(listViewDVD.SelectedItems[0].Tag as Dvd);
            }
        }
        
        //Triggers on View>Large
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {//Checks to see if Large is already checked. If it is not.
            if(!largeToolStripMenuItem.Checked)
            {   //Change the listview view to largeicon
                listViewDVD.View = View.LargeIcon;
                //Check the View>Large menu item
                largeToolStripMenuItem.Checked = true;
                //Uncheck the View>Small menu item
                smallToolStripMenuItem.Checked = false;
            }
        }
        //Tirggers on View>Small
        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {//Checks to see if small is already checked. If it is not.
            if (!smallToolStripMenuItem.Checked)
            {//Change the listview view to smallicon
                listViewDVD.View = View.SmallIcon;
                //Check the View>Small menu item
                smallToolStripMenuItem.Checked = true;
                //Uncheck the View>Large menu item
                largeToolStripMenuItem.Checked = false;
            }
        }

 
        //Triggers on btnUpdate
        private void button1_Click(object sender, EventArgs e)
        {   // check if connection is still open
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            //Make sure an item is selected
            if (listViewDVD.SelectedItems.Count > 0)
            {
                //Instantiate a dvd item based off the selected item to get the idNumber
                Dvd idDvd = listViewDVD.SelectedItems[0].Tag as Dvd;
                int idNumber = idDvd.IdNumber;
                //Get the rest of the properties from the form
                string title = txtDvdTitle.Text;
                string genre = txtGenre.Text;
                int year; 
                int.TryParse(numYear.Value.ToString(), out year);
                decimal price = numPrice.Value;

                //instantiate a dvd object with current form values
                Dvd tmp = new Dvd(idNumber, title, genre, year, price);

                //Update the selected listview item
                listViewDVD.SelectedItems[0].Tag = tmp;
                listViewDVD.SelectedItems[0].Text = tmp.ToString();

              
                //Programattically created MySql Update string
                string sqlUpdate = $"UPDATE dvd SET dvd_title ='{title}', genre ='{genre}', year ={year}, price = {price} WHERE dvdid = {idNumber}";

                try
                {
                    //create command to be passed
                    MySqlCommand cmd = new MySqlCommand(sqlUpdate, conn);
                    //create reader
                    MySqlDataReader reader;
                    //open the connection
                    conn.Open();
                    //execute the command
                    reader = cmd.ExecuteReader();
                }
                catch (Exception exc)
                {
                    //Display the exception
                    MessageBox.Show(exc.Message);
                }


            }
                


            
 

            


        }
    }
}

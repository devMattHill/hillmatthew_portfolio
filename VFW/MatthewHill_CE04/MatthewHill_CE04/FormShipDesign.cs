﻿using System;
using System.Windows.Forms;
/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    public partial class FormShipDesign : Form
    {

        //Public Event Handler
        public EventHandler FormShipDesignClosed;
        public EventHandler AddShipToFleet;

        //Property for modifying and extracting the form values
        public Starship shipValues
        {
            get
            {
                Starship tmp = new Starship();
                tmp.Name = textStarshipName.Text;
                tmp.ShipClass = comboBoxShipClass.Text;
                tmp.Speed = numericUpDownSubLightSpeed.Value;
                tmp.FTL = checkBoxFtlCapable.Checked;
                tmp.IndexNumber = comboBoxShipClass.SelectedIndex;
                return tmp;
            }

            set
            {
                textStarshipName.Text = value.Name;
                comboBoxShipClass.Text = value.ShipClass;
                numericUpDownSubLightSpeed.Value = value.Speed;
                checkBoxFtlCapable.Checked = value.FTL;
                
            }
        }

        //Initializes this window
        public FormShipDesign()
        {
            InitializeComponent();
        }

        //Triggered on "Add To Fleet" button
        private void buttonAddToFleet_Click(object sender, EventArgs e)
        {
            //Checks for a listener
            if(AddShipToFleet != null)
            {//Then Fires
                AddShipToFleet(this, new EventArgs ());
            }
            //sets the form back to default values
            shipValues = new Starship();
            
        }

        //Clears the form's input fields
        private void buttonClearForm_Click(object sender, EventArgs e)
        {
            //by loading default starship info
            shipValues = new Starship();
        }


        //Triggers when the FormShipDesigner is closed.
        private void FormShipDesign_FormClosed(object sender, FormClosedEventArgs e)
        {//Checks to ensure there is a listener
            if(FormShipDesignClosed != null)
            {//Then Fires
                FormShipDesignClosed(this, new EventArgs());
            }
        }
        
        //Triggers on the ToolStrip "Add To Fleet" button
        private void addToFleetToolStripMenuItem_Click(object sender, EventArgs e)
        {//Calls the already made "Add To Fleet" handler
            buttonAddToFleet_Click(this, new EventArgs());
        }
    }
}

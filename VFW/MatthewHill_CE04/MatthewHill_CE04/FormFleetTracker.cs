﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    public partial class FormFleetTracker : Form
    {

        //public event handler declarations
        public EventHandler UpdateFleetList;
       
        //List for storing Starships
        public List<Starship> fleet = new List<Starship>();

        //Variables for updating textBoxes
        private int designWindowsOpen = 0;
        private int fleetSize = 0;

        //Initializes this window
        public FormFleetTracker()
        {
            InitializeComponent();
        }

        //Triggered by File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {//Closes this window and the entire programs as this is the main window
            Close();
        }

        //Triggers with List>Clear
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //checks to ensure there is a listener
            if (UpdateFleetList != null)
            {//Then Fires
                fleetSize = 0;
                textBoxFleetSize.Text = fleetSize.ToString();
                fleet = new List<Starship>();
                UpdateFleetList(this, new EventArgs());
            }
            //[]cont need to send a call to FormFleet to clear out the listview
        }
        
        //Triggers with List>Display button and displays the fleetWindow
        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if the display menu is already checked 
            if (displayToolStripMenuItem.Checked == true)
            {
                //do nothing
            }
            else //else load the fleetWindow
            {
                //This adds a check mark next to the menu item. Be sure to change "check on click" to false in the 
                //form builder or the redundancy here will cause all kinds of problems.
                displayToolStripMenuItem.Checked = true;

                //instantiate the input form
                FormFleet fleetWindow = new FormFleet();

                //Sets the fleetWindow's events to be handled
                fleetWindow.FormFleetClosed += FleetWindow_FormFleetClosed;
                fleetWindow.FormFleetCleared += FleetWindow_FormFleetCleared; 
                fleetWindow.FormFleetSelected += FleetWindow_FormFleetSelected;

                //subscribe to the UpdateFleetList method on fleetWindow
                UpdateFleetList += fleetWindow.UpdateFleetList;
                
                //popup a modeless display form
                fleetWindow.Show();

                //sets off the UpdateList listener so the displayWindow will
                //update the listBox with the current list information upon launch
                //Checks that a listener exists
                if (UpdateFleetList != null)
                {//Fires
                    UpdateFleetList(this, new EventArgs());
                }
                
            }
        }
        
        //Triggers when the user double clicks an item in the FormFleet
        private void FleetWindow_FormFleetSelected(object sender, EventArgs e)
        {//Runs the OpenFormShipDesigner method and passes the FormFleet window as the sender
            //this is key to loading the Designer window with data from the FormFleet.ListView
            OpenFormShipDesign(sender, e);
        }

        //Triggers when the user clicks to design a new ship
        private void buttonDesignNewShip_Click(object sender, EventArgs e)
        {
            //Calls the OpenFormShipDesign metho with FormFleetTracker as the sender.
            OpenFormShipDesign(this, new EventArgs());
        }
        
        //Custom Method that opens a FormShipDesign may be triggered by the two above events
        private void OpenFormShipDesign(object sender, EventArgs e)
        {
            //Instantiate a FormShipDesign
            FormShipDesign designWindow = new FormShipDesign();

            //Sets the designWindow's events to be handled
            designWindow.FormShipDesignClosed += DesignWindow_FormShipDesignClosed;
            designWindow.AddShipToFleet += DesignWindow_AddShipToFleet;

            //if this is the activating form do nothing
            if (sender.GetType() == this.GetType())
            {

            }
            else //load the data sent into the new  designWindow
            {
                //set up usable FormFleet
                FormFleet getShip = (FormFleet)sender;
                //Put selected ship data into the form that is about to popup
                designWindow.shipValues = getShip.selectedShip;
            }
            //Creates a modeless form
            designWindow.Show();
            //Updates the number of design windows that are open
            designWindowsOpen++;
            //And displays that number
            textBoxWindowCount.Text = designWindowsOpen.ToString();
        }
        
        //Triggers when the user closes the FormFleet
        private void FleetWindow_FormFleetClosed(object sender, EventArgs e)
        {//Unchecks List>Display
            displayToolStripMenuItem.Checked = false;
        }

        //Triggers when the user presses "Clear List" in the FormFleet window
        private void FleetWindow_FormFleetCleared(object sender, EventArgs e)
        {//Calls the already built method for clearing the fleet list and listview window
            clearToolStripMenuItem_Click(this, new EventArgs());
        }

        //Triggers when the FormShipDesign presses Add To Fleet
        private void DesignWindow_AddShipToFleet(object sender, EventArgs e)
        {//Instantiates a usable FormShipDesign
            FormShipDesign getShip = (FormShipDesign)sender;
            //Adds the ship to the fleet list
            fleet.Add(getShip.shipValues);
            //Update the fleet size variable
            fleetSize++;
            //Use that variable to update the textBoxFleetSize
            textBoxFleetSize.Text = fleetSize.ToString();
            //Fires the update for FormFleet after checking that a listener is available
            if (UpdateFleetList != null)
            {//Then Fires
               
                UpdateFleetList(this, new EventArgs());
            }


        }

        //Fires when the FormShipDesign closes
        private void DesignWindow_FormShipDesignClosed(object sender, EventArgs e)
        {
            //adjusts the open window count
            designWindowsOpen--;
            //And displays it
            textBoxWindowCount.Text = designWindowsOpen.ToString();
        }



    }
}

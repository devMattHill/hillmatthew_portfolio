﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    partial class FormFleetTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxWindowCount = new System.Windows.Forms.GroupBox();
            this.textBoxWindowCount = new System.Windows.Forms.TextBox();
            this.groupBoxFleetSize = new System.Windows.Forms.GroupBox();
            this.textBoxFleetSize = new System.Windows.Forms.TextBox();
            this.buttonDesignNewShip = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.groupBoxWindowCount.SuspendLayout();
            this.groupBoxFleetSize.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(712, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fIleToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(62, 36);
            this.listToolStripMenuItem.Text = "List";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.groupBoxWindowCount, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.groupBoxFleetSize, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.buttonDesignNewShip, 1, 2);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(712, 515);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // groupBoxWindowCount
            // 
            this.groupBoxWindowCount.Controls.Add(this.textBoxWindowCount);
            this.groupBoxWindowCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxWindowCount.Location = new System.Drawing.Point(359, 23);
            this.groupBoxWindowCount.Name = "groupBoxWindowCount";
            this.groupBoxWindowCount.Padding = new System.Windows.Forms.Padding(6);
            this.groupBoxWindowCount.Size = new System.Drawing.Size(350, 390);
            this.groupBoxWindowCount.TabIndex = 1;
            this.groupBoxWindowCount.TabStop = false;
            this.groupBoxWindowCount.Text = "Current Ship Windows Open";
            // 
            // textBoxWindowCount
            // 
            this.textBoxWindowCount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWindowCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxWindowCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxWindowCount.Location = new System.Drawing.Point(86, 136);
            this.textBoxWindowCount.Name = "textBoxWindowCount";
            this.textBoxWindowCount.ReadOnly = true;
            this.textBoxWindowCount.Size = new System.Drawing.Size(154, 85);
            this.textBoxWindowCount.TabIndex = 0;
            this.textBoxWindowCount.Text = "0";
            this.textBoxWindowCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxWindowCount.WordWrap = false;
            // 
            // groupBoxFleetSize
            // 
            this.groupBoxFleetSize.Controls.Add(this.textBoxFleetSize);
            this.groupBoxFleetSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFleetSize.Location = new System.Drawing.Point(3, 23);
            this.groupBoxFleetSize.Name = "groupBoxFleetSize";
            this.groupBoxFleetSize.Padding = new System.Windows.Forms.Padding(6);
            this.groupBoxFleetSize.Size = new System.Drawing.Size(350, 390);
            this.groupBoxFleetSize.TabIndex = 0;
            this.groupBoxFleetSize.TabStop = false;
            this.groupBoxFleetSize.Text = "Fleet Size";
            // 
            // textBoxFleetSize
            // 
            this.textBoxFleetSize.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFleetSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFleetSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFleetSize.Location = new System.Drawing.Point(80, 152);
            this.textBoxFleetSize.Name = "textBoxFleetSize";
            this.textBoxFleetSize.ReadOnly = true;
            this.textBoxFleetSize.Size = new System.Drawing.Size(154, 85);
            this.textBoxFleetSize.TabIndex = 0;
            this.textBoxFleetSize.Text = "0";
            this.textBoxFleetSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxFleetSize.WordWrap = false;
            // 
            // buttonDesignNewShip
            // 
            this.buttonDesignNewShip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDesignNewShip.Location = new System.Drawing.Point(359, 419);
            this.buttonDesignNewShip.Name = "buttonDesignNewShip";
            this.buttonDesignNewShip.Size = new System.Drawing.Size(350, 93);
            this.buttonDesignNewShip.TabIndex = 2;
            this.buttonDesignNewShip.Text = "Design New Ship";
            this.buttonDesignNewShip.UseVisualStyleBackColor = true;
            this.buttonDesignNewShip.Click += new System.EventHandler(this.buttonDesignNewShip_Click);
            // 
            // FormFleetTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 555);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormFleetTracker";
            this.Text = "Fleet Tracker";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.groupBoxWindowCount.ResumeLayout(false);
            this.groupBoxWindowCount.PerformLayout();
            this.groupBoxFleetSize.ResumeLayout(false);
            this.groupBoxFleetSize.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.GroupBox groupBoxWindowCount;
        private System.Windows.Forms.TextBox textBoxWindowCount;
        private System.Windows.Forms.GroupBox groupBoxFleetSize;
        private System.Windows.Forms.TextBox textBoxFleetSize;
        private System.Windows.Forms.Button buttonDesignNewShip;
    }
}


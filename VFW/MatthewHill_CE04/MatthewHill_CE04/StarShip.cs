﻿using System;
/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    public class Starship
    {
        //Properties of a starship
        string name;
        string shipClass;
        bool ftl;
        decimal speed;
        int indexNumber;

        //Getters and setters
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ShipClass
        {
            get
            {
                return shipClass;
            }
            set
            {
                shipClass = value;
            }
        }

        public bool FTL
        {
            get
            {
                return ftl;
            }
            set
            {
                ftl = value;
            }
        }

        public decimal Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public int IndexNumber
        {
            get
            {
                return indexNumber;
            }
            set
            {
                indexNumber = value;
            }
        }

        //Constructor for instantiating a starship from a files delimited string
        public Starship(string str)
        {
            string[] stats = str.Split('|');
            name = stats[0];
            shipClass = stats[1];
            decimal spd;
            if (decimal.TryParse(stats[2], out spd))
            {
                speed = spd;
            }
            bool loadFtl;
            if (Boolean.TryParse(stats[3], out loadFtl))
            {
                ftl = loadFtl;
            }

        }

        //Constructor for instantiating a blank starship
        public Starship()
        {
            name = "";
            shipClass = "";
            speed = 0.1m;
            ftl = false;
        }

        //String to save the starship with a write & streamwriter.
        public string SaveString()
        {
            string saveValue = "";

            saveValue = name + "|" + shipClass + "|" + speed + "|" + ftl;

            return saveValue;
        }

        //Overridden ToString method
        public override string ToString()
        {
            return shipClass + ": " + name;
        }



    }
}

﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    partial class FormFleet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFleet));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewFleet = new System.Windows.Forms.ListView();
            this.imageListShips = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(698, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Image = global::MatthewHill_CE04.Properties.Resources.Eraser;
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(113, 36);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click_1);
            // 
            // listViewFleet
            // 
            this.listViewFleet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewFleet.LargeImageList = this.imageListShips;
            this.listViewFleet.Location = new System.Drawing.Point(0, 40);
            this.listViewFleet.MultiSelect = false;
            this.listViewFleet.Name = "listViewFleet";
            this.listViewFleet.Size = new System.Drawing.Size(698, 549);
            this.listViewFleet.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewFleet.TabIndex = 1;
            this.listViewFleet.UseCompatibleStateImageBehavior = false;
            this.listViewFleet.DoubleClick += new System.EventHandler(this.listViewFleet_DoubleClick);
            // 
            // imageListShips
            // 
            this.imageListShips.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListShips.ImageStream")));
            this.imageListShips.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListShips.Images.SetKeyName(0, "Carrier.png");
            this.imageListShips.Images.SetKeyName(1, "Freighter.png");
            this.imageListShips.Images.SetKeyName(2, "Cruiser.png");
            this.imageListShips.Images.SetKeyName(3, "Frigate.png");
            this.imageListShips.Images.SetKeyName(4, "Bomber.png");
            this.imageListShips.Images.SetKeyName(5, "Fighter.png");
            // 
            // FormFleet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 589);
            this.Controls.Add(this.listViewFleet);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormFleet";
            this.Text = "FormFleet";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormFleet_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ListView listViewFleet;
        private System.Windows.Forms.ImageList imageListShips;
    }
}
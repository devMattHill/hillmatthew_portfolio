﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    public partial class FormFleet : Form
    {
        public EventHandler FormFleetClosed;
        public EventHandler FormFleetCleared;
        public EventHandler FormFleetSelected;


        //Creating a property for passing ship data to a FormShipDesign
        public Starship selectedShip = new Starship();

        //Initializes this window
        public FormFleet()
        {
            InitializeComponent();
        }
        
        //Triggers when this form closes
        private void FormFleet_FormClosed(object sender, FormClosedEventArgs e)
        {//Checks to ensure there is a listener
            if (FormFleetClosed != null)
            {//Then Fires
                FormFleetClosed(this, new EventArgs());
            }
        }

        //Method triggered by a call from the FormFleetTracker
        public void UpdateFleetList(object sender, EventArgs e)
        {
            //Instantiates a useable version of the main form
            FormFleetTracker getShips = (FormFleetTracker)sender;
            //Pulls the public list named "fleet" from the usable FormFleetTracker
            List < Starship > fleet = getShips.fleet;
            //Clears the current list view
            listViewFleet.Items.Clear();
            //Populates the list view with all items in the fleet list
            {
                //loop through the List
                foreach(Starship ship in fleet){
                    //Instantiate a listview item
                    ListViewItem lvi = new ListViewItem();
                    //Add to the properties of that listview item
                    lvi.Text = ship.ToString();
                    lvi.ImageIndex = ship.IndexNumber;
                    lvi.Tag = ship;
                    //Add that list view item to the listview
                    listViewFleet.Items.Add(lvi);
                }
            }
        }

        //Triggers on listviewitem doubleclick
        //Loads a FormShipDesign with the appropriate information
        private void listViewFleet_DoubleClick(object sender, EventArgs e)
        {
            //set values for the selected ship
            selectedShip = listViewFleet.SelectedItems[0].Tag as Starship;
            //If the handler is present
            if (FormFleetSelected != null)
            {//Fire
                FormFleetSelected(this, new EventArgs());
            }
        }
        
        //Fires when the user presses Clear in the top toolstrip
        private void clearToolStripMenuItem_Click_1(object sender, EventArgs e)
        {//If the handler is present
            if(FormFleetCleared != null)
            {//Fire
                FormFleetCleared(this, new EventArgs());
            }
        }
    }
}

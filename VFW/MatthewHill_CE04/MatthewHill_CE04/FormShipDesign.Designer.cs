﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE04: List Views
*/
namespace MatthewHill_CE04
{
    partial class FormShipDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addToFleetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddToFleet = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelSubLightSpeed = new System.Windows.Forms.Label();
            this.numericUpDownSubLightSpeed = new System.Windows.Forms.NumericUpDown();
            this.comboBoxShipClass = new System.Windows.Forms.ComboBox();
            this.checkBoxFtlCapable = new System.Windows.Forms.CheckBox();
            this.labelStarshipClass = new System.Windows.Forms.Label();
            this.labelStarshipName = new System.Windows.Forms.Label();
            this.textStarshipName = new System.Windows.Forms.TextBox();
            this.buttonClearForm = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToFleetToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(752, 42);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addToFleetToolStripMenuItem
            // 
            this.addToFleetToolStripMenuItem.Name = "addToFleetToolStripMenuItem";
            this.addToFleetToolStripMenuItem.Size = new System.Drawing.Size(161, 38);
            this.addToFleetToolStripMenuItem.Text = "Add To Fleet";
            this.addToFleetToolStripMenuItem.Click += new System.EventHandler(this.addToFleetToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.buttonAddToFleet, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonClearForm, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(752, 587);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // buttonAddToFleet
            // 
            this.buttonAddToFleet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddToFleet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonAddToFleet.Location = new System.Drawing.Point(3, 476);
            this.buttonAddToFleet.Name = "buttonAddToFleet";
            this.buttonAddToFleet.Size = new System.Drawing.Size(595, 108);
            this.buttonAddToFleet.TabIndex = 23;
            this.buttonAddToFleet.Text = "Add To Fleet";
            this.buttonAddToFleet.UseVisualStyleBackColor = true;
            this.buttonAddToFleet.Click += new System.EventHandler(this.buttonAddToFleet_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelSubLightSpeed);
            this.groupBox1.Controls.Add(this.numericUpDownSubLightSpeed);
            this.groupBox1.Controls.Add(this.comboBoxShipClass);
            this.groupBox1.Controls.Add(this.checkBoxFtlCapable);
            this.groupBox1.Controls.Add(this.labelStarshipClass);
            this.groupBox1.Controls.Add(this.labelStarshipName);
            this.groupBox1.Controls.Add(this.textStarshipName);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(595, 447);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Starship Details";
            // 
            // labelSubLightSpeed
            // 
            this.labelSubLightSpeed.AutoSize = true;
            this.labelSubLightSpeed.Location = new System.Drawing.Point(31, 155);
            this.labelSubLightSpeed.Name = "labelSubLightSpeed";
            this.labelSubLightSpeed.Size = new System.Drawing.Size(310, 25);
            this.labelSubLightSpeed.TabIndex = 22;
            this.labelSubLightSpeed.Text = "Sublight speed in AUs per hour";
            // 
            // numericUpDownSubLightSpeed
            // 
            this.numericUpDownSubLightSpeed.DecimalPlaces = 1;
            this.numericUpDownSubLightSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Location = new System.Drawing.Point(372, 153);
            this.numericUpDownSubLightSpeed.Maximum = new decimal(new int[] {
            74,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Name = "numericUpDownSubLightSpeed";
            this.numericUpDownSubLightSpeed.Size = new System.Drawing.Size(120, 31);
            this.numericUpDownSubLightSpeed.TabIndex = 21;
            this.numericUpDownSubLightSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // comboBoxShipClass
            // 
            this.comboBoxShipClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShipClass.FormattingEnabled = true;
            this.comboBoxShipClass.Items.AddRange(new object[] {
            "Carrier",
            "Freighter",
            "Cruiser",
            "Frigate",
            "Bomber",
            "Fighter"});
            this.comboBoxShipClass.Location = new System.Drawing.Point(269, 100);
            this.comboBoxShipClass.Name = "comboBoxShipClass";
            this.comboBoxShipClass.Size = new System.Drawing.Size(320, 33);
            this.comboBoxShipClass.TabIndex = 20;
            // 
            // checkBoxFtlCapable
            // 
            this.checkBoxFtlCapable.AutoSize = true;
            this.checkBoxFtlCapable.Location = new System.Drawing.Point(36, 219);
            this.checkBoxFtlCapable.Name = "checkBoxFtlCapable";
            this.checkBoxFtlCapable.Size = new System.Drawing.Size(168, 29);
            this.checkBoxFtlCapable.TabIndex = 18;
            this.checkBoxFtlCapable.Text = "FTL Capable";
            this.checkBoxFtlCapable.UseVisualStyleBackColor = true;
            // 
            // labelStarshipClass
            // 
            this.labelStarshipClass.AutoSize = true;
            this.labelStarshipClass.Location = new System.Drawing.Point(31, 103);
            this.labelStarshipClass.Name = "labelStarshipClass";
            this.labelStarshipClass.Size = new System.Drawing.Size(151, 25);
            this.labelStarshipClass.TabIndex = 17;
            this.labelStarshipClass.Text = "Starship Class";
            // 
            // labelStarshipName
            // 
            this.labelStarshipName.AutoSize = true;
            this.labelStarshipName.Location = new System.Drawing.Point(29, 52);
            this.labelStarshipName.Name = "labelStarshipName";
            this.labelStarshipName.Size = new System.Drawing.Size(153, 25);
            this.labelStarshipName.TabIndex = 16;
            this.labelStarshipName.Text = "Starship Name";
            // 
            // textStarshipName
            // 
            this.textStarshipName.Location = new System.Drawing.Point(269, 52);
            this.textStarshipName.Name = "textStarshipName";
            this.textStarshipName.Size = new System.Drawing.Size(320, 31);
            this.textStarshipName.TabIndex = 15;
            // 
            // buttonClearForm
            // 
            this.buttonClearForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonClearForm.Location = new System.Drawing.Point(604, 476);
            this.buttonClearForm.Name = "buttonClearForm";
            this.buttonClearForm.Size = new System.Drawing.Size(145, 108);
            this.buttonClearForm.TabIndex = 19;
            this.buttonClearForm.Text = "Clear Form";
            this.buttonClearForm.UseVisualStyleBackColor = true;
            this.buttonClearForm.Click += new System.EventHandler(this.buttonClearForm_Click);
            // 
            // FormShipDesign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 629);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormShipDesign";
            this.Text = "FormShipDesign";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormShipDesign_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToFleetToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonAddToFleet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelSubLightSpeed;
        private System.Windows.Forms.NumericUpDown numericUpDownSubLightSpeed;
        private System.Windows.Forms.ComboBox comboBoxShipClass;
        private System.Windows.Forms.CheckBox checkBoxFtlCapable;
        private System.Windows.Forms.Label labelStarshipClass;
        private System.Windows.Forms.Label labelStarshipName;
        private System.Windows.Forms.TextBox textStarshipName;
        private System.Windows.Forms.Button buttonClearForm;
    }
}
﻿namespace MatthewHill_CE03
{
    partial class FormShipViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxInput = new System.Windows.Forms.GroupBox();
            this.buttonAddToFleet = new System.Windows.Forms.Button();
            this.labelSubLightSpeed = new System.Windows.Forms.Label();
            this.numericUpDownSubLightSpeed = new System.Windows.Forms.NumericUpDown();
            this.comboBoxShipClass = new System.Windows.Forms.ComboBox();
            this.checkBoxFtlCapable = new System.Windows.Forms.CheckBox();
            this.buttonClearForm = new System.Windows.Forms.Button();
            this.labelStarshipClass = new System.Windows.Forms.Label();
            this.labelStarshipName = new System.Windows.Forms.Label();
            this.textStarshipName = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxInput
            // 
            this.groupBoxInput.AutoSize = true;
            this.groupBoxInput.Controls.Add(this.buttonAddToFleet);
            this.groupBoxInput.Controls.Add(this.labelSubLightSpeed);
            this.groupBoxInput.Controls.Add(this.numericUpDownSubLightSpeed);
            this.groupBoxInput.Controls.Add(this.comboBoxShipClass);
            this.groupBoxInput.Controls.Add(this.checkBoxFtlCapable);
            this.groupBoxInput.Controls.Add(this.buttonClearForm);
            this.groupBoxInput.Controls.Add(this.labelStarshipClass);
            this.groupBoxInput.Controls.Add(this.labelStarshipName);
            this.groupBoxInput.Controls.Add(this.textStarshipName);
            this.groupBoxInput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxInput.Location = new System.Drawing.Point(0, 40);
            this.groupBoxInput.Name = "groupBoxInput";
            this.groupBoxInput.Size = new System.Drawing.Size(600, 323);
            this.groupBoxInput.TabIndex = 11;
            this.groupBoxInput.TabStop = false;
            this.groupBoxInput.Text = "Starship Details";
            // 
            // buttonAddToFleet
            // 
            this.buttonAddToFleet.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonAddToFleet.Location = new System.Drawing.Point(193, 250);
            this.buttonAddToFleet.Name = "buttonAddToFleet";
            this.buttonAddToFleet.Size = new System.Drawing.Size(310, 43);
            this.buttonAddToFleet.TabIndex = 14;
            this.buttonAddToFleet.Text = "Add To Fleet";
            this.buttonAddToFleet.UseVisualStyleBackColor = true;
            this.buttonAddToFleet.Click += new System.EventHandler(this.buttonAddToFleet_Click);
            // 
            // labelSubLightSpeed
            // 
            this.labelSubLightSpeed.AutoSize = true;
            this.labelSubLightSpeed.Location = new System.Drawing.Point(24, 122);
            this.labelSubLightSpeed.Name = "labelSubLightSpeed";
            this.labelSubLightSpeed.Size = new System.Drawing.Size(310, 25);
            this.labelSubLightSpeed.TabIndex = 11;
            this.labelSubLightSpeed.Text = "Sublight speed in AUs per hour";
            // 
            // numericUpDownSubLightSpeed
            // 
            this.numericUpDownSubLightSpeed.DecimalPlaces = 1;
            this.numericUpDownSubLightSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Location = new System.Drawing.Point(383, 120);
            this.numericUpDownSubLightSpeed.Maximum = new decimal(new int[] {
            74,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Name = "numericUpDownSubLightSpeed";
            this.numericUpDownSubLightSpeed.Size = new System.Drawing.Size(120, 31);
            this.numericUpDownSubLightSpeed.TabIndex = 10;
            this.numericUpDownSubLightSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // comboBoxShipClass
            // 
            this.comboBoxShipClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShipClass.FormattingEnabled = true;
            this.comboBoxShipClass.Items.AddRange(new object[] {
            "Carrier",
            "Freighter",
            "Cruiser",
            "Frigate",
            "Bomber",
            "Fighter"});
            this.comboBoxShipClass.Location = new System.Drawing.Point(183, 77);
            this.comboBoxShipClass.Name = "comboBoxShipClass";
            this.comboBoxShipClass.Size = new System.Drawing.Size(320, 33);
            this.comboBoxShipClass.TabIndex = 9;
            // 
            // checkBoxFtlCapable
            // 
            this.checkBoxFtlCapable.AutoSize = true;
            this.checkBoxFtlCapable.Location = new System.Drawing.Point(29, 160);
            this.checkBoxFtlCapable.Name = "checkBoxFtlCapable";
            this.checkBoxFtlCapable.Size = new System.Drawing.Size(168, 29);
            this.checkBoxFtlCapable.TabIndex = 5;
            this.checkBoxFtlCapable.Text = "FTL Capable";
            this.checkBoxFtlCapable.UseVisualStyleBackColor = true;
            // 
            // buttonClearForm
            // 
            this.buttonClearForm.Location = new System.Drawing.Point(29, 250);
            this.buttonClearForm.Name = "buttonClearForm";
            this.buttonClearForm.Size = new System.Drawing.Size(158, 43);
            this.buttonClearForm.TabIndex = 8;
            this.buttonClearForm.Text = "Clear Form";
            this.buttonClearForm.UseVisualStyleBackColor = true;
            this.buttonClearForm.Click += new System.EventHandler(this.buttonClearForm_Click);
            // 
            // labelStarshipClass
            // 
            this.labelStarshipClass.AutoSize = true;
            this.labelStarshipClass.Location = new System.Drawing.Point(26, 80);
            this.labelStarshipClass.Name = "labelStarshipClass";
            this.labelStarshipClass.Size = new System.Drawing.Size(151, 25);
            this.labelStarshipClass.TabIndex = 3;
            this.labelStarshipClass.Text = "Starship Class";
            // 
            // labelStarshipName
            // 
            this.labelStarshipName.AutoSize = true;
            this.labelStarshipName.Location = new System.Drawing.Point(24, 42);
            this.labelStarshipName.Name = "labelStarshipName";
            this.labelStarshipName.Size = new System.Drawing.Size(153, 25);
            this.labelStarshipName.TabIndex = 2;
            this.labelStarshipName.Text = "Starship Name";
            // 
            // textStarshipName
            // 
            this.textStarshipName.Location = new System.Drawing.Point(183, 39);
            this.textStarshipName.Name = "textStarshipName";
            this.textStarshipName.Size = new System.Drawing.Size(320, 31);
            this.textStarshipName.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(600, 42);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(62, 38);
            this.listToolStripMenuItem.Text = "List";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // FormShipViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 363);
            this.Controls.Add(this.groupBoxInput);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormShipViewer";
            this.Text = "Ship Viewer";
            this.groupBoxInput.ResumeLayout(false);
            this.groupBoxInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxInput;
        private System.Windows.Forms.Button buttonAddToFleet;
        private System.Windows.Forms.Label labelSubLightSpeed;
        private System.Windows.Forms.NumericUpDown numericUpDownSubLightSpeed;
        private System.Windows.Forms.ComboBox comboBoxShipClass;
        private System.Windows.Forms.CheckBox checkBoxFtlCapable;
        private System.Windows.Forms.Button buttonClearForm;
        private System.Windows.Forms.Label labelStarshipClass;
        private System.Windows.Forms.Label labelStarshipName;
        private System.Windows.Forms.TextBox textStarshipName;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatthewHill_CE03
{
    public class Starship
    {
        //Properties of a starship
        string name;
        string shipClass;
        bool ftl;
        decimal speed;
        //Getters and setters
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ShipClass
        {
            get
            {
                return shipClass;
            }
            set
            {
                shipClass = value;
            }
        }

        public bool FTL
        {
            get
            {
                return ftl;
            }
            set
            {
                ftl = value;
            }
        }

        public decimal Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }
        
        //Constructor for instantiating a starship from a files delimited string
        public Starship(string str)
        {
            string[] stats = str.Split('|');
            name = stats[0];
            shipClass = stats[1];
            decimal spd;
            if (decimal.TryParse(stats[2], out spd))
            {
                speed = spd;
            }
            bool loadFtl;
            if (Boolean.TryParse(stats[3], out loadFtl))
            {
                ftl = loadFtl;
            }

        }
        //Constructor for instantiated a blank starship
        public Starship()
        {
            name = "";
            shipClass = "";
            speed = 0.1m;
            ftl = false;
        }


        //String to save the starship with a write & streamwriter.
        public string SaveString()
        {
            string saveValue = "";

            saveValue = name + "|" + shipClass + "|" + speed + "|" + ftl;

            return saveValue;
        }


        public override string ToString()
        {
            return shipClass + ": " + name;
        }



    }
}

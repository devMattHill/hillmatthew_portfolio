﻿namespace MatthewHill_CE03
{
    partial class DisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DisplayForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItemClear = new System.Windows.Forms.ToolStripMenuItem();
            this.listBoxFleet = new System.Windows.Forms.ListBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItemClear});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(634, 40);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItemClear
            // 
            this.fileToolStripMenuItemClear.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItemClear.Image")));
            this.fileToolStripMenuItemClear.Name = "fileToolStripMenuItemClear";
            this.fileToolStripMenuItemClear.Size = new System.Drawing.Size(113, 36);
            this.fileToolStripMenuItemClear.Text = "Clear";
            this.fileToolStripMenuItemClear.Click += new System.EventHandler(this.fileToolStripMenuItemClear_Click);
            // 
            // listBoxFleet
            // 
            this.listBoxFleet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxFleet.FormattingEnabled = true;
            this.listBoxFleet.ItemHeight = 25;
            this.listBoxFleet.Location = new System.Drawing.Point(0, 40);
            this.listBoxFleet.Name = "listBoxFleet";
            this.listBoxFleet.Size = new System.Drawing.Size(634, 333);
            this.listBoxFleet.TabIndex = 14;
            this.listBoxFleet.SelectedIndexChanged += new System.EventHandler(this.listBoxFleet_SelectedIndexChanged);
            // 
            // DisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 373);
            this.Controls.Add(this.listBoxFleet);
            this.Controls.Add(this.menuStrip1);
            this.Name = "DisplayForm";
            this.Text = "DisplayForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DisplayForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItemClear;
        public System.Windows.Forms.ListBox listBoxFleet;
    }
}
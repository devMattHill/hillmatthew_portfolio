﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MatthewHill_CE03
{
    public partial class DisplayForm : Form
    {
        //public event handler declarations
        public event EventHandler ClearFleetList;
        public event EventHandler DisplayFormClosed;
        public event EventHandler LoadShipData;


        public DisplayForm()
        {
            InitializeComponent();
        }


        //This event is called when the form closes.
        private void DisplayForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            //Checks to ensure there is a listener
            if (DisplayFormClosed != null)
            {//Then Fires
                DisplayFormClosed(this, new EventArgs());
            }
        }




        //Triggers with "clear" toolStripButton
        private void fileToolStripMenuItemClear_Click(object sender, EventArgs e)
        {
            //Checks to ensure there is a listener
            if(ClearFleetList != null)
            {//Then Fires
                ClearFleetList(this, new EventArgs());
            }
        }


        //Method triggered by an event that updates the listBos with list data
        public void UpdateList(object sender, EventArgs e)
        {
            //Instantiates a usable version of the main form
            FormShipViewer mainForm = (FormShipViewer)sender;
            //Pulls the public list named fleet from the new main form
            List<Starship> fleet = mainForm.fleet;
            //Clears out the list box
            listBoxFleet.Items.Clear();
            //Populates the list box with all items in the fleet list
            foreach(Starship ship in fleet)
            {
                listBoxFleet.Items.Add(ship);
            }
        }
       
        
        //Triggers when the user clicks on an item in the fleet list box
        private void listBoxFleet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(LoadShipData != null)
            {
                LoadShipData(this, new EventArgs());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MatthewHill_CE03
{
    public partial class FormShipViewer : Form
    {

        //Public event handlers to subscribe to an event handler on the display form
        public event EventHandler UpdateList;


        //List for storing Starships
        public List<Starship> fleet = new List<Starship>();

        //Property for modifying and extracting the form values
        public Starship shipValues
        {
            get
            {
                Starship tmp = new Starship();
                tmp.Name = textStarshipName.Text;
                tmp.ShipClass = comboBoxShipClass.Text;
                tmp.Speed = numericUpDownSubLightSpeed.Value;
                tmp.FTL = checkBoxFtlCapable.Checked;
                return tmp;
            }

            set
            {
                textStarshipName.Text =value.Name;
                comboBoxShipClass.Text =value.ShipClass;
                numericUpDownSubLightSpeed.Value = value.Speed;
                checkBoxFtlCapable.Checked = value.FTL;
            }
        } 


        public FormShipViewer()
        {
            InitializeComponent();
        }
    
        
        //Triggers when the user clicks the List>Display button and displays the displayWindow
        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (displayToolStripMenuItem.Checked == true)
            {
                //if the display menu is already checked do nothing
            }
            else //else load the DisplayWindow
            {
                //This adds a check mark next to the menu item. Be sure to change "check on click" to false in the 
                //form builder or the redundancy here will cause all kinds of problems.

                displayToolStripMenuItem.Checked = true;
                //instantiate the input form
                DisplayForm displayWindow = new DisplayForm();
                //Sets the displayWindow's events to be handled
                displayWindow.DisplayFormClosed += DisplayWindow_DisplayFormClosed;
                displayWindow.ClearFleetList += DisplayWindow_ClearFleetList;
                displayWindow.LoadShipData += DisplayWindow_LoadShipData;

                //subscribe to the UpdateList method on displayWindow
                UpdateList += displayWindow.UpdateList;


                //popup a modeless display form
                displayWindow.Show();

                //sets off the UpdateList listener so the displayWindow will
                //update the listBox with the current list information upon launch
                if (UpdateList != null)
                {
                    UpdateList(this, new EventArgs());
                }
            }
        }
     
        
        //Triggers when the user clicks on an item in the displayWindow's list box
        private void DisplayWindow_LoadShipData(object sender, EventArgs e)
        {
            //Instantiates a usable version of the displayForm
            DisplayForm useForm = (DisplayForm)sender;
            //Pulls the selected object from the list box
            //Note I had to make the list box properties public to make this work
            shipValues = useForm.listBoxFleet.SelectedItem as Starship;

        }


        //Triggers when the user clicks "clear list" on the display window
        //Loads a blank list into fleet
        //sets off the UpdateList listener so the displayWindow will
        //update the listBox with the now blank list information
        private void DisplayWindow_ClearFleetList(object sender, EventArgs e)
        {
            fleet = new List<Starship>();
            if (UpdateList != null)
            {
                UpdateList(this, new EventArgs());
            }

        }
 
        
        
        //Triggers when the Display Form closes
        private void DisplayWindow_DisplayFormClosed(object sender, EventArgs e)
        {
            //Unchecks the List>Display option
            displayToolStripMenuItem.Checked = false;
        }
 
        
        
        //Triggers when user clicks "add to fleet"
        //uses List.Add to add a starship class obj to the fleet list
        //resets the form values to defaults
        //sets off an event listener that causes the display form to update it's content with the new list data
        private void buttonAddToFleet_Click(object sender, EventArgs e)
        {
            //Adds the current ship value to the fleet list
            fleet.Add(shipValues);
            //sets the form back to default values
            shipValues = new Starship();
            //Checks if there is a listener for updating the listbox on the displayform
            if (UpdateList != null)
            {//Fires
                UpdateList(this, new EventArgs());
            }
        }
       
        
        
        
        //Triggers when user clicks the "clear form" button on the main form       
        private void buttonClearForm_Click(object sender, EventArgs e)
        {//resets all the values in the form to defaults
            shipValues = new Starship();
        }



        //Triggers when user selects List>Clear on the Main form
       
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        { //Calls the DisplayAWindow_ClearFleetList method
            DisplayWindow_ClearFleetList(this, new EventArgs());
        }
  
        
        //Triggers when user selects "exit"        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {//Closes the program
            Close();
        }
    }
}

﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE05: Custom Event Arguments
*/
using System;
using System.Windows.Forms;

namespace MatthewHill_CE05
{
    public partial class ShipDesigner : Form
    {
        //Event Handlers
        public EventHandler DesignWindow_buttonOK;
        public EventHandler DesignWindow_buttonApply;

        //Ship type variable that can be altered by radio buttons
        string shipType = "Freighter";

        //variable for imgIndex
        private int imgIndex;

        //Property for modifying and extracting the form values
        public SpaceShip shipValues
        {
            //Getters and Setters
            get
            {
                SpaceShip tmp = new SpaceShip();
                tmp.Name = textBoxName.Text;
                //Checks which radio button is check
                if (radioButtonCruiser.Checked)
                {
                    tmp.ShipType = "Cruiser";
                }
                else if (radioButtonDestroyer.Checked)
                {
                    tmp.ShipType = "Destroyer";
                }
                else
                {
                    tmp.ShipType = "Freighter";
                }
                tmp.CrewSize = numericUpDownCrewSize.Value;
                tmp.ActiveDuty = checkBoxActiveDuty.Checked;
                tmp.ImgIndex = imgIndex;
                return tmp;
            }

            set
            {
                textBoxName.Text = value.Name;
                shipType = value.ShipType;
                //Sets the appropriate radio button in the form based on the ship type
                if(shipType == "Cruiser")
                {
                    radioButtonCruiser.Checked = true;
                }
                else if(shipType == "Destroyer")
                {
                    radioButtonDestroyer.Checked = true;
                }
                else
                {
                    radioButtonFreighter.Checked = true;
                }
                numericUpDownCrewSize.Value = value.CrewSize;
                checkBoxActiveDuty.Checked = value.ActiveDuty;
                imgIndex = value.ImgIndex;
            }
        }
        
        //Initializes this window
        public ShipDesigner()
        {
            InitializeComponent();
        }

        //The next three events change the value of the shipType and imageIndex based on which radio button is clicked
        //1
        private void radioButtonCruiser_CheckedChanged(object sender, EventArgs e)
        {
            shipType = "Cruiser";
            imgIndex = 2;
        }
        //2
        private void radioButtonDestroyer_CheckedChanged(object sender, EventArgs e)
        {
            shipType = "Destroyer";
            imgIndex = 1;
        }
        //3
        private void radioButtonFreighter_CheckedChanged(object sender, EventArgs e)
        {
            shipType = "Freighter";
            imgIndex = 0;
        }

        //Triggers on Button Cancel
        private void buttonCancel_Click(object sender, EventArgs e)
        {//Closes the window
            Close();
        }

        //Triggers on Button OK
        public void buttonOK_Click(object sender, EventArgs e)
        {//Check for listener
            if(DesignWindow_buttonOK != null)
            {//Fire with default Event Arguments
                DesignWindow_buttonOK(this, new EventArgs());
                //And Close the form
                Close();
            }
        }

        //Triggers on Button Apply. The custom event args are not used until 
        //the listener in FleetViewer is set off by this. It's at the bottom.
        private void buttonApply_Click(object sender, EventArgs e)
        {//Checks for a listener
            if(DesignWindow_buttonApply != null)
            {//Fires
                DesignWindow_buttonApply(this, new EventArgs());
            }
        }

        //This is the method that modifies the listViewItem in the FleetViewer window
        public void HandleModifyShip(object sender, FleetViewer.ModifyShipEventArgs e)
        {//Instantiate a usable ship
            SpaceShip tmp = e.ShipToModify1.Tag as SpaceShip;
            //Apply the values from the current form
            tmp.Name = textBoxName.Text;
            tmp.ShipType = shipType;
            tmp.CrewSize = numericUpDownCrewSize.Value;
            tmp.ActiveDuty = checkBoxActiveDuty.Checked;
            tmp.ImgIndex = imgIndex;

            //Modify the remaining listViewItem properties.
            e.ShipToModify1.Text = tmp.ToString();
            e.ShipToModify1.ImageIndex = tmp.ImgIndex;
        }

    }
}

﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE05: Custom Event Arguments
*/
using System;

namespace MatthewHill_CE05
{
    public class SpaceShip
    {
        //Properties of a spaceship
        string name;
        decimal crewSize;
        bool activeDuty;
        string shipType;
        int imgIndex;
        
        //Getters and setters
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public decimal CrewSize
        {
            get
            {
                return crewSize;
            }
            set
            {
                crewSize = value;
            }
        }

        public string ShipType
        {
            get
            {
                return shipType;
            }
            set
            {
                shipType = value;
            }
        }

        public bool ActiveDuty
        {
            get
            {
                return activeDuty;
            }
            set
            {
                activeDuty = value;
            }
        }

        public int ImgIndex
        {
            get
            {
                return imgIndex;
            }
            set
            {
                imgIndex = value;
            }
        }

        

        //Constructor for instantiating a starship from a files delimited string
        public SpaceShip(string str)
        {
            string[] stats = str.Split('|');
            name = stats[0];
            shipType = stats[1];
            int size;
            if (int.TryParse(stats[2], out size))
            {
                crewSize = size;
            }
            bool loadActv;
            if (Boolean.TryParse(stats[3], out loadActv))
            {
                activeDuty = loadActv;
            }
        }

        //Constructor for instantiating a blank starship
        public SpaceShip()
        {
            name = "";
            shipType = "";
            crewSize = 0;
            activeDuty = false;
            imgIndex = 0;
        }

        //String to save the starship with a write & streamwriter.
        public string SaveString()
        {
            string saveValue = "";

            saveValue = name + "|" + shipType + "|" + crewSize + "|" + activeDuty + "|" + imgIndex;

            return saveValue;
        }

        //Overridden ToString method
        public override string ToString()
        {
            return shipType + " " + name;
        }

    }
}

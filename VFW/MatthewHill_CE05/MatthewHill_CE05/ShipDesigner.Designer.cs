﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE05: Custom Event Arguments
*/
namespace MatthewHill_CE05
{
    partial class ShipDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxShipDetails = new System.Windows.Forms.GroupBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCrewSize = new System.Windows.Forms.Label();
            this.checkBoxActiveDuty = new System.Windows.Forms.CheckBox();
            this.radioButtonCruiser = new System.Windows.Forms.RadioButton();
            this.radioButtonDestroyer = new System.Windows.Forms.RadioButton();
            this.radioButtonFreighter = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.numericUpDownCrewSize = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBoxShipDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCrewSize)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxShipDetails, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(882, 817);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(370, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 79);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.buttonOK, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonApply, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonCancel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 657);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(876, 157);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // buttonOK
            // 
            this.buttonOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOK.Location = new System.Drawing.Point(3, 3);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(285, 151);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonApply.Location = new System.Drawing.Point(294, 3);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(285, 151);
            this.buttonApply.TabIndex = 1;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Visible = false;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(585, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(288, 151);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBoxShipDetails
            // 
            this.groupBoxShipDetails.Controls.Add(this.numericUpDownCrewSize);
            this.groupBoxShipDetails.Controls.Add(this.textBoxName);
            this.groupBoxShipDetails.Controls.Add(this.label1);
            this.groupBoxShipDetails.Controls.Add(this.radioButtonFreighter);
            this.groupBoxShipDetails.Controls.Add(this.radioButtonDestroyer);
            this.groupBoxShipDetails.Controls.Add(this.radioButtonCruiser);
            this.groupBoxShipDetails.Controls.Add(this.checkBoxActiveDuty);
            this.groupBoxShipDetails.Controls.Add(this.labelCrewSize);
            this.groupBoxShipDetails.Controls.Add(this.labelName);
            this.groupBoxShipDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxShipDetails.Location = new System.Drawing.Point(3, 3);
            this.groupBoxShipDetails.Name = "groupBoxShipDetails";
            this.groupBoxShipDetails.Size = new System.Drawing.Size(876, 647);
            this.groupBoxShipDetails.TabIndex = 1;
            this.groupBoxShipDetails.TabStop = false;
            this.groupBoxShipDetails.Text = "Space Ship Details";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(4, 73);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(68, 25);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name";
            // 
            // labelCrewSize
            // 
            this.labelCrewSize.AutoSize = true;
            this.labelCrewSize.Location = new System.Drawing.Point(4, 141);
            this.labelCrewSize.Name = "labelCrewSize";
            this.labelCrewSize.Size = new System.Drawing.Size(109, 25);
            this.labelCrewSize.TabIndex = 1;
            this.labelCrewSize.Text = "Crew Size";
            // 
            // checkBoxActiveDuty
            // 
            this.checkBoxActiveDuty.AutoSize = true;
            this.checkBoxActiveDuty.Location = new System.Drawing.Point(9, 211);
            this.checkBoxActiveDuty.Name = "checkBoxActiveDuty";
            this.checkBoxActiveDuty.Size = new System.Drawing.Size(153, 29);
            this.checkBoxActiveDuty.TabIndex = 2;
            this.checkBoxActiveDuty.Text = "Active Duty";
            this.checkBoxActiveDuty.UseVisualStyleBackColor = true;
            // 
            // radioButtonCruiser
            // 
            this.radioButtonCruiser.AutoSize = true;
            this.radioButtonCruiser.Location = new System.Drawing.Point(9, 353);
            this.radioButtonCruiser.Name = "radioButtonCruiser";
            this.radioButtonCruiser.Size = new System.Drawing.Size(112, 29);
            this.radioButtonCruiser.TabIndex = 3;
            this.radioButtonCruiser.Text = "Cruiser";
            this.radioButtonCruiser.UseVisualStyleBackColor = true;
            this.radioButtonCruiser.CheckedChanged += new System.EventHandler(this.radioButtonCruiser_CheckedChanged);
            // 
            // radioButtonDestroyer
            // 
            this.radioButtonDestroyer.AutoSize = true;
            this.radioButtonDestroyer.Location = new System.Drawing.Point(294, 353);
            this.radioButtonDestroyer.Name = "radioButtonDestroyer";
            this.radioButtonDestroyer.Size = new System.Drawing.Size(136, 29);
            this.radioButtonDestroyer.TabIndex = 4;
            this.radioButtonDestroyer.TabStop = true;
            this.radioButtonDestroyer.Text = "Destroyer";
            this.radioButtonDestroyer.UseVisualStyleBackColor = true;
            this.radioButtonDestroyer.CheckedChanged += new System.EventHandler(this.radioButtonDestroyer_CheckedChanged);
            // 
            // radioButtonFreighter
            // 
            this.radioButtonFreighter.AutoSize = true;
            this.radioButtonFreighter.Checked = true;
            this.radioButtonFreighter.Location = new System.Drawing.Point(585, 353);
            this.radioButtonFreighter.Name = "radioButtonFreighter";
            this.radioButtonFreighter.Size = new System.Drawing.Size(129, 29);
            this.radioButtonFreighter.TabIndex = 5;
            this.radioButtonFreighter.TabStop = true;
            this.radioButtonFreighter.Text = "Freighter";
            this.radioButtonFreighter.UseVisualStyleBackColor = true;
            this.radioButtonFreighter.CheckedChanged += new System.EventHandler(this.radioButtonFreighter_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ship Type";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(78, 66);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(316, 31);
            this.textBoxName.TabIndex = 7;
            // 
            // numericUpDownCrewSize
            // 
            this.numericUpDownCrewSize.Location = new System.Drawing.Point(119, 141);
            this.numericUpDownCrewSize.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownCrewSize.Name = "numericUpDownCrewSize";
            this.numericUpDownCrewSize.Size = new System.Drawing.Size(275, 31);
            this.numericUpDownCrewSize.TabIndex = 8;
            // 
            // ShipDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 817);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ShipDesigner";
            this.Text = "ShipDesigner";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBoxShipDetails.ResumeLayout(false);
            this.groupBoxShipDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCrewSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxShipDetails;
        private System.Windows.Forms.Label labelCrewSize;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButtonFreighter;
        private System.Windows.Forms.RadioButton radioButtonDestroyer;
        private System.Windows.Forms.RadioButton radioButtonCruiser;
        private System.Windows.Forms.CheckBox checkBoxActiveDuty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownCrewSize;
        private System.Windows.Forms.TextBox textBoxName;
        public System.Windows.Forms.Button buttonApply;
    }
}
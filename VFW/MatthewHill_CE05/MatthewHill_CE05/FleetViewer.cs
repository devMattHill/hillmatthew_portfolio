﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE05: Custom Event Arguments
*/
using System;
using System.Windows.Forms;

namespace MatthewHill_CE05
{
    public partial class FleetViewer : Form
    {
        //public Event Handlers
        public EventHandler<ModifyShipEventArgs> ModifyShip;

        //Initializes this form
        public FleetViewer()
        {
            InitializeComponent();
        }

        //Int Variable to track fleet size
        int fleetSize = 0;

        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {//Closes the program
            Close();
        }

        //Triggers on Ship>New
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {//Calls the CreateDesignWindow method with a bool value of true to note that this will be a new ship
            CreateDesignWindow(sender, e, true);
        }
        //Triggers on doubleClick of a listViewFleet.Item
       private void listViewFleet_DoubleClick(object sender, EventArgs e)
        {//Calls the CreateDesignWindow method with a bool value of false to note that this will load an existing ship
            CreateDesignWindow(sender, e, false);
        }

        //DesignWindowCreation
        private void CreateDesignWindow(object sender, EventArgs e,bool newShip)
        {
           
            //instantiate the ShipDesignerWindow
            ShipDesigner designWindow = new ShipDesigner();

            //Sets the designWindow's events to be handled
            
            designWindow.DesignWindow_buttonOK += DesignWindow_buttonOK;
            designWindow.DesignWindow_buttonApply += DesignWindow_buttonApply;
            ModifyShip += designWindow.HandleModifyShip;

            //Checks if this should be a blank form or a filled out form
            if (!newShip) //If not a new ship
            {//fill in form values with selected item values
                designWindow.shipValues = listViewFleet.SelectedItems[0].Tag as SpaceShip;
                //and show the apply button
                designWindow.buttonApply.Visible = true;
            }
            //else load the standard form

            //popup a modeless display form
            designWindow.Show();

        }

        //Triggers on File>Clear
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {//Removes all items from the listViewFleet
            listViewFleet.Items.Clear();
            //reset fleet size
            fleetSize = 0;
            //Update bottom status strip
            UpdateFleetSize(fleetSize);
        }

        //Triggers on View>Small
        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Changes the listView to SmallIcon
            listViewFleet.View = View.SmallIcon;
            //Checks the View>Small menu item
            smallToolStripMenuItem.Checked = true;
            //Unchecks the View>Large menu item
            largeToolStripMenuItem.Checked = false;
        }

        //Triggers on View>Large
        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Changes the listView to LargeIcon
            listViewFleet.View = View.LargeIcon;
            //Unchecks the View>Small menu item
            smallToolStripMenuItem.Checked = false;
            //Checks the View>Large menu item
            largeToolStripMenuItem.Checked = true;
        }

        //Triggers off the event handler listening for ShipDesigner.ButtonOK_Click
        public void DesignWindow_buttonOK(object sender, EventArgs e)
        {   //Instantiate a usable form
            ShipDesigner designForm = (ShipDesigner)sender;
            //Instantiate a usable SpaceShip
            SpaceShip getShip = designForm.shipValues as SpaceShip;
            //Instantiate a listViewItem
            ListViewItem lvi = new ListViewItem();
            //fill it with ship values
            lvi.Text = getShip.ToString();
            lvi.ImageIndex = getShip.ImgIndex;
            lvi.Tag = getShip;
            //Add item to listView
            listViewFleet.Items.Add(lvi);
            //update fleet size
            fleetSize++;
            //Update status bar
            UpdateFleetSize(fleetSize);
        }

        //Method for updating fleet size
        private void UpdateFleetSize(int size)
        {//Updates the status bar at the bottom of the window
            toolStripStatusLabelFleetSize.Text = "Fleet Size: " + size;
        }

        //internal class for custom event args
        public class ModifyShipEventArgs : EventArgs
        {
            //needed in order to update the actual LVI and not just the SpaceShip Object
            ListViewItem ShipToModify;

            //Getters and setters
            public ListViewItem ShipToModify1
            {
                get
                {
                    return ShipToModify;
                }

                set
                {
                    ShipToModify = value;
                }
            }

            //constructor that can take in a list view item, easier than using setters
            public ModifyShipEventArgs(ListViewItem lvi)
            {
                ShipToModify = lvi;
            }
        }

        //Triggers when the DesignWindow buttonApply is pressed the Custom EventArgs are used in here.
        private void DesignWindow_buttonApply(object sender, EventArgs e)
        {//Sets off the ModifyShip event
            //if the listener exists and and item is selected
            if (ModifyShip != null && listViewFleet.SelectedItems.Count > 0)
            {//Fire
                ModifyShip(this, new ModifyShipEventArgs(listViewFleet.SelectedItems[0]));
            }
        }

    }
}

﻿using System;

namespace MatthewHill_CE01
{
    public class Starship
    {
        //Properties of a starship
        string name;
        string shipClass;
        bool ftl;
        decimal speed;
        string alliance;
        //Getters and setters
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ShipClass
        {
            get
            {
                return shipClass;
            }
            set
            {
                shipClass = value;
            }
        }

        public bool FTL
        {
            get
            {
                return ftl;
            }
            set
            {
                ftl = value;
            }
        }

        public decimal Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public string Alliance
        {
            get
            {
                return alliance;
            }
            set
            {
                alliance = value;
            }
        }





        //Constructor for instantiating a starship from a files delimited string
        public Starship(string str)
        {
            string[] stats = str.Split('|');
            name = stats[0];
            shipClass = stats[1];
            decimal spd;
            if(decimal.TryParse(stats[2], out spd))
            {
                speed = spd;
            }
            alliance = stats[3];
            bool loadFtl;
            if (Boolean.TryParse(stats[4], out loadFtl))
            {
                ftl = loadFtl;
            }

        }
        //Constructor for instantiated a blank starship
        public Starship()
        {
            name = "";
            shipClass = "";
            speed = 0.1m;
            alliance = "";
            ftl = false;
        }


        //String to save the starship with a write & streamwriter.
        public string SaveString()
        {
            string saveValue = "";

            saveValue = name + "|" + shipClass + "|" + speed + "|" + alliance + "|" + ftl;

            return saveValue;
        }


        public override string ToString()
        {
            return alliance+" "+shipClass + ": " + name;
        }



    }


}

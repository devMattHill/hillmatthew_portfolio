﻿using System;
using System.Windows.Forms;
using System.IO;

namespace MatthewHill_CE01
{
    public partial class FormStarship : Form
    {
        //Sets an active ship to be compared to any new ship information.
        Starship activeShip = new Starship();
        Starship noActiveShip = new Starship("noActiveShip| |0.1| |false");
        

        public Starship DetailsFalcorian
        {
            get
            {
                Starship s = new Starship();
                s.Name = textStarshipName.Text;
                s.ShipClass = comboBoxShipClass.Text;
                s.Speed = numericUpDownSubLightSpeed.Value;
                s.Alliance = "Falcorian Empire";
                s.FTL = checkBoxFtlCapable.Checked;

                return s;
            }

            set
            {
                textStarshipName.Text = value.Name;
                comboBoxShipClass.Text = value.ShipClass;
                numericUpDownSubLightSpeed.Value = value.Speed;
                textBoxAlliance.Text = value.Alliance;
                checkBoxFtlCapable.Checked = value.FTL;
            }
        }

        public Starship DetailsPhoenix
        {
            get
            {
                Starship s = new Starship();
                s.Name = textStarshipName.Text;
                s.ShipClass = comboBoxShipClass.Text;
                s.Speed = numericUpDownSubLightSpeed.Value;
                s.Alliance = "Phoenix Kingdom";
                s.FTL = checkBoxFtlCapable.Checked;

                return s;
            }

            set
            {
                textStarshipName.Text = value.Name;
                comboBoxShipClass.Text = value.ShipClass;
                numericUpDownSubLightSpeed.Value = value.Speed;
                textBoxAlliance.Text = value.Alliance;
                checkBoxFtlCapable.Checked = value.FTL;
            }
        }

        public Starship DetailsUnflagged
        {
            get
            {
                Starship s = new Starship();
                s.Name = textStarshipName.Text;
                s.ShipClass = comboBoxShipClass.Text;
                s.Speed = numericUpDownSubLightSpeed.Value;
                s.Alliance = "Unflagged";
                s.FTL = checkBoxFtlCapable.Checked;

                return s;
            }

            set
            {
                textStarshipName.Text = value.Name;
                comboBoxShipClass.Text = value.ShipClass;
                numericUpDownSubLightSpeed.Value = value.Speed;
                textBoxAlliance.Text = value.Alliance;
                checkBoxFtlCapable.Checked = value.FTL;
            }
        }




        //Path for saving files
        static string saveFolder = @"..\..\SavedShips";
        //Initializes the Form1 main form
        public FormStarship()
        {
            InitializeComponent();
        }
        
        
        //Makes the File>Exit in the ToolStripMenu exit the program
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //Triggered by the default button
        private void buttonDefault_Click(object sender, EventArgs e)
        {
            //resets the form to default values
            DetailsFalcorian = new Starship();
            activeShip = noActiveShip;
        }

        //This method is triggered by either the save button or the File>Save menu
        //It writes out a file with the ship name as the file name.
        private void buttonSave_Click(object sender, EventArgs e)
        {
            //Creates the directory if it does not already exist.
            Directory.CreateDirectory(saveFolder);

            string saveData = "";
            //opens a save file dialog for storing the starship information
            if (saveFileDialogStarship.ShowDialog() == DialogResult.OK)
            { 
               
                
                //opens a streamwriter to save the ship file as a .txt
                using (StreamWriter outStream = new StreamWriter(saveFileDialogStarship.FileName))
                {
                    //cycles through each ship in each listBox and saves them with a WriteLine
                    //This should be neater but, the fleet names are static.
                    foreach (Starship ship in listBoxFalcorianEmpire.Items)
                    {
                        outStream.WriteLine(ship.SaveString());
                    }
                    foreach (Starship ship in listBoxPhoenixKingdom.Items)
                    {
                        outStream.WriteLine(ship.SaveString());
                    }
                    foreach (Starship ship in listBoxUnflagged.Items)
                    {
                        outStream.WriteLine(ship.SaveString());
                    }
                   
                }
            }
            

        }
       
     
        //Triggered by the Load button or the File>Load menu item
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            //Creates the saved ships directory if it does not exist
            //I can't get the openDialogBox to default to the directory though :( #continue
            Directory.CreateDirectory(saveFolder);


            //Opens an openFialDialog for locating saved ship files.
            if (openFileDialogLoadStarship.ShowDialog() == DialogResult.OK)
            {

                //Clears out the current listboxes before loading the new file.
                listBoxFalcorianEmpire.Items.Clear();
                listBoxPhoenixKingdom.Items.Clear();
                listBoxUnflagged.Items.Clear();
                //opens a streamreader to read the selected file
                using (StreamReader inStream = new StreamReader(openFileDialogLoadStarship.FileName))
                {
                    //Takes the information from the file and fills in the starship properties
                    while (inStream.Peek() > -1)
                    {
                        string line = inStream.ReadLine();
                        Starship tmpStarship = new Starship(line);
                        if (tmpStarship.Alliance == "Falcorian Empire")
                        {
                            listBoxFalcorianEmpire.Items.Add(tmpStarship);
                        }
                        else if (tmpStarship.Alliance == "Phoenix Kingdom")
                        {
                            listBoxPhoenixKingdom.Items.Add(tmpStarship);
                        }
                        else
                        {
                            listBoxUnflagged.Items.Add(tmpStarship);
                        }

                    }
                }
            }
            else
            {
                //Loads a "failed to load" name if the user cancels the openFileDialog
                DetailsFalcorian = new Starship("Failed to Load| |0.1| |False");
            }
            

        }
        //The below function triggers when the user adds a ship to the falcorian empire
        private void buttonAddToFalcorianEmpire_Click(object sender, EventArgs e)
        {
            //First it determines if the details match a ship that was loaded from a listBox
            if (activeShip.Name == DetailsFalcorian.Name)
            {
                //If that is the case and the alliance has changed
                if (activeShip.Alliance == "Unflagged")
                {
                    //The old ship is removed from the appropriate list
                    listBoxUnflagged.Items.Remove(activeShip);
                    //And populated in the new one
                    listBoxFalcorianEmpire.Items.Add(DetailsFalcorian);
                }
                else if (activeShip.Alliance == "Phoenix Kingdom")
                {

                    listBoxPhoenixKingdom.Items.Remove(activeShip);
                    listBoxFalcorianEmpire.Items.Add(DetailsFalcorian);
                }
                //If the ship's alliance did not change
                else
                {
                    //The old data is removed
                    listBoxFalcorianEmpire.Items.Remove(activeShip);
                    //And new data is stored
                    listBoxFalcorianEmpire.Items.Add(DetailsFalcorian);
                }
            }
            else
            {
                //If the ship's name does not match that of a listBox loaded ship, it is populated as a new ship in a list box
                listBoxFalcorianEmpire.Items.Add(DetailsFalcorian);
            }
            //Memory for ship properties is reset after the save
            activeShip = noActiveShip;
            DetailsFalcorian = new Starship();

        }
        //This does the same as the above but for a different list box
        private void buttonAddToPhoenixKingdom_Click(object sender, EventArgs e)
        {
            

            if (activeShip.Name == DetailsPhoenix.Name)
            {

                if (activeShip.Alliance == "Unflagged")
                {
                    listBoxUnflagged.Items.Remove(activeShip);
                    listBoxPhoenixKingdom.Items.Add(DetailsPhoenix);
                }
                else if (activeShip.Alliance == "Falcorian Empire")
                {

                    listBoxFalcorianEmpire.Items.Remove(activeShip);
                    listBoxPhoenixKingdom.Items.Add(DetailsPhoenix);
                }
                else
                {
                    listBoxPhoenixKingdom.Items.Remove(activeShip);
                    listBoxPhoenixKingdom.Items.Add(DetailsPhoenix);
                }
            }
            else
            {
                listBoxPhoenixKingdom.Items.Add(DetailsPhoenix);
            }

            activeShip = noActiveShip;
            DetailsPhoenix = new Starship();


        }
        //This does the same as teh above but for a different list box
        private void buttonAddUnflaggedVessel_Click(object sender, EventArgs e)
        {
            
            if (activeShip.Name == DetailsUnflagged.Name)
            {

                if (activeShip.Alliance == "Phoenix Kingdom")
                {
                    listBoxPhoenixKingdom.Items.Remove(activeShip);
                    listBoxUnflagged.Items.Add(DetailsUnflagged);
                }
                else if (activeShip.Alliance == "Falcorian Empire")
                {

                    listBoxFalcorianEmpire.Items.Remove(activeShip);
                    listBoxUnflagged.Items.Add(DetailsUnflagged);
                }
                else
                {
                    listBoxUnflagged.Items.Remove(activeShip);
                    listBoxUnflagged.Items.Add(DetailsUnflagged);
                }
            }
            else
            {
                listBoxUnflagged.Items.Add(DetailsUnflagged);
            }

            activeShip = noActiveShip;
            DetailsUnflagged = new Starship();
        }

        //Triggers when the user clicks in any list box
        private void listBoxFleetSelectedIndexChanged(object sender, EventArgs e)
        {
            //The below statments check which listbox is active and check that the selected item is within the valid range for that listBox
            
            
            if (sender == listBoxFalcorianEmpire && listBoxFalcorianEmpire.SelectedIndex >= 0 && listBoxFalcorianEmpire.SelectedIndex < listBoxFalcorianEmpire.Items.Count)
            {
                //Then they store the ship data for display and comparison purposes
                Starship s = listBoxFalcorianEmpire.SelectedItem as Starship;
                activeShip = s;
                DetailsFalcorian = s;

                //The commands below also clear out selections in non selected boxes to prevent any confusion on the user
                //or programs part when determining "selected"
                listBoxPhoenixKingdom.ClearSelected();
                listBoxUnflagged.ClearSelected();
            }
            //See above 
            if (sender == listBoxPhoenixKingdom && listBoxPhoenixKingdom.SelectedIndex >= 0 && listBoxPhoenixKingdom.SelectedIndex < listBoxPhoenixKingdom.Items.Count)
            {
                Starship s = listBoxPhoenixKingdom.SelectedItem as Starship;
                activeShip = s;
                DetailsPhoenix = s;
                listBoxFalcorianEmpire.ClearSelected();
                listBoxUnflagged.ClearSelected();
            }
            //See above
            if (sender == listBoxUnflagged && listBoxUnflagged.SelectedIndex >= 0 && listBoxUnflagged.SelectedIndex < listBoxUnflagged.Items.Count)
            {
                Starship s = listBoxUnflagged.SelectedItem as Starship;
                activeShip = s;
                DetailsUnflagged = s;
                listBoxFalcorianEmpire.ClearSelected();
                listBoxPhoenixKingdom.ClearSelected();
            }
        }
        //Causes a modal window based off the Display form to appear.
        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Display form = new Display();
            //Sends the approprate values from form1 to the Display form
            form.FalcorianCount = listBoxFalcorianEmpire.Items.Count.ToString();
            form.PhoenixCount = listBoxPhoenixKingdom.Items.Count.ToString();
            form.UnflaggedCount = listBoxUnflagged.Items.Count.ToString();
            //Makes the form popup as modal
            form.ShowDialog();
            

        }
        //This Method is for deleting a starship object
        private void buttonDeleteVessel_Click(object sender, EventArgs e)
        {
            //the statements below check to see which listbox the object came from and then deletes the object
            if (activeShip.Alliance == "Phoenix Kingdom")
            {
                listBoxPhoenixKingdom.Items.Remove(activeShip);
            }
            else if (activeShip.Alliance == "Falcorian Empire")
            {
                listBoxFalcorianEmpire.Items.Remove(activeShip);
            }
            else
            {
                listBoxUnflagged.Items.Remove(activeShip);
            }
            //I have intentionally NOT cleared the form after a delete in the event the user does so on accident or is adjusting the parameters of a starship
        }
    }
}

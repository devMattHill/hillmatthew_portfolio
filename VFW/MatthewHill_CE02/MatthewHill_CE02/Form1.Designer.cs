﻿namespace MatthewHill_CE01
{
    partial class FormStarship
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textStarshipName = new System.Windows.Forms.TextBox();
            this.labelStarshipName = new System.Windows.Forms.Label();
            this.labelStarshipClass = new System.Windows.Forms.Label();
            this.checkBoxFtlCapable = new System.Windows.Forms.CheckBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonClearForm = new System.Windows.Forms.Button();
            this.comboBoxShipClass = new System.Windows.Forms.ComboBox();
            this.openFileDialogLoadStarship = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogStarship = new System.Windows.Forms.SaveFileDialog();
            this.groupBoxInput = new System.Windows.Forms.GroupBox();
            this.buttonDeleteVessel = new System.Windows.Forms.Button();
            this.buttonAddUnflaggedVessel = new System.Windows.Forms.Button();
            this.buttonAddToPhoenixKingdom = new System.Windows.Forms.Button();
            this.textBoxAlliance = new System.Windows.Forms.TextBox();
            this.buttonAddToFalcorianEmpire = new System.Windows.Forms.Button();
            this.labelFleetAlliance = new System.Windows.Forms.Label();
            this.labelSubLightSpeed = new System.Windows.Forms.Label();
            this.numericUpDownSubLightSpeed = new System.Windows.Forms.NumericUpDown();
            this.listBoxFalcorianEmpire = new System.Windows.Forms.ListBox();
            this.listBoxPhoenixKingdom = new System.Windows.Forms.ListBox();
            this.listBoxUnflagged = new System.Windows.Forms.ListBox();
            this.labelFalcorianEmpire = new System.Windows.Forms.Label();
            this.labelPhoenixKingdom = new System.Windows.Forms.Label();
            this.labelUnflagged = new System.Windows.Forms.Label();
            this.groupBoxFleetInventory = new System.Windows.Forms.GroupBox();
            this.menuStrip.SuspendLayout();
            this.groupBoxInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).BeginInit();
            this.groupBoxFleetInventory.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.statsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1340, 40);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // statsToolStripMenuItem
            // 
            this.statsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem});
            this.statsToolStripMenuItem.Name = "statsToolStripMenuItem";
            this.statsToolStripMenuItem.Size = new System.Drawing.Size(77, 38);
            this.statsToolStripMenuItem.Text = "Stats";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // textStarshipName
            // 
            this.textStarshipName.Location = new System.Drawing.Point(183, 39);
            this.textStarshipName.Name = "textStarshipName";
            this.textStarshipName.Size = new System.Drawing.Size(320, 31);
            this.textStarshipName.TabIndex = 1;
            // 
            // labelStarshipName
            // 
            this.labelStarshipName.AutoSize = true;
            this.labelStarshipName.Location = new System.Drawing.Point(24, 42);
            this.labelStarshipName.Name = "labelStarshipName";
            this.labelStarshipName.Size = new System.Drawing.Size(153, 25);
            this.labelStarshipName.TabIndex = 2;
            this.labelStarshipName.Text = "Starship Name";
            // 
            // labelStarshipClass
            // 
            this.labelStarshipClass.AutoSize = true;
            this.labelStarshipClass.Location = new System.Drawing.Point(26, 80);
            this.labelStarshipClass.Name = "labelStarshipClass";
            this.labelStarshipClass.Size = new System.Drawing.Size(151, 25);
            this.labelStarshipClass.TabIndex = 3;
            this.labelStarshipClass.Text = "Starship Class";
            // 
            // checkBoxFtlCapable
            // 
            this.checkBoxFtlCapable.AutoSize = true;
            this.checkBoxFtlCapable.Location = new System.Drawing.Point(29, 197);
            this.checkBoxFtlCapable.Name = "checkBoxFtlCapable";
            this.checkBoxFtlCapable.Size = new System.Drawing.Size(168, 29);
            this.checkBoxFtlCapable.TabIndex = 5;
            this.checkBoxFtlCapable.Text = "FTL Capable";
            this.checkBoxFtlCapable.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(610, 658);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(101, 43);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoad.Location = new System.Drawing.Point(717, 658);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(104, 43);
            this.buttonLoad.TabIndex = 7;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonClearForm
            // 
            this.buttonClearForm.Location = new System.Drawing.Point(29, 250);
            this.buttonClearForm.Name = "buttonClearForm";
            this.buttonClearForm.Size = new System.Drawing.Size(158, 43);
            this.buttonClearForm.TabIndex = 8;
            this.buttonClearForm.Text = "Clear Form";
            this.buttonClearForm.UseVisualStyleBackColor = true;
            this.buttonClearForm.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // comboBoxShipClass
            // 
            this.comboBoxShipClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShipClass.FormattingEnabled = true;
            this.comboBoxShipClass.Items.AddRange(new object[] {
            "Carrier",
            "Freighter",
            "Cruiser",
            "Frigate",
            "Bomber",
            "Fighter"});
            this.comboBoxShipClass.Location = new System.Drawing.Point(183, 77);
            this.comboBoxShipClass.Name = "comboBoxShipClass";
            this.comboBoxShipClass.Size = new System.Drawing.Size(320, 33);
            this.comboBoxShipClass.TabIndex = 9;
            // 
            // openFileDialogLoadStarship
            // 
            this.openFileDialogLoadStarship.FileName = "openFileDialogStarship";
            this.openFileDialogLoadStarship.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialogLoadStarship.InitialDirectory = "..\\..\\SavedShips";
            this.openFileDialogLoadStarship.Title = "Select a starship file";
            // 
            // saveFileDialogStarship
            // 
            this.saveFileDialogStarship.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            // 
            // groupBoxInput
            // 
            this.groupBoxInput.AutoSize = true;
            this.groupBoxInput.Controls.Add(this.buttonDeleteVessel);
            this.groupBoxInput.Controls.Add(this.buttonAddUnflaggedVessel);
            this.groupBoxInput.Controls.Add(this.buttonAddToPhoenixKingdom);
            this.groupBoxInput.Controls.Add(this.textBoxAlliance);
            this.groupBoxInput.Controls.Add(this.buttonAddToFalcorianEmpire);
            this.groupBoxInput.Controls.Add(this.labelFleetAlliance);
            this.groupBoxInput.Controls.Add(this.labelSubLightSpeed);
            this.groupBoxInput.Controls.Add(this.numericUpDownSubLightSpeed);
            this.groupBoxInput.Controls.Add(this.comboBoxShipClass);
            this.groupBoxInput.Controls.Add(this.checkBoxFtlCapable);
            this.groupBoxInput.Controls.Add(this.buttonClearForm);
            this.groupBoxInput.Controls.Add(this.labelStarshipClass);
            this.groupBoxInput.Controls.Add(this.labelStarshipName);
            this.groupBoxInput.Controls.Add(this.textStarshipName);
            this.groupBoxInput.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBoxInput.Location = new System.Drawing.Point(0, 40);
            this.groupBoxInput.Name = "groupBoxInput";
            this.groupBoxInput.Size = new System.Drawing.Size(509, 713);
            this.groupBoxInput.TabIndex = 10;
            this.groupBoxInput.TabStop = false;
            this.groupBoxInput.Text = "Starship Details";
            // 
            // buttonDeleteVessel
            // 
            this.buttonDeleteVessel.Location = new System.Drawing.Point(29, 315);
            this.buttonDeleteVessel.Name = "buttonDeleteVessel";
            this.buttonDeleteVessel.Size = new System.Drawing.Size(158, 43);
            this.buttonDeleteVessel.TabIndex = 18;
            this.buttonDeleteVessel.Text = "Delete Vessel";
            this.buttonDeleteVessel.UseVisualStyleBackColor = true;
            this.buttonDeleteVessel.Click += new System.EventHandler(this.buttonDeleteVessel_Click);
            // 
            // buttonAddUnflaggedVessel
            // 
            this.buttonAddUnflaggedVessel.Location = new System.Drawing.Point(193, 383);
            this.buttonAddUnflaggedVessel.Name = "buttonAddUnflaggedVessel";
            this.buttonAddUnflaggedVessel.Size = new System.Drawing.Size(310, 43);
            this.buttonAddUnflaggedVessel.TabIndex = 17;
            this.buttonAddUnflaggedVessel.Text = "Add Unflagged Vessel";
            this.buttonAddUnflaggedVessel.UseVisualStyleBackColor = true;
            this.buttonAddUnflaggedVessel.Click += new System.EventHandler(this.buttonAddUnflaggedVessel_Click);
            // 
            // buttonAddToPhoenixKingdom
            // 
            this.buttonAddToPhoenixKingdom.Location = new System.Drawing.Point(193, 315);
            this.buttonAddToPhoenixKingdom.Name = "buttonAddToPhoenixKingdom";
            this.buttonAddToPhoenixKingdom.Size = new System.Drawing.Size(310, 43);
            this.buttonAddToPhoenixKingdom.TabIndex = 16;
            this.buttonAddToPhoenixKingdom.Text = "Add To Phoenix Kingdom";
            this.buttonAddToPhoenixKingdom.UseVisualStyleBackColor = true;
            this.buttonAddToPhoenixKingdom.Click += new System.EventHandler(this.buttonAddToPhoenixKingdom_Click);
            // 
            // textBoxAlliance
            // 
            this.textBoxAlliance.Location = new System.Drawing.Point(193, 161);
            this.textBoxAlliance.Name = "textBoxAlliance";
            this.textBoxAlliance.ReadOnly = true;
            this.textBoxAlliance.Size = new System.Drawing.Size(310, 31);
            this.textBoxAlliance.TabIndex = 15;
            // 
            // buttonAddToFalcorianEmpire
            // 
            this.buttonAddToFalcorianEmpire.Location = new System.Drawing.Point(193, 250);
            this.buttonAddToFalcorianEmpire.Name = "buttonAddToFalcorianEmpire";
            this.buttonAddToFalcorianEmpire.Size = new System.Drawing.Size(310, 43);
            this.buttonAddToFalcorianEmpire.TabIndex = 14;
            this.buttonAddToFalcorianEmpire.Text = "Add To Falcorian Empire";
            this.buttonAddToFalcorianEmpire.UseVisualStyleBackColor = true;
            this.buttonAddToFalcorianEmpire.Click += new System.EventHandler(this.buttonAddToFalcorianEmpire_Click);
            // 
            // labelFleetAlliance
            // 
            this.labelFleetAlliance.AutoSize = true;
            this.labelFleetAlliance.Location = new System.Drawing.Point(24, 161);
            this.labelFleetAlliance.Name = "labelFleetAlliance";
            this.labelFleetAlliance.Size = new System.Drawing.Size(142, 25);
            this.labelFleetAlliance.TabIndex = 12;
            this.labelFleetAlliance.Text = "Fleet Alliance";
            // 
            // labelSubLightSpeed
            // 
            this.labelSubLightSpeed.AutoSize = true;
            this.labelSubLightSpeed.Location = new System.Drawing.Point(24, 122);
            this.labelSubLightSpeed.Name = "labelSubLightSpeed";
            this.labelSubLightSpeed.Size = new System.Drawing.Size(310, 25);
            this.labelSubLightSpeed.TabIndex = 11;
            this.labelSubLightSpeed.Text = "Sublight speed in AUs per hour";
            // 
            // numericUpDownSubLightSpeed
            // 
            this.numericUpDownSubLightSpeed.DecimalPlaces = 1;
            this.numericUpDownSubLightSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Location = new System.Drawing.Point(383, 120);
            this.numericUpDownSubLightSpeed.Maximum = new decimal(new int[] {
            74,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSubLightSpeed.Name = "numericUpDownSubLightSpeed";
            this.numericUpDownSubLightSpeed.Size = new System.Drawing.Size(120, 31);
            this.numericUpDownSubLightSpeed.TabIndex = 10;
            this.numericUpDownSubLightSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // listBoxFalcorianEmpire
            // 
            this.listBoxFalcorianEmpire.FormattingEnabled = true;
            this.listBoxFalcorianEmpire.ItemHeight = 25;
            this.listBoxFalcorianEmpire.Location = new System.Drawing.Point(218, 36);
            this.listBoxFalcorianEmpire.Name = "listBoxFalcorianEmpire";
            this.listBoxFalcorianEmpire.Size = new System.Drawing.Size(524, 179);
            this.listBoxFalcorianEmpire.TabIndex = 15;
            this.listBoxFalcorianEmpire.SelectedIndexChanged += new System.EventHandler(this.listBoxFleetSelectedIndexChanged);
            // 
            // listBoxPhoenixKingdom
            // 
            this.listBoxPhoenixKingdom.FormattingEnabled = true;
            this.listBoxPhoenixKingdom.ItemHeight = 25;
            this.listBoxPhoenixKingdom.Location = new System.Drawing.Point(218, 238);
            this.listBoxPhoenixKingdom.Name = "listBoxPhoenixKingdom";
            this.listBoxPhoenixKingdom.Size = new System.Drawing.Size(524, 179);
            this.listBoxPhoenixKingdom.TabIndex = 16;
            this.listBoxPhoenixKingdom.SelectedIndexChanged += new System.EventHandler(this.listBoxFleetSelectedIndexChanged);
            // 
            // listBoxUnflagged
            // 
            this.listBoxUnflagged.FormattingEnabled = true;
            this.listBoxUnflagged.ItemHeight = 25;
            this.listBoxUnflagged.Location = new System.Drawing.Point(218, 446);
            this.listBoxUnflagged.Name = "listBoxUnflagged";
            this.listBoxUnflagged.Size = new System.Drawing.Size(524, 179);
            this.listBoxUnflagged.TabIndex = 17;
            this.listBoxUnflagged.SelectedIndexChanged += new System.EventHandler(this.listBoxFleetSelectedIndexChanged);
            // 
            // labelFalcorianEmpire
            // 
            this.labelFalcorianEmpire.AutoSize = true;
            this.labelFalcorianEmpire.Location = new System.Drawing.Point(24, 39);
            this.labelFalcorianEmpire.Name = "labelFalcorianEmpire";
            this.labelFalcorianEmpire.Size = new System.Drawing.Size(174, 25);
            this.labelFalcorianEmpire.TabIndex = 18;
            this.labelFalcorianEmpire.Text = "Falcorian Empire";
            // 
            // labelPhoenixKingdom
            // 
            this.labelPhoenixKingdom.AutoSize = true;
            this.labelPhoenixKingdom.Location = new System.Drawing.Point(24, 238);
            this.labelPhoenixKingdom.Name = "labelPhoenixKingdom";
            this.labelPhoenixKingdom.Size = new System.Drawing.Size(180, 25);
            this.labelPhoenixKingdom.TabIndex = 19;
            this.labelPhoenixKingdom.Text = "Phoenix Kingdom";
            // 
            // labelUnflagged
            // 
            this.labelUnflagged.AutoSize = true;
            this.labelUnflagged.Location = new System.Drawing.Point(24, 446);
            this.labelUnflagged.Name = "labelUnflagged";
            this.labelUnflagged.Size = new System.Drawing.Size(110, 25);
            this.labelUnflagged.TabIndex = 20;
            this.labelUnflagged.Text = "Unflagged";
            // 
            // groupBoxFleetInventory
            // 
            this.groupBoxFleetInventory.Controls.Add(this.labelUnflagged);
            this.groupBoxFleetInventory.Controls.Add(this.listBoxUnflagged);
            this.groupBoxFleetInventory.Controls.Add(this.labelPhoenixKingdom);
            this.groupBoxFleetInventory.Controls.Add(this.listBoxFalcorianEmpire);
            this.groupBoxFleetInventory.Controls.Add(this.labelFalcorianEmpire);
            this.groupBoxFleetInventory.Controls.Add(this.buttonLoad);
            this.groupBoxFleetInventory.Controls.Add(this.listBoxPhoenixKingdom);
            this.groupBoxFleetInventory.Controls.Add(this.buttonSave);
            this.groupBoxFleetInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFleetInventory.Location = new System.Drawing.Point(509, 40);
            this.groupBoxFleetInventory.Name = "groupBoxFleetInventory";
            this.groupBoxFleetInventory.Size = new System.Drawing.Size(831, 713);
            this.groupBoxFleetInventory.TabIndex = 11;
            this.groupBoxFleetInventory.TabStop = false;
            this.groupBoxFleetInventory.Text = "Fleet Inventory";
            // 
            // FormStarship
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 753);
            this.Controls.Add(this.groupBoxFleetInventory);
            this.Controls.Add(this.groupBoxInput);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FormStarship";
            this.Text = "Starship Viewer";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBoxInput.ResumeLayout(false);
            this.groupBoxInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSubLightSpeed)).EndInit();
            this.groupBoxFleetInventory.ResumeLayout(false);
            this.groupBoxFleetInventory.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox textStarshipName;
        private System.Windows.Forms.Label labelStarshipName;
        private System.Windows.Forms.Label labelStarshipClass;
        private System.Windows.Forms.CheckBox checkBoxFtlCapable;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonClearForm;
        private System.Windows.Forms.ComboBox comboBoxShipClass;
        private System.Windows.Forms.OpenFileDialog openFileDialogLoadStarship;
        private System.Windows.Forms.SaveFileDialog saveFileDialogStarship;
        private System.Windows.Forms.GroupBox groupBoxInput;
        private System.Windows.Forms.Label labelUnflagged;
        private System.Windows.Forms.Label labelPhoenixKingdom;
        private System.Windows.Forms.Label labelFalcorianEmpire;
        private System.Windows.Forms.ListBox listBoxUnflagged;
        private System.Windows.Forms.ListBox listBoxPhoenixKingdom;
        private System.Windows.Forms.ListBox listBoxFalcorianEmpire;
        private System.Windows.Forms.Button buttonAddToFalcorianEmpire;
        private System.Windows.Forms.Label labelFleetAlliance;
        private System.Windows.Forms.Label labelSubLightSpeed;
        private System.Windows.Forms.NumericUpDown numericUpDownSubLightSpeed;
        private System.Windows.Forms.GroupBox groupBoxFleetInventory;
        private System.Windows.Forms.TextBox textBoxAlliance;
        private System.Windows.Forms.Button buttonAddUnflaggedVessel;
        private System.Windows.Forms.Button buttonAddToPhoenixKingdom;
        private System.Windows.Forms.ToolStripMenuItem statsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.Button buttonDeleteVessel;
    }
}


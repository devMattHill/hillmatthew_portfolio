﻿namespace MatthewHill_CE01
{
    partial class Display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxFleetSize = new System.Windows.Forms.GroupBox();
            this.textBoxUnflaggedCount = new System.Windows.Forms.TextBox();
            this.textBoxPhoenixCount = new System.Windows.Forms.TextBox();
            this.textBoxFalcorianCount = new System.Windows.Forms.TextBox();
            this.labelPhoenixFleetSize = new System.Windows.Forms.Label();
            this.labelUnflaggedFleetSize = new System.Windows.Forms.Label();
            this.labelFalcorianFleetSize = new System.Windows.Forms.Label();
            this.groupBoxFleetSize.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFleetSize
            // 
            this.groupBoxFleetSize.Controls.Add(this.textBoxUnflaggedCount);
            this.groupBoxFleetSize.Controls.Add(this.textBoxPhoenixCount);
            this.groupBoxFleetSize.Controls.Add(this.textBoxFalcorianCount);
            this.groupBoxFleetSize.Controls.Add(this.labelPhoenixFleetSize);
            this.groupBoxFleetSize.Controls.Add(this.labelUnflaggedFleetSize);
            this.groupBoxFleetSize.Controls.Add(this.labelFalcorianFleetSize);
            this.groupBoxFleetSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFleetSize.Location = new System.Drawing.Point(0, 0);
            this.groupBoxFleetSize.Name = "groupBoxFleetSize";
            this.groupBoxFleetSize.Size = new System.Drawing.Size(772, 323);
            this.groupBoxFleetSize.TabIndex = 0;
            this.groupBoxFleetSize.TabStop = false;
            this.groupBoxFleetSize.Text = "Fleet Sizes";
            // 
            // textBoxUnflaggedCount
            // 
            this.textBoxUnflaggedCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxUnflaggedCount.Location = new System.Drawing.Point(290, 219);
            this.textBoxUnflaggedCount.Name = "textBoxUnflaggedCount";
            this.textBoxUnflaggedCount.ReadOnly = true;
            this.textBoxUnflaggedCount.Size = new System.Drawing.Size(187, 24);
            this.textBoxUnflaggedCount.TabIndex = 5;
            // 
            // textBoxPhoenixCount
            // 
            this.textBoxPhoenixCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPhoenixCount.Location = new System.Drawing.Point(494, 103);
            this.textBoxPhoenixCount.Name = "textBoxPhoenixCount";
            this.textBoxPhoenixCount.ReadOnly = true;
            this.textBoxPhoenixCount.Size = new System.Drawing.Size(169, 24);
            this.textBoxPhoenixCount.TabIndex = 4;
            // 
            // textBoxFalcorianCount
            // 
            this.textBoxFalcorianCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFalcorianCount.Location = new System.Drawing.Point(104, 103);
            this.textBoxFalcorianCount.Name = "textBoxFalcorianCount";
            this.textBoxFalcorianCount.ReadOnly = true;
            this.textBoxFalcorianCount.Size = new System.Drawing.Size(169, 24);
            this.textBoxFalcorianCount.TabIndex = 3;
            // 
            // labelPhoenixFleetSize
            // 
            this.labelPhoenixFleetSize.AutoSize = true;
            this.labelPhoenixFleetSize.Location = new System.Drawing.Point(489, 74);
            this.labelPhoenixFleetSize.Name = "labelPhoenixFleetSize";
            this.labelPhoenixFleetSize.Size = new System.Drawing.Size(180, 25);
            this.labelPhoenixFleetSize.TabIndex = 2;
            this.labelPhoenixFleetSize.Text = "Phoenix Kingdom";
            // 
            // labelUnflaggedFleetSize
            // 
            this.labelUnflaggedFleetSize.AutoSize = true;
            this.labelUnflaggedFleetSize.Location = new System.Drawing.Point(285, 191);
            this.labelUnflaggedFleetSize.Name = "labelUnflaggedFleetSize";
            this.labelUnflaggedFleetSize.Size = new System.Drawing.Size(192, 25);
            this.labelUnflaggedFleetSize.TabIndex = 1;
            this.labelUnflaggedFleetSize.Text = "Unflagged Vessels";
            // 
            // labelFalcorianFleetSize
            // 
            this.labelFalcorianFleetSize.AutoSize = true;
            this.labelFalcorianFleetSize.Location = new System.Drawing.Point(99, 74);
            this.labelFalcorianFleetSize.Name = "labelFalcorianFleetSize";
            this.labelFalcorianFleetSize.Size = new System.Drawing.Size(174, 25);
            this.labelFalcorianFleetSize.TabIndex = 0;
            this.labelFalcorianFleetSize.Text = "Falcorian Empire";
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 323);
            this.Controls.Add(this.groupBoxFleetSize);
            this.MinimizeBox = false;
            this.Name = "Display";
            this.Text = "Display";
            this.groupBoxFleetSize.ResumeLayout(false);
            this.groupBoxFleetSize.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFleetSize;
        private System.Windows.Forms.TextBox textBoxUnflaggedCount;
        private System.Windows.Forms.TextBox textBoxPhoenixCount;
        private System.Windows.Forms.TextBox textBoxFalcorianCount;
        private System.Windows.Forms.Label labelPhoenixFleetSize;
        private System.Windows.Forms.Label labelUnflaggedFleetSize;
        private System.Windows.Forms.Label labelFalcorianFleetSize;
    }
}
﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE08: XML and File I/O
*/

namespace MatthewHill_CE08
{
    partial class FormClassViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxClassInformation = new System.Windows.Forms.GroupBox();
            this.numericHours = new System.Windows.Forms.NumericUpDown();
            this.numericMonth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBxCourseCode = new System.Windows.Forms.TextBox();
            this.txtBxClassName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBxCourseDescription = new System.Windows.Forms.TextBox();
            this.groupBoxOnlineSources = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOnlineData = new System.Windows.Forms.Button();
            this.radBtnASD = new System.Windows.Forms.RadioButton();
            this.radBtnVFW = new System.Windows.Forms.RadioButton();
            this.groupBoxFileType = new System.Windows.Forms.GroupBox();
            this.labelFileType = new System.Windows.Forms.Label();
            this.radBtnJSON = new System.Windows.Forms.RadioButton();
            this.radBtnXML = new System.Windows.Forms.RadioButton();
            this.saveFileDialogJSON = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogJSON = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogXML = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogXML = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.groupBoxClassInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonth)).BeginInit();
            this.groupBoxOnlineSources.SuspendLayout();
            this.groupBoxFileType.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(602, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBoxClassInformation
            // 
            this.groupBoxClassInformation.Controls.Add(this.numericHours);
            this.groupBoxClassInformation.Controls.Add(this.numericMonth);
            this.groupBoxClassInformation.Controls.Add(this.label6);
            this.groupBoxClassInformation.Controls.Add(this.label5);
            this.groupBoxClassInformation.Controls.Add(this.txtBxCourseCode);
            this.groupBoxClassInformation.Controls.Add(this.txtBxClassName);
            this.groupBoxClassInformation.Controls.Add(this.label4);
            this.groupBoxClassInformation.Controls.Add(this.label3);
            this.groupBoxClassInformation.Controls.Add(this.label2);
            this.groupBoxClassInformation.Controls.Add(this.txtBxCourseDescription);
            this.groupBoxClassInformation.Location = new System.Drawing.Point(25, 60);
            this.groupBoxClassInformation.Name = "groupBoxClassInformation";
            this.groupBoxClassInformation.Size = new System.Drawing.Size(555, 580);
            this.groupBoxClassInformation.TabIndex = 2;
            this.groupBoxClassInformation.TabStop = false;
            this.groupBoxClassInformation.Text = "Class Information";
            // 
            // numericHours
            // 
            this.numericHours.Location = new System.Drawing.Point(314, 289);
            this.numericHours.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericHours.Name = "numericHours";
            this.numericHours.Size = new System.Drawing.Size(127, 31);
            this.numericHours.TabIndex = 9;
            // 
            // numericMonth
            // 
            this.numericMonth.Location = new System.Drawing.Point(21, 289);
            this.numericMonth.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            0});
            this.numericMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMonth.Name = "numericMonth";
            this.numericMonth.Size = new System.Drawing.Size(120, 31);
            this.numericMonth.TabIndex = 8;
            this.numericMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(309, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Credit Hours";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Month";
            // 
            // txtBxCourseCode
            // 
            this.txtBxCourseCode.Location = new System.Drawing.Point(21, 188);
            this.txtBxCourseCode.Name = "txtBxCourseCode";
            this.txtBxCourseCode.Size = new System.Drawing.Size(211, 31);
            this.txtBxCourseCode.TabIndex = 5;
            // 
            // txtBxClassName
            // 
            this.txtBxClassName.Location = new System.Drawing.Point(21, 97);
            this.txtBxClassName.Name = "txtBxClassName";
            this.txtBxClassName.Size = new System.Drawing.Size(509, 31);
            this.txtBxClassName.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Course Code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Class Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Course Description";
            // 
            // txtBxCourseDescription
            // 
            this.txtBxCourseDescription.Location = new System.Drawing.Point(21, 388);
            this.txtBxCourseDescription.Multiline = true;
            this.txtBxCourseDescription.Name = "txtBxCourseDescription";
            this.txtBxCourseDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBxCourseDescription.Size = new System.Drawing.Size(509, 167);
            this.txtBxCourseDescription.TabIndex = 0;
            // 
            // groupBoxOnlineSources
            // 
            this.groupBoxOnlineSources.Controls.Add(this.label1);
            this.groupBoxOnlineSources.Controls.Add(this.btnOnlineData);
            this.groupBoxOnlineSources.Controls.Add(this.radBtnASD);
            this.groupBoxOnlineSources.Controls.Add(this.radBtnVFW);
            this.groupBoxOnlineSources.Location = new System.Drawing.Point(328, 664);
            this.groupBoxOnlineSources.Name = "groupBoxOnlineSources";
            this.groupBoxOnlineSources.Size = new System.Drawing.Size(252, 311);
            this.groupBoxOnlineSources.TabIndex = 3;
            this.groupBoxOnlineSources.TabStop = false;
            this.groupBoxOnlineSources.Text = "Online Sources";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select a course.";
            // 
            // btnOnlineData
            // 
            this.btnOnlineData.Location = new System.Drawing.Point(17, 191);
            this.btnOnlineData.Name = "btnOnlineData";
            this.btnOnlineData.Size = new System.Drawing.Size(216, 89);
            this.btnOnlineData.TabIndex = 2;
            this.btnOnlineData.Text = "Check Online Information";
            this.btnOnlineData.UseVisualStyleBackColor = true;
            this.btnOnlineData.Click += new System.EventHandler(this.btnOnlineData_Click);
            // 
            // radBtnASD
            // 
            this.radBtnASD.AutoSize = true;
            this.radBtnASD.Location = new System.Drawing.Point(17, 92);
            this.radBtnASD.Name = "radBtnASD";
            this.radBtnASD.Size = new System.Drawing.Size(86, 29);
            this.radBtnASD.TabIndex = 1;
            this.radBtnASD.TabStop = true;
            this.radBtnASD.Text = "ASD";
            this.radBtnASD.UseVisualStyleBackColor = true;
            // 
            // radBtnVFW
            // 
            this.radBtnVFW.AutoSize = true;
            this.radBtnVFW.Location = new System.Drawing.Point(17, 148);
            this.radBtnVFW.Name = "radBtnVFW";
            this.radBtnVFW.Size = new System.Drawing.Size(90, 29);
            this.radBtnVFW.TabIndex = 0;
            this.radBtnVFW.TabStop = true;
            this.radBtnVFW.Text = "VFW";
            this.radBtnVFW.UseVisualStyleBackColor = true;
            // 
            // groupBoxFileType
            // 
            this.groupBoxFileType.Controls.Add(this.labelFileType);
            this.groupBoxFileType.Controls.Add(this.radBtnJSON);
            this.groupBoxFileType.Controls.Add(this.radBtnXML);
            this.groupBoxFileType.Location = new System.Drawing.Point(25, 664);
            this.groupBoxFileType.Name = "groupBoxFileType";
            this.groupBoxFileType.Size = new System.Drawing.Size(262, 311);
            this.groupBoxFileType.TabIndex = 4;
            this.groupBoxFileType.TabStop = false;
            this.groupBoxFileType.Text = "FileType";
            // 
            // labelFileType
            // 
            this.labelFileType.AutoSize = true;
            this.labelFileType.Location = new System.Drawing.Point(16, 48);
            this.labelFileType.Name = "labelFileType";
            this.labelFileType.Size = new System.Drawing.Size(191, 25);
            this.labelFileType.TabIndex = 6;
            this.labelFileType.Text = "Choose a file type.";
            // 
            // radBtnJSON
            // 
            this.radBtnJSON.AutoSize = true;
            this.radBtnJSON.Location = new System.Drawing.Point(21, 92);
            this.radBtnJSON.Name = "radBtnJSON";
            this.radBtnJSON.Size = new System.Drawing.Size(99, 29);
            this.radBtnJSON.TabIndex = 5;
            this.radBtnJSON.TabStop = true;
            this.radBtnJSON.Text = "JSON";
            this.radBtnJSON.UseVisualStyleBackColor = true;
            // 
            // radBtnXML
            // 
            this.radBtnXML.AutoSize = true;
            this.radBtnXML.Location = new System.Drawing.Point(21, 148);
            this.radBtnXML.Name = "radBtnXML";
            this.radBtnXML.Size = new System.Drawing.Size(87, 29);
            this.radBtnXML.TabIndex = 4;
            this.radBtnXML.TabStop = true;
            this.radBtnXML.Text = "XML";
            this.radBtnXML.UseVisualStyleBackColor = true;
            // 
            // saveFileDialogJSON
            // 
            this.saveFileDialogJSON.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            // 
            // openFileDialogJSON
            // 
            this.openFileDialogJSON.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            // 
            // openFileDialogXML
            // 
            this.openFileDialogXML.FileName = "openFileDialog1";
            this.openFileDialogXML.Filter = "XML Files (*.xml)|*.xml|All files (*.*)|*.*";
            // 
            // saveFileDialogXML
            // 
            this.saveFileDialogXML.Filter = "XML Files (*.xml)|*.xml|All files (*.*)|*.*";
            // 
            // FormClassViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 1073);
            this.Controls.Add(this.groupBoxFileType);
            this.Controls.Add(this.groupBoxOnlineSources);
            this.Controls.Add(this.groupBoxClassInformation);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormClassViewer";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxClassInformation.ResumeLayout(false);
            this.groupBoxClassInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonth)).EndInit();
            this.groupBoxOnlineSources.ResumeLayout(false);
            this.groupBoxOnlineSources.PerformLayout();
            this.groupBoxFileType.ResumeLayout(false);
            this.groupBoxFileType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxClassInformation;
        private System.Windows.Forms.NumericUpDown numericHours;
        private System.Windows.Forms.NumericUpDown numericMonth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBxCourseCode;
        private System.Windows.Forms.TextBox txtBxClassName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBxCourseDescription;
        private System.Windows.Forms.GroupBox groupBoxOnlineSources;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOnlineData;
        private System.Windows.Forms.RadioButton radBtnASD;
        private System.Windows.Forms.RadioButton radBtnVFW;
        private System.Windows.Forms.GroupBox groupBoxFileType;
        private System.Windows.Forms.Label labelFileType;
        private System.Windows.Forms.RadioButton radBtnJSON;
        private System.Windows.Forms.RadioButton radBtnXML;
        private System.Windows.Forms.SaveFileDialog saveFileDialogJSON;
        private System.Windows.Forms.OpenFileDialog openFileDialogJSON;
        private System.Windows.Forms.OpenFileDialog openFileDialogXML;
        private System.Windows.Forms.SaveFileDialog saveFileDialogXML;
    }
}


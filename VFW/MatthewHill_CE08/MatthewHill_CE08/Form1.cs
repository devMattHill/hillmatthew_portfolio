﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE08: XML and File I/O
*/

using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Xml;


namespace MatthewHill_CE08
{
    public partial class FormClassViewer : Form
    {
        //Instance variables for the form
        WebClient jsonConnection = new WebClient();


        //ASD string
        string apiASD = "http://mdv-vfw.com/asd.";
        //VFW string
        string apiVFW = "http://mdv-vfw.com/vfw.";

        //Path for saving files
        static string saveFolder = @"..\..\SavedCourses";

        //Initializes the primary form
        public FormClassViewer()
        {
            InitializeComponent();
            //Sets the form to default values.
            FormReset();
            radBtnASD.Checked = true;
            radBtnJSON.Checked = true;
        }

        //Property for modifying and extracting the form values
        public Course values
        {
            //Getters and Setters
            get
            {
                Course tmp = new Course();

                tmp.Name = txtBxClassName.Text;
                tmp.Code = txtBxCourseCode.Text;
                tmp.Month = numericMonth.Value;
                tmp.Credits = numericHours.Value;
                tmp.Description = txtBxCourseDescription.Text;

                return tmp;
            }


            set
            {
                txtBxClassName.Text = value.Name;
                txtBxCourseCode.Text = value.Code;
                numericMonth.Value = value.Month;
                numericHours.Value = value.Credits;
                txtBxCourseDescription.Text = value.Description;
            }
        }

        //Method for resetting the form
        private void FormReset()
        {
            values = new Course();
        }

        //Method for reading JSON data from an API
        private void ReadDataJSON()
        {   //Setup the correct api to access based on radio buttons
            string apiString = apiVFW+"json";
            if (radBtnASD.Checked == true)
            {
                apiString = apiASD+"json";
            }
            //Setup the variable to store the downloaded string in 
            string apiData = "";

            //Tries to complete the webClient connection
            try
            {
                apiData = jsonConnection.DownloadString(apiString);
                //if successful it clears the form and
                FormReset();
                //pushes the apiData to a method that fills the form
                PutJsonObjectIntoForm(apiData);

            }//Catches any errors
            catch (WebException error)
            {//Provides an error message
                MessageBox.Show("Error: \n" + error.Message);
            }





        }

        //method for taking the Download String and filling in the form
        private void PutJsonObjectIntoForm(string apiData)
        {
            //see the string
            //MessageBox.Show("The string: \n" + apiData);

            //Convert string into a json object
            JObject o = JObject.Parse(apiData);

            //Find specific data in the jObject 
            string name = o["class"]["course_name_clean"].ToString();
            string code = o["class"]["course_code_long"].ToString();

            //parse as decimals
            string monthStr = o["class"]["sequence"].ToString();
            decimal month;
            decimal.TryParse(monthStr, out month);

            //parse as decimals
            string creditsStr = o["class"]["credit"].ToString();
            decimal credits;
            decimal.TryParse(creditsStr, out credits);

            string description = o["class"]["course_description"].ToString();

            //and fill the form values
            values = new Course(name, code, month, credits, description);

        }

        //Triggers on button "CheckOnlineInformation"
        private void btnOnlineData_Click(object sender, EventArgs e)
        {
            //Check data type before picking apiMethod
            if (radBtnJSON.Checked)
            {
                ReadDataJSON();
            }
            else
            {
                ReadDataXML();
            }
            

        }

        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Closes the application
            Application.Exit();
        }

        //Triggers on File>New
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {   //Resets to defaults
            FormReset();
        }

        //Triggers on File>Save
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates the save directory if it does not already exist.
            Directory.CreateDirectory(saveFolder);
            //Check radio buttons to determine save type
            if (radBtnJSON.Checked)
            {
                SaveJson();
            }
            else
            {
                SaveXML();
            }

            

        }

        //Method for saving JSON data
        private void SaveJson()
        {
            //set default extension for txt
            saveFileDialogJSON.DefaultExt = "txt";
            //opens a save file dialog for storing the starship information
            if (saveFileDialogJSON.ShowDialog() == DialogResult.OK)
            {    //opens a streamwriter to save the ship file as a .txt
                using (StreamWriter outStream = new StreamWriter(saveFileDialogJSON.FileName))
                {
                    //cycles through each ship in each listBox and saves them with a WriteLine
                    //This should be neater but, the fleet names are static.
                    outStream.WriteLine(values.SaveJSONString());
                }
            }
        }

        //Method for saving XML Data
        private void SaveXML()
        {
            //set default extentions
            saveFileDialogXML.DefaultExt = "xml";
            //opens a save file dialog for storing the starship information
            if (saveFileDialogJSON.ShowDialog() == DialogResult.OK)
            {   //instantiates an XMLWriterSettings
                //Set conformance levels
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                //set indent to true so it looks like a nice xml when opened
                settings.Indent = true;

                //Instantiate the XmlWriter with Create and using
                using (XmlWriter writer = XmlWriter.Create(saveFileDialogXML.FileName, settings))
                {
                    //write the first element which will be our check element when loading
                    writer.WriteStartElement("CourseFile");

                    //new nodes of the course file node
                    writer.WriteElementString("course_name_clean", txtBxClassName.Text);
                    writer.WriteElementString("course_code_long", txtBxCourseCode.Text);
                    writer.WriteElementString("sequence", numericMonth.Value.ToString());
                    writer.WriteElementString("credit", numericHours.Value.ToString());
                    writer.WriteElementString("course_description", txtBxCourseDescription.Text);

                    //write end element for course file
                    writer.WriteEndElement();
                }

            }

        }

        //Triggers on File>Load
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creates the saved course directory if it does not exist
            //I can't get the openDialogBox to default to the directory though :( #continue
            Directory.CreateDirectory(saveFolder);

            //Determines the load method based on radio buttons
            if (radBtnJSON.Checked)
            {
                LoadJson();
            }
            else
            {
                LoadXml();
            }
            
        }

        //Loads data from a txt in json format
        private void LoadJson()
        {
            //Opens an openFialDialog for locating saved course files.
            if (openFileDialogJSON.ShowDialog() == DialogResult.OK)
            {
                //Clears out the current form
                FormReset();
                //opens a streamreader to read the selected file
                using (StreamReader inStream = new StreamReader(openFileDialogJSON.FileName))
                {
                    //Takes the information from the file and fills in the course properties
                    while (inStream.Peek() > -1)
                    {   //Read in the file information
                        string line = inStream.ReadLine();
                        //Convert string into a json object
                        JObject o = JObject.Parse(line);
                        
                        //Check for appropriate file type
                        if(o["filetype"].ToString() == "CourseFile")
                        {
                            //Find specific data in the jObject 
                            string name = o["data"]["name"].ToString();
                            string code = o["data"]["code"].ToString();

                            //parse as decimals
                            string monthStr = o["data"]["month"].ToString();
                            decimal month;
                            decimal.TryParse(monthStr, out month);

                            //parse as decimals
                            string creditsStr = o["data"]["credits"].ToString();
                            decimal credits;
                            decimal.TryParse(creditsStr, out credits);

                            string description = o["data"]["description"].ToString();

                            //and fill the form values
                            values = new Course(name, code, month, credits, description);
                        }
                        else
                        {

                            MessageBox.Show("Error: Invalid File Type.");
                        }
                    }
                }
            }
            else
            {
                //Loads a "failed to load" name if the user cancels the openFileDialog

                MessageBox.Show("Error: Failed To Load File.");
            }
        }

        //loads data from an xml file
        private void LoadXml()
        {
            if (openFileDialogXML.ShowDialog() == DialogResult.OK)
            {//Clears out the form
                FormReset();
                //Get settings for the reader
                XmlReaderSettings settings = new XmlReaderSettings();
                //Set conformance level
                settings.ConformanceLevel = ConformanceLevel.Document;
                //ignore any extra properties and white space
                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;

                //create the reader with using and create
                using (XmlReader reader = XmlReader.Create(openFileDialogXML.FileName, settings))
                {
                    //skip any metadata
                    reader.MoveToContent();

                    //check for file type
                    if(reader.Name != "CourseFile")
                    {//if the wrong type of file give a popup
                        MessageBox.Show("Error: \nInvalid file type.");
                        return;
                    }
                    else
                    {
                        //push the reader to the prebuilt method
                        PutXmlReaderDataIntoForm(reader);
                    }


                }


                   
            }
            else
            {
                //Loads a "failed to load" name if the user cancels the openFileDialog

                MessageBox.Show("Error: Failed To Load File.");
            }


        }

        //Method for extracting XML data from an API
        private void ReadDataXML()
        {
            string apiString = apiVFW + "xml";
            if (radBtnASD.Checked == true)
            {
                apiString = apiASD + "xml";
            }

            //Try to get the Xml Data
            try
            {
                XmlTextReader apiData = new XmlTextReader(apiString);
                PutXmlTextReaderDataIntoForm(apiData);
            }
            catch (Exception error)
            {//Provides an error message
                MessageBox.Show("Error: \n" + error.Message);
            }

            
        }

        //Method for putting XMLTextReader data into the form
        private void PutXmlTextReaderDataIntoForm(XmlTextReader apiData)
        {
            //Find specific data in the jObject 
            string name = "";
            string code = "";
            string monthStr = "";
            string creditsStr = "";
            string description = "";


            //loop through the XML data
            while (apiData.Read())
            {
                
                // look for the course name while name is an empty string
                if(apiData.Name == "course_name_clean" && name == "")
                {
                    //Save the value
                    name = apiData.ReadString();
                    
                }
                // look for the course code while code is an empty string
                if (apiData.Name == "course_code_long" && code == "")
                {
                    //Save the value
                    code = apiData.ReadString();
                    
                }
                // look for the course month while monthStr is an empty string
                if (apiData.Name == "sequence" && monthStr == "")
                {
                    //Save the value
                    monthStr = apiData.ReadString();
                    
                }
                // look for the course credit while creditStr is an empty string
                if (apiData.Name == "credit" && creditsStr == "")
                {
                    //Save the value
                    creditsStr = apiData.ReadString();
                    
                }
                // look for the course description while description is an empty string
                if (apiData.Name == "course_description" && description == "")
                {
                    //Save the value
                    description = apiData.ReadString();
                    
                }
            }

            //parse as decimals
            decimal month;
            decimal.TryParse(monthStr, out month);
            decimal credits;
            decimal.TryParse(creditsStr, out credits);

            //and fill the form values
            values = new Course(name, code, month, credits, description);
        }

        //Method for putting XMLReader data into the form
        private void PutXmlReaderDataIntoForm(XmlReader xmlReader)
        {
            //Find specific data in the jObject 
            string name = "";
            string code = "";
            string monthStr = "";
            string creditsStr = "";
            string description = "";


            //loop through the XML data
            while (xmlReader.Read())
            {

                // look for the course name while name is an empty string
                if (xmlReader.Name == "course_name_clean" && name == "")
                {
                    //Save the value
                    name = xmlReader.ReadString();

                }
                // look for the course code while code is an empty string
                if (xmlReader.Name == "course_code_long" && code == "")
                {
                    //Save the value
                    code = xmlReader.ReadString();

                }
                // look for the course month while monthStr is an empty string
                if (xmlReader.Name == "sequence" && monthStr == "")
                {
                    //Save the value
                    monthStr = xmlReader.ReadString();

                }
                // look for the course credit while creditStr is an empty string
                if (xmlReader.Name == "credit" && creditsStr == "")
                {
                    //Save the value
                    creditsStr = xmlReader.ReadString();

                }
                // look for the course description while description is an empty string
                if (xmlReader.Name == "course_description" && description == "")
                {
                    //Save the value
                    description = xmlReader.ReadString();

                }
            }

            //parse as decimals
            decimal month;
            decimal.TryParse(monthStr, out month);
            decimal credits;
            decimal.TryParse(creditsStr, out credits);

            //and fill the form values
            values = new Course(name, code, month, credits, description);
        }

    }
}

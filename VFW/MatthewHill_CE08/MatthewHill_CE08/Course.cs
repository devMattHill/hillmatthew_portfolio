﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE08: XML and File I/O
*/

namespace MatthewHill_CE08
{
    public class Course
    {
        //Properties
        string name;
        string code;
        decimal month;
        decimal credits;
        string description;

        //Constructors
        public Course()
        {
            name = "";
            code = "";
            month = 1;
            credits = 0;
            description = "";
        }

        public Course(string name, string code, decimal month, decimal credits, string description)
        {
            this.name = name;
            this.code = code;
            this.month = month;
            this.credits = credits;
            this.description = description;
        }

        //Getters and Setters
        public string Name { get => name; set => name = value; }
        public string Code { get => code; set => code = value; }
        public decimal Month { get => month; set => month = value; }
        public decimal Credits { get => credits; set => credits = value; }
        public string Description { get => description; set => description = value; }

        //To String Override
        public override string ToString()
        {
            string toString = name + " " + code;
            return toString;
        }

        //String for saving as a JSON object
        public string SaveJSONString()
        {
            string saveValue = "{";
            saveValue += $"\"filetype\":\"CourseFile\",\"data\":";
            saveValue += "{";
            saveValue += $"\"name\":\"{name}\",\"code\":\"{code}\",\"month\":\"{month.ToString()}\",\"credits\":\"{credits.ToString()}\",\"description\":\"{description}\"";
            saveValue += "} }";

            return saveValue;
        }




    }



}


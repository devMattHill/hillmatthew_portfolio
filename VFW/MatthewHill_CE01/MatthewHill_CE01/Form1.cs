﻿using System;
using System.Windows.Forms;
using System.IO;

namespace MatthewHill_CE01
{
    public partial class FormStarship : Form
    {
        //Path for saving files
        static string saveFolder = @"..\..\SavedShips";
        //Initializes the Form1 main form
        public FormStarship()
        {
            InitializeComponent();
        }
        
        
        //Makes the File>Exit in the ToolStripMenu exit the program
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //Triggered by the default button
        private void buttonDefault_Click(object sender, EventArgs e)
        {
            //resets the form to default values
            textStarshipName.Text = "";
            comboBoxShipClass.SelectedItem = "Carrier";
            checkBoxFtlCapable.Checked = false;
        }

        //This method is triggered by either the save button or the File>Save menu
        //It writes out a file with the ship name as the file name.
        private void buttonSave_Click(object sender, EventArgs e)
        {
            //Creates the directory if it does not already exist.
            Directory.CreateDirectory(saveFolder);
            //Instantiates a starship class and stores values from the form
            Starship starship = new Starship();
            starship.Name = textStarshipName.Text;
            starship.ShipClass = comboBoxShipClass.Text;
            starship.FTL = checkBoxFtlCapable.Checked;

            //opens a save file dialog for storing the starship information
            if (saveFileDialogStarship.ShowDialog() == DialogResult.OK)
            {
                //opens a streamwriter to save the ship file as a .txt
                using (StreamWriter outStream = new StreamWriter(saveFileDialogStarship.FileName))
                {
                    outStream.Write(starship.SaveString());
                }
            }
            

            //resets the form to default values
            textStarshipName.Text = "";
            comboBoxShipClass.SelectedItem = "Carrier";
            checkBoxFtlCapable.Checked = false;
        }
       
     
        //Triggered by the Load button or the File>Load menu item
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            //Creates the saved ships directory if it does not exist
            //I can't get the openDialogBox to default to the directory though :( #continue
            Directory.CreateDirectory(saveFolder);
            //Instantiates a blank starship
            Starship loadedStarShip;
            //Opens an openFialDialog for locating saved ship files.
            if (openFileDialogLoadStarship.ShowDialog() == DialogResult.OK)
            {
                //opens a streamread to read the selected file
                using (StreamReader inStream = new StreamReader(openFileDialogLoadStarship.FileName))
                {
                    //Takes the information from the file and fills in the starship properties
                    string line = inStream.ReadLine();
                    loadedStarShip = new Starship(line);
                }
            }
            else
            {
                //Loads a "failed to load" name if the user cancels the openFileDialog
                loadedStarShip = new Starship("Failed to Load|Carrier|False");
            }

            //Fills in the form with the loadedStarship's property values
            textStarshipName.Text = loadedStarShip.Name;
            comboBoxShipClass.SelectedItem = loadedStarShip.ShipClass;
            checkBoxFtlCapable.Checked = loadedStarShip.FTL;


        }
    }
}

﻿namespace MatthewHill_CE01
{
    partial class FormStarship
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textStarshipName = new System.Windows.Forms.TextBox();
            this.labelStarshipName = new System.Windows.Forms.Label();
            this.labelStarshipClass = new System.Windows.Forms.Label();
            this.checkBoxFtlCapable = new System.Windows.Forms.CheckBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.comboBoxShipClass = new System.Windows.Forms.ComboBox();
            this.openFileDialogLoadStarship = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogStarship = new System.Windows.Forms.SaveFileDialog();
            this.groupBoxInput = new System.Windows.Forms.GroupBox();
            this.menuStrip.SuspendLayout();
            this.groupBoxInput.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(616, 40);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // textStarshipName
            // 
            this.textStarshipName.Location = new System.Drawing.Point(183, 39);
            this.textStarshipName.Name = "textStarshipName";
            this.textStarshipName.Size = new System.Drawing.Size(320, 31);
            this.textStarshipName.TabIndex = 1;
            // 
            // labelStarshipName
            // 
            this.labelStarshipName.AutoSize = true;
            this.labelStarshipName.Location = new System.Drawing.Point(24, 42);
            this.labelStarshipName.Name = "labelStarshipName";
            this.labelStarshipName.Size = new System.Drawing.Size(153, 25);
            this.labelStarshipName.TabIndex = 2;
            this.labelStarshipName.Text = "Starship Name";
            // 
            // labelStarshipClass
            // 
            this.labelStarshipClass.AutoSize = true;
            this.labelStarshipClass.Location = new System.Drawing.Point(26, 80);
            this.labelStarshipClass.Name = "labelStarshipClass";
            this.labelStarshipClass.Size = new System.Drawing.Size(151, 25);
            this.labelStarshipClass.TabIndex = 3;
            this.labelStarshipClass.Text = "Starship Class";
            // 
            // checkBoxFtlCapable
            // 
            this.checkBoxFtlCapable.AutoSize = true;
            this.checkBoxFtlCapable.Location = new System.Drawing.Point(29, 116);
            this.checkBoxFtlCapable.Name = "checkBoxFtlCapable";
            this.checkBoxFtlCapable.Size = new System.Drawing.Size(168, 29);
            this.checkBoxFtlCapable.TabIndex = 5;
            this.checkBoxFtlCapable.Text = "FTL Capable";
            this.checkBoxFtlCapable.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(393, 238);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(101, 43);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(500, 238);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(104, 43);
            this.buttonLoad.TabIndex = 7;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonDefault
            // 
            this.buttonDefault.Location = new System.Drawing.Point(278, 238);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(109, 43);
            this.buttonDefault.TabIndex = 8;
            this.buttonDefault.Text = "Default";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // comboBoxShipClass
            // 
            this.comboBoxShipClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShipClass.FormattingEnabled = true;
            this.comboBoxShipClass.Items.AddRange(new object[] {
            "Carrier",
            "Freighter",
            "Cruiser",
            "Frigate",
            "Bomber",
            "Fighter"});
            this.comboBoxShipClass.Location = new System.Drawing.Point(183, 77);
            this.comboBoxShipClass.Name = "comboBoxShipClass";
            this.comboBoxShipClass.Size = new System.Drawing.Size(320, 33);
            this.comboBoxShipClass.TabIndex = 9;
            // 
            // openFileDialogLoadStarship
            // 
            this.openFileDialogLoadStarship.FileName = "openFileDialogStarship";
            this.openFileDialogLoadStarship.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialogLoadStarship.InitialDirectory = "..\\..\\SavedShips";
            this.openFileDialogLoadStarship.Title = "Select a starship file";
            // 
            // saveFileDialogStarship
            // 
            this.saveFileDialogStarship.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            // 
            // groupBoxInput
            // 
            this.groupBoxInput.Controls.Add(this.buttonLoad);
            this.groupBoxInput.Controls.Add(this.comboBoxShipClass);
            this.groupBoxInput.Controls.Add(this.buttonSave);
            this.groupBoxInput.Controls.Add(this.checkBoxFtlCapable);
            this.groupBoxInput.Controls.Add(this.buttonDefault);
            this.groupBoxInput.Controls.Add(this.labelStarshipClass);
            this.groupBoxInput.Controls.Add(this.labelStarshipName);
            this.groupBoxInput.Controls.Add(this.textStarshipName);
            this.groupBoxInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxInput.Location = new System.Drawing.Point(0, 40);
            this.groupBoxInput.Name = "groupBoxInput";
            this.groupBoxInput.Size = new System.Drawing.Size(616, 293);
            this.groupBoxInput.TabIndex = 10;
            this.groupBoxInput.TabStop = false;
            this.groupBoxInput.Text = "Starship Details";
            // 
            // FormStarship
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 333);
            this.Controls.Add(this.groupBoxInput);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FormStarship";
            this.Text = "Starship Viewer";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBoxInput.ResumeLayout(false);
            this.groupBoxInput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox textStarshipName;
        private System.Windows.Forms.Label labelStarshipName;
        private System.Windows.Forms.Label labelStarshipClass;
        private System.Windows.Forms.CheckBox checkBoxFtlCapable;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.ComboBox comboBoxShipClass;
        private System.Windows.Forms.OpenFileDialog openFileDialogLoadStarship;
        private System.Windows.Forms.SaveFileDialog saveFileDialogStarship;
        private System.Windows.Forms.GroupBox groupBoxInput;
    }
}


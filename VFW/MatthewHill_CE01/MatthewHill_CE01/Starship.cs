﻿using System;

namespace MatthewHill_CE01
{
    class Starship
    {
        //Properties of a starship
        string name;
        string shipClass;
        bool ftl;
        //Getters and setters
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ShipClass
        {
            get
            {
                return shipClass;
            }
            set
            {
                shipClass = value;
            }
        }

        public bool FTL
        {
            get
            {
                return ftl;
            }
            set
            {
                ftl = value;
            }
        }



        //Constructor for instantiating a starship from a files delimited string
        public Starship(string str)
        {
            string[] stats = str.Split('|');
            name = stats[0];
            shipClass = stats[1];
            bool loadFtl;
            if (Boolean.TryParse(stats[2], out loadFtl))
            {
                ftl = loadFtl;
            }

        }
        //Constructor for instantiated a blank starship
        public Starship()
        {
            name = "";
            shipClass = "";
            ftl = false;
        }


        //String to save the starship with a write & streamwriter.
        public string SaveString()
        {
            string saveValue = "";

            saveValue = name + "|" + shipClass + "|" + ftl;

            return saveValue;
        }


    }


}

﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE09: Databse Connectivity
*/


namespace MatthewHill_CE09
{
    public class Dvd
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }

        public Dvd() { }
        public Dvd(string title,string genre, int year, decimal price)
        {
            Title = title;
            Genre = genre;
            Year = year;
            Price = price;
        }


        public override string ToString()
        {
            string str = $"Title: {Title} \tPrice: {Price}";
            return str;
        }

        public string SaveString()
        {
            string saveString = Title + "|" + Genre + "|" + Year + "|" + Price;
            return saveString;
        }

    }
}

﻿/*
Matthew Hill
MDV1830-O 
Section 01
Term C201802
CE09: Databse Connectivity
*/

//name space for using mysql
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
//name space for saving files
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace MatthewHill_CE09
{
    public partial class Form1 : Form
    {
        //connection string variable
        MySqlConnection conn = new MySqlConnection();

        //Instantiate the DataTable object
        DataTable lore = new DataTable();

        // instance variable for the current record
        int currentRecord = 0;

        //instance variable for number of records
        int numberOfRecords = 0;

        //Instance variable for storing our list of DVDs
        List<Dvd> inventory = new List<Dvd>();


        //Initializes the form and then runs the three follow on required methods to get 
        //Data from the MySql Database
        public Form1()
        {
            InitializeComponent();

            string connectionString = BuildConnectionString();

            Connect(connectionString);

            RetrieveData();
        }

        //Method for querying the database after the connection is made
        private void RetrieveData()
        {
            //Hard coded sql query
            string sql = "SELECT dvd_title, genre, year, price FROM dvd LIMIT 25";

            //Create the DataAdapter
            MySqlDataAdapter adapter = new MySqlDataAdapter(sql, conn);

            //Set the type for the SELECT command to text
            adapter.SelectCommand.CommandType = CommandType.Text;
            
            //use the msqladapter to fill the datatable
            adapter.Fill(lore);

            //Use the datatable to fill a dataRowCollection
            DataRowCollection rows = lore.Rows;

            //storing all Dvd opjects into a list
            foreach(DataRow row in rows)
            {
                string title = row["DVD_Title"].ToString();
                string genre = row["Genre"].ToString();
                int year;
                int.TryParse(row["Year"].ToString(), out year);
                decimal price;
                decimal.TryParse(row["Price"].ToString(), out price);

                Dvd tmp = new Dvd(title, genre, year, price);
                inventory.Add(tmp);
            }
            
            //Get a count of the number of rows in the DataTable
            numberOfRecords = inventory.Count();
            
            //Put the first records data into the form
            UpdateForm(0);
            
        }

        //Method for connecting to the MySql Database
        private void Connect(string myConnectionString)
        {
            try
            {//Tries to open a connection
                conn.ConnectionString = myConnectionString;
                conn.Open();
                //MessageBox.Show("Connected!");
            }//Catches errors if it fails
            catch (MySqlException e)
            {
                // message string variable
                string msg = "";
                //check the exception
                switch (e.Number)
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve host address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username or password.";
                        break;
                    default:
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }//Shows any caught exceptions
                MessageBox.Show(msg);
            }
        }


        //Method for building the connection string for the MySql database
        private string BuildConnectionString()
        {
            string serverIP = "";
            //Tries to read the predetermined file for saving IP address
            try
            {
                //open C:\VFW\connect.txt file with a stream reader
                using (StreamReader reader = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    // read server IP
                    serverIP = reader.ReadToEnd();
                }
            }//Catches any errors in finding the file
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            //returns the connection string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";

        }

      
        //Method for updating the form
        private void UpdateForm(int record)
        {
            //Put current record data into the form
            txtDvdTitle.Text = inventory[record].Title;
            txtGenre.Text = inventory[record].Genre;
            numYear.Value = inventory[record].Year;
            numPrice.Value = inventory[record].Price;

            //build a string to update the status bar witht the current record
            string statusString = $"DVD {record +1} of {numberOfRecords}";
            //store the string to the status labl text propery
            statusCurrentDvd.Text = statusString;

            //Make the btnFirst and btnPrev disappear if we are on the first record
            if(record < 1)
            {
                btnFirst.Visible = false;
                btnPrev.Visible = false;
            }
            else//and reappear if the number is greater than zero
            {
                btnFirst.Visible = true;
                btnPrev.Visible = true;
            }

            //Make the btnLast and btnNext disappear if we are on the last record
            if(record == numberOfRecords - 1)
            {
                btnNext.Visible = false;
                btnLast.Visible = false;
            }
            else//and reappear if the number is less than the last record
            {
                btnNext.Visible = true;
                btnLast.Visible = true;
            }


        }
 
        //Button Events below

        //Triggers on btnNext
        private void btnNext_Click(object sender, EventArgs e)
        {//adds one to the curren record and updates
            currentRecord += 1;
            UpdateForm(currentRecord);
        }
        //Triggers on btnPrev
        private void btnPrev_Click(object sender, EventArgs e)
        {//subtracts one from the current record and updates
            currentRecord -= 1;
            UpdateForm(currentRecord);
        }

        //Triggers on btnFirst
        private void btnFirst_Click(object sender, EventArgs e)
        {//changes the currentRecord to index 0 and updates
            currentRecord = 0;
            UpdateForm(currentRecord);
        }

        //Triggers on btnLast
        private void btnLast_Click(object sender, EventArgs e)
        {//changes the currentRecord to the maximum index and updates
            currentRecord = numberOfRecords-1;
            UpdateForm(currentRecord);
        }

        //Triggers on File>Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {//exits the application
            Application.Exit();
        }

        //Triggers on File>Save
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Instantiates a string for the save data
            string saveData = "";
            //opens a save file dialog for storing the dvd information
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //opens a streamwriter to save the dvd file as a .txt
                using (StreamWriter outStream = new StreamWriter(saveFileDialog1.FileName))
                {
                    //cycles through each dvd in each the inventory 
                    foreach (Dvd dvd in inventory)
                    {//and saves them with a WriteLine
                        outStream.WriteLine(dvd.SaveString());
                    }

                }
            }


        }
    }
}

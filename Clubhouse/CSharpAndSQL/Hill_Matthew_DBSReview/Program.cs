﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MattsTools;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;


namespace Hill_Matthew_DBSReview
{
    class Program
    {
        MySqlConnection Con = null;


        static void Main(string[] args)
        {
            Program instance = new Program();

            instance.Con = new MySqlConnection();
            instance.Connect();
            //menu here

            //And finally the main part of the program
            //#CONT redo the menu to just three items, city name, clear screen and exit

            string menuHeader = "Welcome to Weather Not Com.";
            Dictionary<string, string> menuOption = new Dictionary<string, string>();
            menuOption.Add("Check the Weather", "");
            menuOption.Add("Clear Screen", "");
            menuOption.Add("Exit", "");

            string chosenOption = "";

            FancyMenu mainMenu = new FancyMenu(menuHeader, menuOption);

            //#CONT redo the menu to just three items, city name, clear screen and exit
            while (chosenOption != "exit")
            {
                chosenOption = mainMenu.Display();
                switch (chosenOption)
                {

                    case "check the weather":
                        {
                            string city = instance.GetCity();
                            DataTable data = instance.QueryDB($"SELECT city, createdDate, temp, pressure, humidity from weather where city = '{city}' order by createdDate desc limit 1");

                            DataRowCollection rows = data.Rows;
                            if (rows.Count > 0)
                            {
                                decimal temp = 0;
                                decimal.TryParse(rows[0]["temp"].ToString(), out temp);
                                decimal farenheitTemp = Math.Round(temp * 9 / 5 - 459.67m, 2);
                                string pressure = rows[0]["pressure"].ToString();
                                string humidity = rows[0]["humidity"].ToString();
                                Console.WriteLine();
                                Console.WriteLine($"The last recorded temperature in {city} was {farenheitTemp} degrees farenheit at {humidity}% humidity with air pressure of {pressure} mbar.");
                                Console.WriteLine();
                            }
                            else
                            {
                                Console.WriteLine();
                                Console.WriteLine($"No Data Available for {city}.");
                                Console.WriteLine();
                            }



                            break;
                        }
                    case "clear screen":
                        {
                            Console.Clear();
                            break;
                        }

                    case "exit":
                        {

                            break;
                        }

                    default:
                        break;
                }

            }

            //End of the main program
            Console.WriteLine("Thank you for checking the weather. Press any key to continue.");
            Console.ReadKey();


            //#CONT come back and check this select query, it's the one that will find the weather info


        }
        //#CONT needed connection method
        void Connect()
        {
            BuildConString();

            try
            {
                Con.Open();
                Console.WriteLine("Connection successful!");
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + Con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username or password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }
                Console.WriteLine(msg);
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();


            }

        }
        //Needed to Build a connection string for accessing the database
        void BuildConString()
        {

            //using this to pull the IP for the connection string
            string constringHeader = "Select the IP address option you wish to use.";
            List<string> constringOptions = new List<string>();
            constringOptions.Add(@"Use C:\VFW\connect.txt");
            constringOptions.Add("Enter a custom IP address. WARNING the wrong IP address will cause the system to stop.");
            string ip = "";
            NumberMenu constringMenu = new NumberMenu(constringHeader, constringOptions);
            int option = constringMenu.Display();

            if (option == 1)
            {
                using (StreamReader sr = new StreamReader(@"C:\VFW\connect.txt"))
                {
                    ip = sr.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Type in the IP address below");
                Validator validator = new Validator();
                ip = validator.GetString();
            }



            string conString = $"Server={ip};";
            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=SampleAPIData;";
            conString += "port=8889";

            Con.ConnectionString = conString;

        }

        //Needed for the query
        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, Con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }

        string GetCity()
        {
            Validator validator = new Validator();
            Console.WriteLine("Enter the name of the City in which you want to check the weather.");
            return validator.GetString();

        }
        
    }
}

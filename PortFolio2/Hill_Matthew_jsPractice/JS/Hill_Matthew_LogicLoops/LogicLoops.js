
function Main(){
    let header =  "Choose a tool.";
    let options = ["Tire Pressure","Movie Ticket Price","Add Up the Odds or Evens","Charge It","Exit"];
    let menu = new Menu(header, options);

    let choice= "";

    //The menu for the main function here, it will keep running until the user chooses to exit
    while(choice !== "exit"){
        choice = menu.display();
        switch(choice){
            case "tire pressure":
                TirePressure();
                break;
            case "movie ticket price":
                MovieTicketPrice();
                break;

            case "add up the odds or evens":
                AddOddsOrEvens();
                break;

            case "charge it":
                ChargeIt();
                break;

            case "exit":
                break;

            case null://This allows the user to press cancel on a prompt to leave the program
                choice = "exit";
                break;
            default:

                break;
        }
    }
}

function TirePressure(){
    let tires = [];//This is the array that will hold the tire pressure numbers
    let tireHeaders = ["front left","front right","back left","back right"];//This array identifies the tires
    let validator = new Validator();
    let pressure;//Pressure is instantiated out of the loop so it can be used as a test for the user canceling a prompt
    for (let i = 0; i < tireHeaders.length; i++){//This loop uses tire headers to fill in user values to tires array
        let header = "What is the pressure in your "+tireHeaders[i]+" tire?";//A dynamic header for changing tire names
        pressure = validator.getInt(header);
            if (pressure !== null){//Allows the user to cancel a prompt
                tires.push(pressure)//Adds the user input value to the tires array
            }else{
                i = 5;//Breaks the for loop early if the user cancels a prompt
            }

    }
    if (pressure === null){//Tests if the last user input was a canceled prompt
    }else{
        if((tires[0] === tires[1])&&(tires[2] === tires[3])){//Makes sure tire pares have the same values
            alert("Your tires pas spec!")
        }else{//If they do not your tires need checked
            alert("Get your tires checked out!")
        }
    }
}

function MovieTicketPrice(){//Stores headers for prompts
    let timeHeader = "Enter the time of the movie.\n Use whole number military time.\nEx: 1am is 1 and 1pm is 13";
    let ageHeader = "Enter the age of the customer.\n Ages 0 - 150 are valid.";
    let age = -1;
    let time = -1;
    let validator = new Validator();

    while ((age < 0 || age > 150) && age !==null){//A loop that checks for valid ages 0-150
        age = validator.getInt(ageHeader);          //or for a null value from a canceled prompt
    }
    if (age !== null){//This check only continues the process if age is not null
        while ((time < 0 || time > 23) && time !==null){//A loop that checks for valid times 0-23
            time = validator.getInt(timeHeader);        //or for a null value from a canceled prompt
        }
        if(time !== null){//This check only continues the process if time is not null
            if (time >= 14 && time <= 17){//Checks times for possible discount
                alert("The ticket price is $7.00");
            }else if(age < 10 || age > 54){//Checks age for possible discount
                alert("The ticket price is $7.00");
            }else{//Gives un-discounted ticket price
                alert("The ticket price is $12.00");
            }
        }
    }
}

function AddOddsOrEvens(){
    let countables = [3,6,9,12,15,18];//Numbers to add up
    let header = countables + "\nWould you like to add up the even or odd numbers?";
    let options = ["Odd","Even"];// This is just another menu setup to determine with set of numbers to add
    let menu = new Menu(header, options);
    let choice= "";

    while(choice !== "exit"){//Loop to capture user choices
        choice = menu.display();
        switch(choice){
            case "odd":
                choice = "exit";//Set to exit to make sure the loop does not repeat after choosing a valid option
                Odd(countables);//Calls the Odd() function and passes the array of numbers
                break;
            case "even":
                choice = "exit";
                Even(countables);//Calls the Even() function and passes the array of numbers
                break;

            case null://This allows the user to press cancel on a prompt to leave the program
                choice = "exit";
                break;
            default:

                break;
        }
    }
}

function Odd(countables){
    let sum = 0;
    for(let item of countables){//Iterates through the given array and uses modulus to find odd numbers
        if (item%2 === 1 ){//Then totals them all in sum
            sum += item;
        }
    }//Then prints the sum
    alert("The total of odd numbers in the array is "+sum+".")//27

}

function Even(countables){
    let sum = 0;
    for(let item of countables){//Iterates through the given array and uses modulus to find even numbers
        if (item%2 === 0 ){//Then totals them all in sum
            sum += item;
        }
    }//Then prints the sum
    alert("The total of even numbers in the array is "+sum+".")//36

}

function ChargeIt(){
    let validator = new Validator();
    let limitHeader = "What is your credit limit?";
    let limit = validator.getFloat(limitHeader);//Uses a validator to get the credit limit from the user
    if (limit !== null){//Continues if the user did not cancel the prompt
        let purchase = 0;//Instantiated purchase outside of the loop to test for canceled prompts further down
        let purchaseTotal = 0;
        while (purchaseTotal < limit){//A loop that runs until the purchases exceed the credit limit
            let remaining = limit - purchaseTotal;//Convenience variable to prompt the user with their remaining limit
            let header = "Your remaining limit is $"+remaining.toFixed(2)+".\n What is the price of your next purchase?";
            purchase = validator.getFloat(header);//Validator used to get purchase price from user
            if(purchase !== null){//Continues if the user did not cancel the prompt
                purchaseTotal += purchase;
            }else{
                break;//Breaks the loop if the user did cancel a prompt
            }
        }
        if(purchase !== null){//Continues if the user did not cancel a prompt
            let excess = purchaseTotal - limit;//Final credit outcome is posted
            alert("With your last purchase you have reached your credit limit and exceeded it by $"+excess.toFixed(2)+".");
        }
    }

}

Main();


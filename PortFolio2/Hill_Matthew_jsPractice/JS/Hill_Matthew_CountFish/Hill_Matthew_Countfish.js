




function Main(){
    //Array for fish
    let aquarium = ["red","yellow","green","blue","red","red","yellow","yellow","yellow","green","green","light urple","green","blue","yellow","yellow","yellow"];
    //Builds the main menu using the Menus_Validators library.
    let header = aquarium + "\n\nChoose the color of the fish you would like to count.\n";
    let options = [];
    options.push("Red");
    options.push("Blue");
    options.push("Green");
    options.push("Yellow");
    options.push("Light Urple");
    options.push("Exit");
    let menu = new Menu(header, options);
    let choice= "";

    //The menu for the main function here, it will keep running until the user chooses a valid option
    while(choice !==null && choice !== "exit"){
        choice = menu.display();//Menu validates user inputs
        if(choice !==null && choice !== "exit"){//This assumes the user wants to continue the process
            let amount = 0;//This is the variable that counts the fish
            for(let fish of aquarium){//This loops iterates through the aquarium
                console.log(fish,choice);
                if(fish === choice){//and counts every fish that matches the users choice
                    amount++;
                }
            }
            let verb = "are";
            if(amount === 1){//made a dynamic verb for subject verb agreement
                verb = "is"
            }//Popup the answer.
            alert("There "+verb+" "+amount+" "+choice+ " fish in the tank.");
        }
    }

}



Main();
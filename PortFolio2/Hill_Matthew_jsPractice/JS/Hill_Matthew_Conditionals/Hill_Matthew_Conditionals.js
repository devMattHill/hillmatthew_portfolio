




function Main(){
    //Builds the main menu using the Menus_Validators library.
    let header = "Choose the tool you would like to use.";
    let options = [];
    options.push("Temperature Converter");
    options.push("Last Chance for Gas");
    options.push("Grade Letter Calculator");
    options.push("Discount Double Check");
    options.push("Exit");
    let menu = new Menu(header, options);
    let choice= "";

    //The menu for the main function here, it will keep running until the user chooses to exit
    while(choice !== "exit"){
        choice = menu.display();
        switch(choice){
            case "temperature converter":
                TempConverter();
                break;
            case "last chance for gas":
                LastGas();
                break;

            case "grade letter calculator":
                GradeLetterCalculator();
                break;

            case "discount double check":
                DiscountDoubleCheck();
                break;

            case "exit":
                break;

            case null://This allows the user to press cancel on a prompt to leave the program
                choice = "exit";
                break;
            default:

                break;
        }
    }
    console.log("Goodbye!");
}



function TempConverter(){//This sets up a menu to determine the type of conversion required.
    let header = "Choose a conversion option.";
    let options = [];
    options.push("Fahrenheit to Celsius");
    options.push("Celsius to Fahrenheit");

    let menu = new Menu(header,options);
    let choice="";

    while (choice !== "exit"){
        choice = menu.display();
        switch(choice){
            case "fahrenheit to celsius":
                choice = "exit";//This prevents the while loop from looping after the follow on method is chosen.
                F2C();
                break;
            case "celsius to fahrenheit":
                choice = "exit";//This prevents the while loop from looping after the follow on method is chosen.
                C2F();
                break;

            case null://The allows the user to press cancel and return to the main menu.
                choice = "exit";
                break;

            default:

                break;
        }

    }

}

function LastGas(){//Determines if a car can make 200 miles of travel to the next gas station.
    let validator = new Validator();
    let tankSize;
    let fillAmount;
    let mpg;
    let tankHeader = "How many gallons does your car's tank hold?";
    tankSize = validator.getFloat(tankHeader);//Uses a Validator(). to store acquire three variables needed
    if(tankSize !== null) {//Repeated null checks allow the user to press cancel at any time during the process
        let fillHeader = "How full is your car's tank as a percentage?\n Ex: if 75% full enter \"75\"";
        fillAmount = validator.getFloat(fillHeader);
        if(fillAmount !== null){
            let mpgHeader = "How many miles per gallon does your car get?";
            mpg = validator.getFloat(mpgHeader);
            if(mpg !== null){

                let fill = fillAmount/100;
                let milesRemaining = tankSize * fill * mpg;
                if (milesRemaining >= 200){//Popup answer if car has enough gas.
                    alert("Yes, you can drive "+ milesRemaining +" more miles and you can make it without stopping for gas!");
                }else{//Popup answer if car does not have enough gas.
                    alert("You only have "+ milesRemaining +" you can drive, better get gas now while you can!");
                }

            }
        }
    }




}



function GradeLetterCalculator(){//Checks an integer value from 0 - 100 and posts a letter grade for that value
    let header = "Please enter the grade for the course in whole numbers between 0 and 100.";
    let validator = new Validator();
    let grade = -1;
    while ((grade > 100 || grade < 0) && grade !== null){//Keeps all answers between 0 and 100 allows for null
        grade = validator.getInt(header);//Uses a Validator to acquire an int value from the user.
    }
    if (grade === null){//Allow the user to press cancel on the prompt

    }else if(grade > 89){//These all popup an appropriate grade based of the integer value
        alert("You have a "+ grade +" which means you have earned an A in the class!");
    }else if(grade > 79){
        alert("You have a "+ grade +" which means you have earned a B in the class.");
    }else if(grade > 72){
        alert("You have a "+ grade +" which means you have earned a C in the class.");
    }else if(grade > 69){
        alert("You have a "+ grade +" which means you have earned a D in the class.");
    }else{
        alert("You have a "+ grade +" which means you have earned an F the class!");
    }

}

function DiscountDoubleCheck(){
    let validator = new Validator();
    let price1;
    let price2;
    let itemHeader1 = "How much did you pay for the first item?";

    price1 = validator.getFloat(itemHeader1);//Uses a validator to acquire a price from the user
    if (price1 !== null){//Allows the user to press cancel on a prompt
        let itemHeader2 = "Item 1: $"+price1+"\nHow much did you pay for the second item?";
        price2 = validator.getFloat(itemHeader2);
        if(price2 !== null){
            let subTotal = price1 + price2;//Adds the two prices together
            let total;
            if (subTotal >= 100){//If statements determine the discount earned based on the subTotal.
                total = (subTotal * .9).toFixed(2);//Discount is applied and a popup with all the prices and total appears
                alert("Item 1: $"+price1+"\nItem 2 :$"+price2+"\nSubtotal: $"+subTotal+"\nYour total purchase is $" + total +", which includes your 10% discount.");
            }else if(subTotal >= 50){
                total = (subTotal * .95).toFixed(2);
                alert("Item 1: $"+price1+"\nItem 2 :$"+price2+"\nSubtotal: $"+subTotal+"\nYour total purchase is $" + total +", which includes your 5% discount.");
            }else{
                total = subTotal;
                alert("Item 1: $"+price1+"\nItem 2 :$"+price2+"\nSubtotal: $"+subTotal+"\nYour total purchase is $" + total +".");
            }

        }
    }


}

function F2C(){//Converts fahrenheit to celsius.
    let validator = new Validator();
    let header = "Enter the temperature in degrees Fahrenheit.";
    let fahrenheit = validator.getFloat(header);//Uses a Validator() to get back a float number.
    if (fahrenheit !== null){//Null check allows the user to end the process by pressing cancel on a prompt
        let res = (fahrenheit -32) / 1.8;//This is the math that does the conversion
        alert(fahrenheit + " degrees Fahrenheit converts to " + res.toFixed(2) + "degrees Celsius.");//Popup with answer
    }
}

function C2F(){//Converts celsius to fahrenheit.
    let validator = new Validator();
    let header = "Enter the temperature in degrees Celsius.";
    let celsius = validator.getFloat(header);//Uses a Validator() to get back a float number.
    if (celsius !== null){//Null check allows the user to end the process by pressing cancel on a prompt
        let res = (celsius * 1.8) + 32;//This is the math that does the conversion
        alert(celsius + " degrees Celsius converts to " + res.toFixed(2) + "degrees Fahrenheit.");//Popup with answer
    }
}

Main();
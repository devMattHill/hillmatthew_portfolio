﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Matthew Hill
            #02
            14 May 2017
            Count Fish Assignment
            */

            //Declare Variables for the menu.
            string menuOptionStr = "";
            int menuOption = 0;
            //This loop makes sure the user must pick a number 1 - 4
            while (menuOption != 1 && menuOption != 2 && menuOption != 3 && menuOption != 4)
            {
                //This loop parses the 1 - 4 as an integer into the variable 'menuOption'
                while (!int.TryParse(menuOptionStr, out menuOption))
                {
                    Console.WriteLine("Welcome to the Fish Counter!");
                    Console.WriteLine("Choose the color you would like to count. \r\nType the number next to the color and press enter.");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("1. Red");
                    Console.WriteLine("2. Blue");
                    Console.WriteLine("3. Green");
                    Console.WriteLine("4. Yellow");
                    Console.WriteLine("------------------------------");
                    menuOptionStr = Console.ReadLine();
                }
            }
            /* 
             Make an array with all the fish colors so we can just reference the array instead of building an ugly if then chain for storing the color.
             This will also allow for easy expansion of the menu in the future if we pick up some purple fish. Note, there should be purple fish in the fish tank.
             The colorChoice variable is to make up for the fact that our menu options do not start at 0.
            */
            
            int colorChoice = menuOption - 1;
            string[] fishColor = new string[4] { "red","blue","green","yellow"};
            // The given fish tank
            string[] fishTank = new string[10] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };
            
            int fishAmount = 0;
            //This runs through the tank and checks to see if the fish are the same color as the fish color array choice. If they are idd adds 1 to the fish count.
            foreach (string fish in fishTank) {
                if (fish == fishColor[colorChoice]) {
                    fishAmount++;
                }
            }

            Console.WriteLine("In the fish tank there are {0} fish of the color {1}.",fishAmount,fishColor[colorChoice]);


            /*
                • Test that each color choice, 1,2,3, and 4, works from your menu and gives you the
                correct amount of fish.
                • Try typing in the number 6. Does your code ask the user to give a valid choice of 1-4?
                     It just repeats itself.
                • If you add more fish to the tank, does your code still work?
                     Yes there's even room for easily expanding to more colors.
                • User Choice – 1 for Red
                o Results - “In the fish tank there are 2 fish of the color Red.”
             
             */
        }
    }
}

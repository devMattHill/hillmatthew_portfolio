﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {


              /*
                  Matthew Hill
                  #02
                  13 May 2017
                  Logic Loops Assignment
              */

            string menuOptionStr = "";
            int menuOption = 0;
            while (menuOption != 1 && menuOption != 2 && menuOption != 3 && menuOption != 4)
            {

                while (!int.TryParse(menuOptionStr, out menuOption))
                {
                    Console.WriteLine("Welcome to the Logic Loops tool suite.");
                    Console.WriteLine("Choose the tool you would like to run by typing the number and pressing enter.");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("1. Tire Pressure");
                    Console.WriteLine("2. Movie Ticket Price");
                    Console.WriteLine("3. Add Up The Odds or Evens");
                    Console.WriteLine("4. Charge It!");
                    Console.WriteLine("------------------------------");
                    menuOptionStr = Console.ReadLine();
                }
            }

            if (menuOption == 1)
            {
                //Problem #1 Tire Pressure
                Console.WriteLine("\r\n\r\n");
                //Declare Variables
                int[] tirePressure = new int[4];
                string[] tirePressurestr = new string[4];
                string tireName = "";

                //User Input


                //The forloop will step through four times, once for each tire
                for (int i = 0; i < 4; i++)
                {
                    //While loop to make sure the tire pressure is input, with a TryParse to make sure the input is a number.
                    //Use of the i in the array values will make sure all arrays are testing or storing to the same index values
                    while (!int.TryParse(tirePressurestr[i], out tirePressure[i]))
                    {
                        //This if else statement will determine which tire we are asking about. The i in the array index values will keep everything lined up.
                        if (i == 0) { tireName = "Front Left"; }
                        else if (i == 1) { tireName = "Front Right"; }
                        else if (i == 2) { tireName = "Back Left"; }
                        else if (i == 3) { tireName = "Back Right"; }
                            Console.WriteLine("\r\nPlease enter the pressure of the {0} tire in whole numbers.",tireName);
                        tirePressurestr[i] = Console.ReadLine();
                    }


                }


                //This was a test writeline to check performance of the inut section.
                //Console.WriteLine("Your tires' pressures are as follows {0}, {1}, {2}, and {3}", tirePressure[0], tirePressure[1], tirePressure[2], tirePressure[3]);

                //Math
                //Boolean variables for testing if the front and back match. Any false values will lead to checking tires.
                //bool front = (tirePressure[0] == tirePressure[1]);
                //bool back = (tirePressure[2] == tirePressure[3]);
                //Removed these after going back over the rubric

                //Ouput Results
                //If all values are true then both sets of corresponding tires have equal tire pressure and we are good, any other result requires checking the tires.
                if ((tirePressure[0] == tirePressure[1]) && (tirePressure[2] == tirePressure[3]))
                {
                    Console.WriteLine("\r\nThe tires pass spec!");
                }
                else
                {
                    Console.WriteLine("\r\nGet your tires checked out!");
                }


                /*
                 Data Sets to test
                    o Front Left – 32, Front Right – 32, Back Left -30 Back Right- 30 - Tires OK
                    o Front Left – 36, Front Right – 32, Back Left -25 Back Right- 25 - Check Tires
                    o Test One Of Your Own & Write the results in your comment section.
                    o Front Left – 35, Front Right – 35, Back Left -35 Back Right- 34 - Check Tires
                */
            }
            else if (menuOption == 2)
            {
                //Problem #2 Movie Ticket Price

                //Declare Variables
                string timeStr = "";
                int time = 0;
                string ageStr = "";
                int age = 0;
                string price = "";
                //User Input
                // Age input with a loop to make sure an integer is input.
                while (!int.TryParse(ageStr, out age))
                {

                    Console.WriteLine("\r\nPlease enter the age of the movie goer in years.");
                    ageStr = Console.ReadLine();
                }

                // Age input with a loop to make sure an integer is input.
                while (!int.TryParse(timeStr, out time))
                {

                    Console.WriteLine("\r\nPlease enter the time of the movie in 24 hour format.");
                    Console.WriteLine("For example, if the movie is at 1pm input 13.");
                    timeStr = Console.ReadLine();
                }
                //Logic Block
                // if we meet any of the following criteria the ticket is $7, age is 55+ OR age is less than 10 or the time is between 14 and 17
                if (age > 54 || age < 10 || (time > 13 && time < 18))
                {
                    price = "$7.00";
                } else {
                    price = "$12.00";
                }


                //Ouput Results
                Console.WriteLine("The ticket price is {0}.", price);
                /*
                 Data Sets to test
                    o Age – 57, Time – 20, Ticket Price - $7.00
                    o Age – 9, Time – 20, Ticket Price - $7.00
                    o Age – 38, Time – 20, Ticket Price - $12.00
                    o Age – 25, Time – 16, Ticket Price - $7.00
                    o Test One Of Your Own & Write the results in your comment section.
                    o Age - 35, Time - 22, Ticket Price - $12.00
                */
            }
            else if (menuOption == 3)
            {
                //Problem #3 Add Up The Odds or Evens

                //Declare Variables
                int[] numBlock = new int[9] { 11, 22, 33, 44, 55, 66, 77, 88, 99};
                int total = 0;
                string type = "";

                //User Input


                string menuStr = "";
                int menuChoice = 0;
                //This line only allows the use of a number 1 or 2.
                while (menuChoice != 1 && menuChoice != 2)
                {
                    //This line makes sure an integer is input as the choice.
                    while (!int.TryParse(menuStr, out menuChoice))
                    {

                        Console.WriteLine("Would you like to add up the odds or evens?");
                        Console.WriteLine("------------------------------");
                        Console.WriteLine("1. Add the odds.");
                        Console.WriteLine("2. Add the evens.");
                        Console.WriteLine("------------------------------");
                        menuStr = Console.ReadLine();
                    }
                }



                //Logic and Mathsection

                if (menuChoice == 1) {
                    type = "odd";
                    foreach (int number in numBlock) {
                        //If statement to find odd integers and add them to the total
                        if (number % 2 == 1) {
                            total += number;

                        }
                    }
                } else {
                    type = "even";
                    foreach (int number in numBlock) {
                        //If statement to find even integers and add them to the total
                        if (number % 2 == 0)
                        {
                            total += number;

                        }
                    }
                }

                //Ouput Results    
                Console.WriteLine("The {0} numbers add up to {1}.", type, total);

                /*
                Data Sets to test
                    o Array: {1,2,3,4,5,6,7}, Sum of Evens – 12 Sum of Odds – 16
                    o Array: {12,13,14,15,16,17}, Sum of Evens – 42 Sum of Odds – 45
                    o Your array should be different than mine, make sure to test both even and odd
                    and put your results in this section.
                    o Array: {11,22,33,44,55,66,77,88,99}, Sum of Evens – 220 Sum of Odds – 275

                */
            }
            else if (menuOption == 4)
            {
                //Problem #4 Charge It!

                //Declare Variables
                
                decimal creditLimit = 0;
                string limitStr = "";
                decimal remainingCredit;
                string purchaseStr = "";
                decimal purchase = 0;
                decimal spent = 0;
                //User Input
                //A while loop that makes sure the user inputs a number
                while (!decimal.TryParse(limitStr, out creditLimit))
                {

                    Console.WriteLine("\r\nPlease enter your credit limit.");
                    Console.Write("$");
                    limitStr = Console.ReadLine();
                }


                //Math and loops outpus Oh my!
                //Keeps this forloop going until spent is greater than or equal to the credit limit
                while (spent < creditLimit) {
                    //Resetting these variables inside the loop so they reset every time the loop resets
                    purchaseStr = "";
                    purchase = 0;
                    //A while loop that makes sure the user inputs a number
                    while (!decimal.TryParse(purchaseStr, out purchase))
                    {

                        Console.WriteLine("Please enter your most recent purchase amount.");
                        Console.Write("$");
                        purchaseStr = Console.ReadLine();
                        
                        
                    }
                    //Adding the purhcase amount to the total spent
                    spent += purchase;
                    //Calculate remaining credit
                    remainingCredit = creditLimit - spent;
                    //Output based on remaining credit limit
                    if(spent < creditLimit) { 
                        Console.WriteLine("\r\nWith your current purchase of ${0}, you can still spend ${1}.", purchase, remainingCredit);
                    }else
                    {
                        Console.WriteLine("\r\nWith your last purchase you have reached your credit limit and exceeded it by ${0}.", spent - creditLimit);
                    }
                }
                


                /*
                Data Sets to test
                o Credit Limit – 20.00
                § Purchase1- 5.00 - You can still spend $15.00
                § Purchase2- 12.00 - You can still spend $3.00
                § Purchase3- 7.00 - You have exceeded your limit by $4.00.
                o Test One Of Your Own & Write the results in your comment section.

                o Credit Limit – 50.00
                § Purchase1- 9.00 - You can still spend $41.00
                § Purchase2- 10.00 - You can still spend $31.00
                § Purchase3- 11.00 - You can still spend $20.00.
                § Purchase4- 12.00 - You can still spend $8.00
                § Purchase5- 13.00 - You have exceeded your limit by $5.00.

                */
            }
            else
            {
                Console.WriteLine("Something Broke");
            }



            Console.WriteLine("\r\n\r\n\r\nThank you for using the Logic Loops tool suite!");


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hill_Matthew_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Matthew Hill
             #02
             12 May 2017
             Conditionals Assignment
             */

            /*
             Main Menu
             
             */
            string menuOptionStr = "";
            int menuOption = 0;
            while (menuOption != 1 && menuOption != 2 && menuOption != 3 && menuOption != 4)
            { 

                while (!int.TryParse(menuOptionStr, out menuOption)) {
                    Console.WriteLine("Welcome to the Conditionals tool suite.");
                    Console.WriteLine("Choose the tool you would like to run by typing the number and pressing enter.");
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("1. Temperature Converter");
                    Console.WriteLine("2. Last Chance For Gas");
                    Console.WriteLine("3. Grade Letter Calculator");
                    Console.WriteLine("4. Discount Double Check");
                    Console.WriteLine("------------------------------");
                    menuOptionStr = Console.ReadLine();
                }
            }

            if (menuOption == 1)
            {
                /*
                 Problem #1
                 Temperature Converter
                */
                //Declare Variables

                string numInput = "";

                decimal numToConvert;

                string typeInput = "";

                decimal numOutput = 0;

                string typeOuput;

                //Begin User input

                //Introduction about converting things

                Console.WriteLine("\r\n\r\nHello I'm the the Temperature Converter!");
                Console.WriteLine("I can help you convert from Farenheit to Celsius and vice versa!");
                //Input and validate type
                //Validate a string that has C or F in it. We will allow for upper or lower case as this will allow for a better UX.


                while (typeInput != "F" && typeInput != "f" && typeInput != "C" && typeInput != "c")
                {
                    Console.WriteLine("Choose your initial temperature type by typing \"C\" or \"F\" and pressing enter.");
                    typeInput = Console.ReadLine();
                }
                //Store a string value to make the following user prompt better and determine with math conversion to do
                string fOrC;
                if (typeInput == "f" || typeInput == "F")
                {
                    fOrC = "Farenheit";
                    typeOuput = "Celsius";
                }
                else
                {
                    fOrC = "Celsius";
                    typeOuput = "Farenheit";
                }


                //Input and validate a decimal

                while (!decimal.TryParse(numInput, out numToConvert))
                {
                    Console.WriteLine("Please enter the temperature in degrees {0}.", fOrC);
                    numInput = Console.ReadLine();
                }

                //End User Input

                //Begin conversion math
                //Test for C or F. Then base math on the result.
                if (fOrC == "Farenheit")
                {
                    //Math to turn Farenheit into Celsius
                    numOutput = (numToConvert - 32) / 1.8m;

                }
                else
                {
                    //Math to turn Celsius into Farenheit
                    numOutput = (numToConvert * 1.8m) + 32m;
                }



                //End conversion math

                //Display results.

                Console.WriteLine("\r\nGiven the original temprature of {0} Degrees {1}, \r\nThe temperature is {2} Degrees {3}.", numToConvert, fOrC, numOutput, typeOuput);


                /*
                 End of code for Problem #1

                Data	Sets	to	Test:
                (Note that data sets are not the only numbers that should work with your code, but you	
                need to	be sure	to test	these plus 1 of	your own for the test value section.)
                32F	is 0C
                100C is 212F
                50 c, Does a lowercase c make a	difference? Yes but, I accounted for it in the input section before reading this.
                o Test One	Of Your Own.  20C is 68F

                */
            }
            else if (menuOption == 2)
            {

                /*
                Problem #2
                Last Chance For Gas
                */

                //Declare variables
                string tankSizeStr = "";
                string tankFullnesStr = "";
                string mpgStr = "";
                decimal tankSize;
                decimal tankFullness;
                decimal mpg;

                Console.WriteLine("\r\n\r\nWelcome to the Last Chance For Gas tool.");
                Console.WriteLine("200 miles until the next stop! Can you make it?");

                //Begin User Input


                while (!decimal.TryParse(tankSizeStr, out tankSize))
                {
                    Console.WriteLine("\r\nPlease enter the size of your gas tank in gallons.");
                    tankSizeStr = Console.ReadLine();
                }

                while (!decimal.TryParse(tankFullnesStr, out tankFullness))
                {
                    Console.WriteLine("How full is your gas tank? If your tank is 50% full enter \"50\".");
                    tankFullnesStr = Console.ReadLine();
                }

                while (!decimal.TryParse(mpgStr, out mpg))
                {
                    Console.WriteLine("Please enter your vehicles average miles per gallon.");
                    mpgStr = Console.ReadLine();
                }

                //End User Input

                //Math stuff here

                decimal maxDistance = tankSize * tankFullness / 100 * mpg;

                //Display Results
                /*
                Console.WriteLine("Your gas tank has "+ tankSize+" gallons in it.");
                Console.WriteLine("Your gas tank is "+ tankFullness +"% full.");
                Console.WriteLine("Your car gets "+ mpg +" miles per gallon.");
                Console.WriteLine("Your can drive another " + maxDistance + " miles.");
                */
                if (maxDistance > 200)
                {
                    Console.WriteLine("\r\nYes, you can drive " + maxDistance + " more miles and you can make it without stopping for gas!");
                }
                else
                {

                    Console.WriteLine("\r\nYou only have " + maxDistance + " miles you can drive, better get gas now while you can!");
                }


                /*
                 End of code for Problem #2

                    Gallons 20, Gas Tank = 50% full, MPG-25
                    Result – “Yes, you can drive 250 more miles and you can make it without stopping for gas!”
                    Gallons 12 , Gas Tank = 60% full, MPG-20
                    Result – “You only have 144 miles you can drive, better get gas now while you can!”
                    Test One Of Your Own
                    Gallons 10, Gas Tank = 50% full, MPG-10
                    Result – “You only have 50 miles you can drive, better get gas now while you can!”
                */
            }
            else if (menuOption == 3)
            {
                /*
                Problem #3
                Grade Letter Calculator
                */

                string gradeStr = "";
                int grade = -1;
                while (grade > 100 || grade < 0)
                {

                    while (!int.TryParse(gradeStr, out grade))
                    {
                        Console.WriteLine("\r\n\r\nWelcome to the Grade Letter Calculator tool.");
                        Console.WriteLine("Please enter the whole number value of your grade to receive the letter value of your grade.");
                        gradeStr = Console.ReadLine();
                    }
                }

                string gradeLetter ="";
                if (grade > 89) {
                    gradeLetter = "A";
                }else if (grade > 79) {
                    gradeLetter = "B";
                } else if (grade > 72) {
                    gradeLetter = "C";
                } else if (grade > 69) {
                    gradeLetter = "D";
                } else  {
                    gradeLetter = "F";
                }

                Console.WriteLine("\r\nYou have a {0}%, which means you have a(n) {1} in the class!", grade, gradeLetter);

                /*
                 End of code for Problem #3

                    o Grade	– 92%	
                    § Result - “You have a 92%, which means you have earned a(n) A in the class!”
                    o Grade	– 80%	
                    § Result - “You have a 80%, which means you have earned a(n) B in the class!”
                    o Grade	– 67%	
                    § Result - “You have a 67%, which means you have earned a(n) F in the class!”
                    o What happens when you type in 120%? I planned for that it just keeps asking you to input your grade.
                    § Is this a valid response? No because the assignment said between 0 and 100. So I set up the initial while loop to take bad inputs into account.
                    Test one of your own
                    Grade - 75
                    “You have a 75%, which means you have earned a(n) C in the class!”

                */
            }
            else if (menuOption == 4)
            {


                /*
                Problem #4
                Discount Double Check
                */
                Console.WriteLine("\r\n\r\nWelcome to the Discount Double Check tool.");
                Console.WriteLine("You are going to purchase (2) items from an online store.");
                Console.WriteLine("If you spend $100 or more, you will get a 10% discount on your total purchase.");
                Console.WriteLine("If you spend between $50 and $100, you will get a 5% discount on your total purchase.");
                Console.WriteLine("If you spend less than $50, you will get no discount.");

                //Declare variables
                string item1str = "";
                decimal item1 = 0m;
                string item2str = "";
                decimal item2 = 0m;
                decimal subTotal = 0m;
                string discount = "";
                decimal grandTotal = 0m;

                //User Input
                while (!decimal.TryParse(item1str, out item1))
                {
                    
                    Console.WriteLine("\r\nPlease enter the price of your first item.");
                    Console.Write("$");
                    item1str = Console.ReadLine();
                }


                while (!decimal.TryParse(item2str, out item2))
                {

                    Console.WriteLine("\r\nPlease enter the price of your second item.");
                    Console.Write("$");
                    item2str = Console.ReadLine();
                }


                //Determine the subtotal
                subTotal = item1 + item2;

                //Determine the discount
                if (subTotal >= 100) {
                    discount = "10%";
                    grandTotal = subTotal * .9m;
                    Console.WriteLine("\r\nYour total purchase is ${0}, which includes your {1} discount.", grandTotal, discount);
                } else if (subTotal >= 50) {
                    grandTotal = subTotal * .95m;
                    discount = "5%";
                    Console.WriteLine("\r\nYour total purchase is ${0}, which includes your {1} discount.", grandTotal, discount);
                } else {
                    grandTotal = subTotal * 1;
                    Console.WriteLine("\r\nYour total purchase is ${0}.", grandTotal);
                }

                /*
                 End of code for Problem #4

            First Item Cost $45.50, Second Item Cost $75.00, Total $108.45
            Your total purchase is $108.45, which includes your 10% discount.

            First Item Cost $30.00, Second Item Cost $25.00, Total $52.25
            Your total purchase is $52.25, which includes your 5% discount.

            First Item Cost $5.75, Second Item Cost $12.50, Total $18.25
            Your total purchase is $18.25.

            First Item Cost $101.00, Second Item Cost $102.00, Total $
            Your total purchase is $182.70, which includes your 10% discount.

            

                */
            }
            else
            {
                Console.WriteLine("\r\n\r\nYou broke it. I don't know how you broke it but you did. :( ");
            }
            Console.WriteLine("\r\n\r\n\r\nThank you for using the Conditionals tool suite!");
        }
    }
}

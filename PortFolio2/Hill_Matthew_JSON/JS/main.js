/*
Matthew Hill 2017 Nov 07
JSON Practice
 */

//Variable just helps with clarity in typing
var forecast = data.results.forecast;
//For each loop that prints the required info
forecast.forEach(function(fc){
    console.log("On "+fc.day+" it will be "+fc.sky+" with a high of "+fc.high+" and a low of "+fc.low+".");
});

//logs to separate things neatly
console.log(" ");
console.log("-----------------------------------------------------");
console.log(" ");

//Variable for storing parsed json text data
var obj = JSON.parse(text);
//Variables to help with clarity in typing
var roster = obj.employees;
var positions = myJSON.people;
//For loop for printing the required data
for(i = 0; i<roster.length; i++){
    console.log(roster[i].firstName + " " + roster[i].lastName+": "+positions[i].job)
};